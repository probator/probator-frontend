import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {UserService} from '../services/user.service';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AppSettings} from '../app-settings';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private userService: UserService, private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.userService.isLoggedIn) {
            req = req.clone({
                setHeaders: {
                    Authorization: this.userService.jwtToken,
                    'X-Csrf-Token': this.userService.csrfToken
                }
            });
        }

        return next.handle(req).pipe(
            catchError(error => {
                if (error.status === 401) {
                    this.handleAuthError();
                    return of(error);
                }

                throw error;
            })
        );
    }

    private handleAuthError() {
        this.userService.logout();
        window.localStorage.setItem(`${AppSettings.StorageKey}-auth-redir`, this.router.url);
        this.router.navigate(['/login']);
    }
}
