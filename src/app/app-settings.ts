import {animate, state, style, transition, trigger} from '@angular/animations';
import {IMenuGroup} from './interfaces';

export class AppSettings {
    public static API_V1 = '/api/v1';
    public static StorageKey = 'probator';
    public static StorageKeyRedir = 'probator-auth-redir';
    public static SupportedAPIVersions = '~1.0';
    public static MaxUploadFileSize = 5 * 1024 * 1024;  // 5MB upload limit
    public static MenuItems: IMenuGroup[] = [
        {
            name: 'Browse',
            key: 'browse',
            menuItems: [
                {name: 'Dashboard', path: '/browse/dashboard'},
                {name: 'Resources', path: '/browse/resources'},
                {name: 'Issues', path: '/browse/issues'},
            ]
        },
        {
            name: 'Admin',
            key: 'admin',
            requiredRole: 'Admin',
            menuItems: [
                {name: 'Accounts', path: '/admin/accounts'},
                {name: 'Config', path: '/admin/config'},
                {name: 'Users', path: '/admin/users'},
                {name: 'API Keys', path: '/admin/apikeys'},
                {name: 'Roles', path: '/admin/roles'},
                {name: 'Templates', path: '/admin/templates'},
                {name: 'Emails', path: '/admin/emails'},
                {name: 'Audit Logs', path: '/admin/auditlog'},
                {name: 'Logs', path: '/admin/logs'},
            ]
        }
    ];

    // region Animations
    public static OpenCloseAnimations = [
        trigger('openClose', [
            transition(':enter', [
                style({ height: 0 }),
                animate(250, style({ height: '*' }))
            ]),
            transition(':leave', [
                animate(250, style({ height: 0 }))
            ])
        ]),
        trigger('rotate', [
            state('closed', style({
                transform: 'rotate(180deg)',
            })),
            state('open', style({ })),
            transition('open <=> closed', animate('250ms linear')),
        ])
    ];
    public static InlineDetailAnimations = [
        trigger('detailExpand', [
            state('collapsed', style({height: '0', minHeight: '0'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('300ms cubic-bezier(0.645, 0.045, 0.355, 1)')),
        ]),
    ];
    public static MenuAnimations = [
        trigger('openClose', [
            transition(':enter', [
                style({ height: 0 }),
                animate('.1s', style({ height: '*' }))
            ]),
            transition(':leave', [
                animate('.1s', style({ height: 0 }))
            ])
        ]),
        trigger('rotate', [
            state('closed', style({
                transform: 'rotate(180deg)',
            })),
            state('open', style({ })),
            transition('open <=> closed', animate('250ms linear')),
        ])
    ];
    // endregion
}
