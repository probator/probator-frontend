import {MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

export interface BaseResponse {
    message?: string;
}

export interface IAuthInfo {
    state?: string;
    url?: string;
}

export interface IRole {
    roleId?: number;
    name: string;
    color: string;
}

export interface IRoleList {
    roles: IRole[];
}

export interface IUser {
    userId?: number;
    authSystem: string;
    username: string;
    name: string;
    email: string;
    roles: IRole[];
}

export interface IIssueType {
    name: string;
    issueTypeId: number;
    issueType: string;
    properties: IIssueTypeProperty[];
}

export interface IIssueTypeProperty {
    key: string;
    name: string;
    type: string;
    show: boolean;
    primary: boolean;
    resourceReference: boolean;
    accountReference: boolean;
}

export interface IMetadata {
    accounts: IAccount[];
    regions: string[];
    menuItems: object;
    accountTypes: IAccountType[];
    currentUser: IUser;
    notifiers: INotifier[];
    resourceTypes: IResourceType[];
    issueTypes: IIssueType[];
}

export interface IAccountContact {
    type: string;
    value: string;
}

export interface IAccountProperty {
    key: string;
    type: string;
    name: string;
    default: any;
    required: boolean;
    pattern: string;
}

export interface IAccountList {
    accounts: IAccount[];
}

export interface IAccount {
    accountId?: number;
    accountName: string;
    accountType: string;
    accountTypeId: number;
    contacts: IAccountContact[];
    enabled: boolean;
    properties: object;
    requiredRoles: string[];
}

export interface IAccountType {
    name: string;
    properties: IAccountProperty[];
}

export interface IDisplayProperty {
    objectType: 'resource'|'issue';
    parentType: number;
    propertyType: string;
    key: string;
    value: any;
}

export interface IResourceTypeProperty {
    key: string;
    name: string;
    type: string;
    show: boolean;
    primary: boolean;
    resourceReference: boolean;
}

export interface IResourceType {
    name: string;
    resourceTypeId: number;
    resourceType: string;
    resourceGroup: string;
    properties: IResourceTypeProperty[];
}

export interface IResourceTag {
    key: string;
    value: any;
}

export interface IResource {
    resourceId: string;
    resourceType: number;
    accountId: number;
    location: string;
    properties: object;
    account: IAccount;
    tags: IResourceTag[];
}

export interface IResourceList {
    resourceCount: number;
    resources: IResource[];
}

export interface INotifier {
    type: string;
    validation: string;
}

export interface IMessageOptions {
    verticalPosition?: MatSnackBarVerticalPosition;
    duration?: number;
    panelClass?: string | null;
}

export interface IMenuItem {
    name: string;
    path: string;
}

export interface IMenuGroup {
    name: string;
    key: string;
    requiredRole?: string;
    menuItems: IMenuItem[];
}

export interface IVersionInfo {
    version: string;
}
