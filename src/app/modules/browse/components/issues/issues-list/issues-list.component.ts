import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FilterComponent} from '../../../../utils/components/filter/filter.component';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {IIssue, IIssueList} from '../../../interfaces';
import {BrowseApiService} from '../../../services/browse-api.service';
import {AppSettings} from '../../../../../app-settings';
import {IssuesSearchHelpComponent} from '../issues-search-help/issues-search-help.component';
import {IFilter, IFilterButton} from '../../../../utils/interfaces';

@Component({
    selector: 'app-issues-list',
    templateUrl: './issues-list.component.html',
    styleUrls: ['./issues-list.component.scss'],
    animations: AppSettings.InlineDetailAnimations,
})
export class IssuesListComponent implements OnInit {
    issueCount: number;
    issues: IIssue[];
    displayedColumns = ['issueId', 'issueType', 'account', 'location', 'info'];
    loading = false;
    filters: IFilter[];
    buttons: IFilterButton[];
    expandedIssue: IIssue | null;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(FilterComponent, { static: true }) filterComp: FilterComponent;
    @ViewChild('top', { read: ElementRef, static: true }) pageTop: ElementRef;
    getIssueTypeName = (t) => this.metadata.getIssueTypeName(t);

    constructor(
        private browseApi: BrowseApiService,
        private metadata: MetadataService,
        private utils: UtilsService,
        private route: ActivatedRoute,
        private router: Router,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        if (this.metadata.loaded) {
            this.init();
        } else {
            this.metadata.metadataLoaded.subscribe(() => {
                this.init();
            });
        }
    }

    init() {
        const page = this.route.snapshot.queryParams.page;
        if (page) {
            this.paginator.pageIndex = page;
        }

        const limit = this.route.snapshot.queryParams.limit;
        if (limit) {
            this.paginator.pageSize = limit;
        }

        this.setupFilters();

        this.route.queryParams.subscribe(params => {
            this.paginator.pageIndex = params.hasOwnProperty('page') ? params.page : 0;
            this.paginator.pageSize = params.hasOwnProperty('limit') ? params.limit : 25;

            this.loadIssues(true).subscribe(data => this.updateIssues(data));
        });
    }

    loadIssues(initial: boolean): Observable<IIssueList> {
        this.loading = true;
        let terms, accounts, locations, issueTypes;

        // We need to have a separate use case for the initial page load, to properly handle people
        // bookmarking links with filters enabled.
        if (initial) {
            const params = this.route.snapshot.queryParams;

            terms = params.hasOwnProperty('terms') ? params.terms : '';
            accounts = params.hasOwnProperty('accounts') ? params.accounts : [];
            locations = params.hasOwnProperty('locations') ? params.locations : [];
            issueTypes = params.hasOwnProperty('issueTypes') ? params.issueTypes : [];
        } else {
            terms = this.filterComp.getValues('terms');
            accounts = this.filterComp.getValues('accounts');
            locations = this.filterComp.getValues('locations');
            issueTypes = this.filterComp.getValues('issueTypes');
        }

        return this.browseApi.getIssues(
            {
                limit: this.paginator.pageSize || 25,
                page: this.paginator.pageIndex,
                accounts: accounts,
                locations: locations,
                terms: terms,
                issueTypes: issueTypes,
            }
        );
    }

    updateIssues(data: IIssueList) {
        this.loading = false;
        if (data) {
            this.issueCount = data.issueCount;
            this.issues = data.issues;

            this.pageTop.nativeElement.scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            });
        }
    }

    updateQueryParams(): void {
        const queryParams: any = {};

        if (this.paginator.pageSize !== 25) {
            queryParams.limit = this.paginator.pageSize;
        }

        if (this.paginator.pageIndex !== 0) {
            queryParams.page = this.paginator.pageIndex;
        }

        for (const filter of this.filters) {
            if (filter.values.length > 0) {
                queryParams[filter.key] = filter.values;
            }
        }

        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: queryParams
        }).then();
    }

    setupFilters(): void {
        const listify = a => {
            if (!Array.isArray(a)) {
                return [a];
            } else {
                return '';
            }
        };

        const params = this.route.snapshot.queryParams;

        this.filters = [
            {
                name: 'Search',
                key: 'terms',
                type: 'input',
                values: params.hasOwnProperty('terms') ? listify(params.terms) : '',
            },
            {
                name: 'Accounts',
                key: 'accounts',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('accounts') ? listify(params.accounts) : [],
                options: this.metadata.getAccountFilters()
            },
            {
                name: 'Locations',
                key: 'locations',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('locations') ? listify(params.locations) : [],
                options: this.metadata.getLocationFilters()
            },
            {
                name: 'Issue Types',
                key: 'issueTypes',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('issueTypes') ? listify(params.issueTypes) : [],
                options: this.metadata.getIssueTypeFilters()
            }
        ];

        this.buttons = [
            {
                icon: 'help_outline',
                color: 'accent',
                tooltip: 'Search help',
                action: this.openHelp.bind(this),
            }
        ];

        this.paginator.page.subscribe(() => this.updateQueryParams());
    }

    getPrimaryProperty(issue: IIssue) {
        const properties = this.metadata.getIssuePrimaryProperties(issue.issueType);
        const out = [];

        if (properties) {
            for (const property of properties) {
                for (const [key, value] of Object.entries(issue.properties)) {
                    if (key === property.key) {
                        if (property.type === 'array') {
                            out.push(`<b>${property.name}</b>: ${value.join(', ')}`);
                        } else {
                            out.push(`<b>${property.name}</b>: ${value}`);
                        }
                    }
                }
            }
        }

        if (out.length) {
            if (out.length > 1) {
                return `<ul><li>${out.join('</li><li>')}</li></ul>`;
            } else {
                return out[0];
            }
        }

        return '<i>N/A</i>';
    }

    filterChange() {
        // When the filters change, reset to the first page
        this.paginator.pageIndex = 0;
        this.updateQueryParams();
    }

    openHelp() {
        this.dialog.open(IssuesSearchHelpComponent, {
            width: '800px'
        });
    }
}
