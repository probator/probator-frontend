import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IssuesListComponent} from './issues-list.component';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../../admin/admin.material.module';
import {UtilsModule} from '../../../../utils/utils.module';
import {IssuesDetailsInlineComponent} from '../issues-details-inline/issues-details-inline.component';
import {PropertyDisplayComponent} from '../../property-display/property-display.component';

describe('IssuesListComponent', () => {
    let component: IssuesListComponent;
    let fixture: ComponentFixture<IssuesListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                IssuesListComponent,
                IssuesDetailsInlineComponent,
                PropertyDisplayComponent,
            ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule,
                UtilsModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of([]),
                        snapshot: {
                            data: {
                                issues: {
                                    issueCount: 0,
                                    issues: [
                                        {
                                            issueType: 1,
                                            issueId: 'reqtag-01161f927cb7e01b',
                                            properties: {
                                                resourceId: 'i-0223c74dba057874b',
                                                accountId: 1,
                                                location: 'us-west-2',
                                                created: 1543964186.914466,
                                                lastAlert: '0 seconds',
                                                missingTags: ['owner', 'accounting'],
                                                notes: [],
                                                resourceType: 'EC2 Instance',
                                                lastChange: '2018-12-04T15:09:20.162483',
                                                state: 'ALERT'
                                            }
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IssuesListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
