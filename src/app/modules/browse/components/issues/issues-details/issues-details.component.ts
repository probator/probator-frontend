import {Component, OnInit} from '@angular/core';
import {DetailsTableRow, IIssue} from '../../../interfaces';
import {ActivatedRoute} from '@angular/router';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {AppSettings} from '../../../../../app-settings';
import {IDisplayProperty} from '../../../../../interfaces';

@Component({
    selector: 'app-issues-details',
    templateUrl: './issues-details.component.html',
    styleUrls: ['./issues-details.component.scss'],
    animations: AppSettings.OpenCloseAnimations
})
export class IssuesDetailsComponent implements OnInit {
    issue: IIssue;
    isOpen = {
        'issue': true,
        'properties': true,
    };
    getIssueTypeName = (t) => this.metadata.getIssueTypeName(t);
    getIssuePropertyName = (t, p) => this.metadata.getIssuePropertyName(t, p);

    constructor(private metadata: MetadataService, private utils: UtilsService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.params.subscribe(() => {
            this.issue = this.route.snapshot.data.issue.issue;
        });
    }

    getDisplayProperty(property): IDisplayProperty {
        return {
            parentType: this.issue.issueType,
            objectType: 'issue',
            propertyType: this.metadata.getIssuePropertyType(this.issue.issueType, property.key),
            key: property.key,
            value: property.value
        };
    }

    filteredProperties(): DetailsTableRow[] {
        const out = [];
        for (const [key, value] of Object.entries(this.issue.properties)) {
            const property = this.metadata.getIssueProperty(this.issue.issueType, key);
            if (property && !property.show) {
                continue;
            }
            out.push({key: key, value: value});
        }

        return out;
    }

    issueInfo(): DetailsTableRow[] {
        return [
            {
                key: 'Issue ID',
                value: this.issue.issueId,
            },
            {
                key: 'Issue Type',
                value: this.getIssueTypeName(this.issue.issueType)
            }
        ];
    }
}
