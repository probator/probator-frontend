import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IssuesDetailsComponent} from './issues-details.component';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowseMaterialModule} from '../../../browse.material.module';
import {ActivatedRoute} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {of} from 'rxjs';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {PropertyDisplayComponent} from '../../property-display/property-display.component';
import {UtilsModule} from '../../../../utils/utils.module';

describe('IssuesDetailsComponent', () => {
    let component: IssuesDetailsComponent;
    let fixture: ComponentFixture<IssuesDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                IssuesDetailsComponent,
                PropertyDisplayComponent,
            ],
            imports: [
                UtilsModule,
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                BrowseMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of([]),
                        snapshot: {
                            data: {
                                issue: {
                                    issue: {
                                        issueType: 1,
                                        issueId: 'reqtag-01161f927cb7e01b',
                                        properties: {
                                            resourceId: 'i-0223c74dba057874b',
                                            accountId: 1,
                                            location: 'us-west-2',
                                            created: 1543964186.914466,
                                            lastAlert: '0 seconds',
                                            missingTags: ['owner', 'accounting'],
                                            notes: [],
                                            resourceType: 'EC2 Instance',
                                            lastChange: '2018-12-04T15:09:20.162483',
                                            state: 'ALERT'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IssuesDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
