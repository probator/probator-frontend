import {Component} from '@angular/core';
import {MetadataService} from '../../../../../services/metadata.service';
import {MatDialogRef} from '@angular/material/dialog';
import {ResourceListComponent} from '../../resources/resource-list/resource-list.component';
import {IFilterValue} from '../../../../utils/interfaces';

@Component({
  selector: 'app-issue-search-help',
  templateUrl: './issues-search-help.component.html',
  styleUrls: ['./issues-search-help.component.scss']
})
export class IssuesSearchHelpComponent {
    columns = ['name', 'key'];
    issueTypes: IFilterValue[];
    properties = [];

    examples = {
        issueId: 'reqtag-01161f927cb7e01b',
        property: 'property:accountId=1'
    };
    constructor(private metadata: MetadataService, public dialogRef: MatDialogRef<ResourceListComponent>) {
        this.issueTypes = metadata.getIssueTypeFilters();
    }

    updateIssueProperties($event) {
        for (const itype of this.metadata.issueTypes) {
            if (itype.issueType !== $event.value) {
                continue;
            }
            this.properties = itype.properties.map(prop => ({key: prop.key, name: prop.name}));
        }
    }

    onClose() {
        this.dialogRef.close();
    }
}
