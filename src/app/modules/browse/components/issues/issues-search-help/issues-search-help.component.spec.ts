import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IssuesSearchHelpComponent} from './issues-search-help.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../../admin/admin.material.module';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('IssuesSearchHelpComponent', () => {
    let component: IssuesSearchHelpComponent;
    let fixture: ComponentFixture<IssuesSearchHelpComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ IssuesSearchHelpComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                ReactiveFormsModule,
                HttpClientModule,
                AdminMaterialModule
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IssuesSearchHelpComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
