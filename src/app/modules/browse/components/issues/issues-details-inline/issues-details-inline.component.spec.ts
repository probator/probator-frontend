import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {IssuesDetailsInlineComponent} from './issues-details-inline.component';
import {PropertyDisplayComponent} from '../../property-display/property-display.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {BrowseMaterialModule} from '../../../browse.material.module';
import {UtilsModule} from '../../../../utils/utils.module';

describe('IssuesDetailsInlineComponent', () => {
    let component: IssuesDetailsInlineComponent;
    let fixture: ComponentFixture<IssuesDetailsInlineComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                IssuesDetailsInlineComponent,
                PropertyDisplayComponent,
            ],
            imports: [
                UtilsModule,
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                BrowseMaterialModule,
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IssuesDetailsInlineComponent);
        component = fixture.componentInstance;
        component.issue = {
            issueType: 1,
            issueId: 'reqtag-01161f927cb7e01b',
            accountId: 1,
            account: null,
            location: 'us-west-2',
            properties: {
                resourceId: 'i-0223c74dba057874b',
                created: 1543964186.914466,
                lastAlert: '0 seconds',
                missingTags: ['owner', 'accounting'],
                notes: [],
                resourceType: 'EC2 Instance',
                lastChange: '2018-12-04T15:09:20.162483',
                state: 'ALERT'
            }
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
