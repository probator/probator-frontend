import {Component, Input} from '@angular/core';
import {IIssue} from '../../../interfaces';
import {MetadataService} from '../../../../../services/metadata.service';
import {IDisplayProperty} from '../../../../../interfaces';
import {AppSettings} from '../../../../../app-settings';

const hostname = window.location.hostname;

@Component({
    selector: 'app-issues-details-inline',
    templateUrl: './issues-details-inline.component.html',
    styleUrls: ['./issues-details-inline.component.scss'],
    animations: AppSettings.OpenCloseAnimations
})
export class IssuesDetailsInlineComponent {
    @Input() issue: IIssue;
    getIssuePropertyName = (t, p) => this.metadata.getIssuePropertyName(t, p);

    constructor(private metadata: MetadataService) { }

    getDisplayProperty(property): IDisplayProperty {
        return {
            parentType: this.issue.issueType,
            objectType: 'issue',
            propertyType: this.metadata.getIssuePropertyType(this.issue.issueType, property.key),
            key: property.key,
            value: property.value
        };
    }

    filteredProperties() {
        const out = {};
        for (const [key, value] of Object.entries(this.issue.properties)) {
            const property = this.metadata.getIssueProperty(this.issue.issueType, key);
            if (property && !property.show) {
                continue;
            }
            out[key] = value;
        }

        return out;
    }

    getPermanentLink(): string {
        return `https://${hostname}/browse/issues/details/${this.issue.issueId}`;
    }
}
