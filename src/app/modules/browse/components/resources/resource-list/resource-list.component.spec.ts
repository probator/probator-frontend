import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ResourceListComponent} from './resource-list.component';
import {RouterTestingModule} from '@angular/router/testing';
import {UtilsModule} from '../../../../utils/utils.module';
import {BrowseMaterialModule} from '../../../browse.material.module';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ResourceDetailsInlineComponent} from '../resource-details-inline/resource-details-inline.component';
import {PropertyDisplayComponent} from '../../property-display/property-display.component';

describe('ResourceListComponent', () => {
    let component: ResourceListComponent;
    let fixture: ComponentFixture<ResourceListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ResourceListComponent,
                ResourceDetailsInlineComponent,
                PropertyDisplayComponent,
            ],
            imports: [
                UtilsModule,
                RouterTestingModule,
                BrowseMaterialModule,
                HttpClientModule,
                NoopAnimationsModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ResourceListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
