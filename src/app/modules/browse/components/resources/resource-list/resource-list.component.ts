import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilsService} from '../../../../utils/services/utils.service';
import {IResource, IResourceList} from '../../../../../interfaces';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {FilterComponent} from '../../../../utils/components/filter/filter.component';
import {MetadataService} from '../../../../../services/metadata.service';
import {Observable} from 'rxjs';
import {BrowseApiService} from '../../../services/browse-api.service';
import {AppSettings} from '../../../../../app-settings';
import {ResourceSearchHelpComponent} from '../resource-search-help/resource-search-help.component';
import {IFilter, IFilterButton} from '../../../../utils/interfaces';

@Component({
    selector: 'app-resource-list',
    templateUrl: './resource-list.component.html',
    styleUrls: ['./resource-list.component.scss'],
    animations: AppSettings.InlineDetailAnimations,
})
export class ResourceListComponent implements OnInit {
    resourceCount = 0;
    resources: IResource[];
    displayedColumns = ['resourceId', 'resourceType', 'account', 'location', 'info'];
    loading = false;
    filters: IFilter[];
    buttons: IFilterButton[];
    expandedResource: IResource | null;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(FilterComponent, { static: true }) filterComp: FilterComponent;
    @ViewChild('top', { read: ElementRef, static: true }) pageTop: ElementRef;
    getResourceTypeName = (t) => this.metadata.getResourceTypeName(t);

    constructor(
        private route: ActivatedRoute,
        private browseApi: BrowseApiService,
        private metadata: MetadataService,
        private router: Router,
        public utils: UtilsService,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
        if (this.metadata.loaded) {
            this.init();
        } else {
            this.metadata.metadataLoaded.subscribe(() => {
                this.init();
            });
        }
    }

    init() {
        const page = this.route.snapshot.queryParams.page;
        if (page) {
            this.paginator.pageIndex = page;
        }

        const limit = this.route.snapshot.queryParams.limit;
        if (limit) {
            this.paginator.pageSize = limit;
        }

        this.setupFilters();
        this.route.queryParams.subscribe((params) => {
            this.paginator.pageIndex = params.hasOwnProperty('page') ? params.page : 0;
            this.paginator.pageSize = params.hasOwnProperty('limit') ? params.limit : 25;

            this.loadResources(true).subscribe(data => this.updateResources(data));
        });
    }

    loadResources(initial: boolean): Observable<IResourceList> {
        this.loading = true;
        let keywords, resourceTypes, accounts, locations;

        // We need to have a separate use case for the initial page load, to properly handle people
        // bookmarking links with filters enabled.
        if (initial) {
            const params = this.route.snapshot.queryParams;

            keywords = params.hasOwnProperty('keywords') ? params.keywords : '';
            resourceTypes = params.hasOwnProperty('resourceTypes') ? params.resourceTypes : [];
            accounts = params.hasOwnProperty('accounts') ? params.accounts : [];
            locations = params.hasOwnProperty('locations') ? params.locations : [];
        } else {
            keywords = this.filterComp.getValues('keywords');
            resourceTypes = this.filterComp.getValues('resourceTypes');
            accounts = this.filterComp.getValues('accounts');
            locations = this.filterComp.getValues('locations');
        }

        return this.browseApi.getResources(
            this.paginator.pageSize || 25,
            this.paginator.pageIndex,
            keywords,
            resourceTypes,
            accounts,
            locations

        );
    }

    updateResources(data: IResourceList) {
        this.loading = false;
        this.resourceCount = data.resourceCount;
        this.resources = data.resources;
        this.pageTop.nativeElement.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    }

    updateQueryParams(): void {
        const queryParams: any = {};

        if (this.paginator.pageSize !== 25) {
            queryParams.limit = this.paginator.pageSize;
        }

        if (this.paginator.pageIndex !== 0) {
            queryParams.page = this.paginator.pageIndex;
        }

        for (const filter of this.filters) {
            if (filter.values.length > 0) {
                queryParams[filter.key] = filter.values;
            }
        }

        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: queryParams
        }).then();
    }

    setupFilters(): void {
        const listify = a => { if (!Array.isArray(a)) {
            return [a];
        } else {
            return '';
        }};
        const params = this.route.snapshot.queryParams;

        this.filters = [
            {
                name: 'Search',
                key: 'keywords',
                type: 'input',
                multiple: false,
                values: params.hasOwnProperty('keywords') ? params.keywords : ''
            },
            {
                name: 'Accounts',
                key: 'accounts',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('accounts') ? listify(params.accounts) : [],
                options: this.metadata.getAccountFilters()
            },
            {
                name: 'Locations',
                key: 'locations',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('locations') ? listify(params.locations) : [],
                options: this.metadata.getLocationFilters()
            },
            {
                name: 'Resource Types',
                key: 'resourceTypes',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('resourceTypes') ? listify(params.resourceTypes) : [],
                options: this.metadata.getResourceTypeFilters(),
                grouped: true
            }
        ];

        this.buttons = [
            {
                icon: 'help_outline',
                color: 'accent',
                tooltip: 'Search help',
                action: this.openHelp.bind(this),
            }
        ];

        this.paginator.page.subscribe(() => this.updateQueryParams());
    }

    filterChange() {
        // When the filters change, reset to the first page
        this.paginator.pageIndex = 0;
        this.updateQueryParams();
    }

    getPrimaryProperty(resource: IResource) {
        const properties = this.metadata.getResourcePrimaryProperties(resource.resourceType);
        const out = [];

        if (properties) {
            for (const property of properties) {
                for (const [key, value] of Object.entries(resource.properties)) {
                    if (key === property.key) {
                        if (property.type === 'array') {
                            out.push(`<b>${property.name}</b>: ${value.join(', ')}`);
                        } else {
                            if (property.type === 'datetime') {
                                out.push(`<b>${property.name}</b>: ${this.utils.formatDate(value)}`);
                            } else {
                                out.push(`<b>${property.name}</b>: ${value}`);
                            }
                        }
                    }
                }
            }
        }

        if (out.length) {
            if (out.length > 1) {
                return `<ul><li>${out.join('</li><li>')}</li></ul>`;
            } else {
                return out[0];
            }
        }

        return '<i>N/A</i>';
    }

    openHelp() {
        this.dialog.open(ResourceSearchHelpComponent, {
            width: '800px'
        });
    }
}
