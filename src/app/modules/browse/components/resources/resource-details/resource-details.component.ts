import {Component, OnInit, ViewChild} from '@angular/core';
import {IDisplayProperty, IResource} from '../../../../../interfaces';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {catchError, map, switchMap} from 'rxjs/operators';
import {MetadataService} from '../../../../../services/metadata.service';
import {BrowseApiService} from '../../../services/browse-api.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {AppSettings} from '../../../../../app-settings';
import {DetailsTableRow} from '../../../interfaces';

@Component({
    selector: 'app-resource-details',
    templateUrl: './resource-details.component.html',
    styleUrls: ['./resource-details.component.scss'],
    animations: AppSettings.OpenCloseAnimations
})
export class ResourceDetailsComponent implements OnInit {
    resource: IResource;
    childCount = 0;
    children: IResource[];
    isOpen = {
        'resource': true,
        'properties': true,
        'tags': true,
        'children': true
    };
    @ViewChild(MatPaginator) paginator: MatPaginator;
    getResourceTypeName = (t) => this.metadata.getResourceTypeName(t);
    getResourcePropertyName = (t, p) => this.metadata.getResourcePropertyName(t, p);

    constructor(
        private browseApi: BrowseApiService,
        private metadata: MetadataService,
        private utils: UtilsService,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.route.params.subscribe(() => {
            this.resource = this.route.snapshot.data.resource.resource;
            if (this.resource !== null) {
                this.childCount = this.route.snapshot.data.children.resourceCount;
                this.children = this.route.snapshot.data.children.resources;
                this.setupChildrenPaginator();
            }
        });
    }

    getDisplayProperty(property): IDisplayProperty {
        return {
            parentType: this.resource.resourceType,
            objectType: 'resource',
            propertyType: this.metadata.getResourcePropertyType(this.resource.resourceType, property.key),
            key: property.key,
            value: property.value
        };
    }

    setupChildrenPaginator() {
        // It can sometimes take a few cycles before the menu paginator is loaded into the view.
        // This function will reschedule itself to run until it finds the paginator, if children are present on the account
        if (this.childCount > 0) {
            if (this.paginator) {
                this.paginator.page
                    .pipe(
                        switchMap(() => {
                            return this.browseApi.getResourceChildren(
                                this.resource.resourceId,
                                this.paginator.pageSize,
                                this.paginator.pageIndex
                            );
                        }),
                        map(data => {
                            this.childCount = data.resourceCount;
                            return data.resources;
                        }),
                        catchError(this.utils.handleError)
                    )
                    .subscribe(resources => {
                        this.children = resources;
                    })
                ;
            } else {
                setTimeout(() => {
                    this.setupChildrenPaginator();
                }, 250);
            }
        }
    }

    filteredProperties(): DetailsTableRow[] {
        const out = [];
        for (const [key, value] of Object.entries(this.resource.properties)) {
            const property = this.metadata.getResourceProperty(this.resource.resourceType, key);
            if (property && !property.show) {
                continue;
            }
            out.push({key: key, value: value});
        }

        return out;
    }

    resourceInfo(): DetailsTableRow[] {
        return [
            {
                key: 'Resource ID',
                value: this.resource.resourceId,
            },
            {
                key: 'Resource Type',
                value: this.getResourceTypeName(this.resource.resourceType)
            },
            {
                key: 'Account',
                value: this.resource.account.accountName,
            },
            {
                key: 'Location',
                value: this.resource.location
            }
        ];
    }
}
