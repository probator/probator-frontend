import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ResourceDetailsComponent} from './resource-details.component';
import {BrowseMaterialModule} from '../../../browse.material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {PropertyDisplayComponent} from '../../property-display/property-display.component';
import {UtilsModule} from '../../../../utils/utils.module';

describe('ResourceDetailsComponent', () => {
    let component: ResourceDetailsComponent;
    let fixture: ComponentFixture<ResourceDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ResourceDetailsComponent,
                PropertyDisplayComponent,
            ],
            imports: [
                UtilsModule,
                NoopAnimationsModule,
                BrowseMaterialModule,
                RouterTestingModule,
                HttpClientModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        params: of([]),
                        snapshot: {
                            data: {
                                resource: {
                                    resource: {
                                        resourceType: 4,
                                        resourceId: 'cfd-4e915d98e5eb3771',
                                        accountId: 1,
                                        account: {
                                            accountId: 1,
                                            accountName: 'account-name',
                                            accountTypeId: 1,
                                            contacts: [
                                                {
                                                    type: 'email',
                                                    value: 'name@domain.tld'
                                                }
                                            ],
                                            enabled: 1,
                                            requiredRoles: [],
                                            properties: [
                                                {
                                                    propertyId: 1,
                                                    accountId: 1,
                                                    name: 'account_number',
                                                    value: '123456789012'
                                                },
                                                {
                                                    propertyId: 2,
                                                    accountId: 1,
                                                    name: 'ad_group_base',
                                                    value: ''
                                                }
                                            ]
                                        },
                                        location: null,
                                        properties: {
                                            arn: 'arn:aws:cloudfront::123456789012:distribution/INVALID',
                                            domainName: 'dbogus.cloudfront.net',
                                            origins: [
                                                {
                                                    type: 's3',
                                                    source: 'mybucket.s3.amazonaws.com'
                                                }
                                            ],
                                            enabled: true,
                                            type: 'web'
                                        },
                                        tags: [
                                            {
                                                key: 'Name',
                                                value: 'My Cloudfront Distribution'
                                            }
                                        ],
                                    },
                                },
                                children: {
                                    resourceCount: 0,
                                    resources: []
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ResourceDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
