import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ResourceSearchHelpComponent} from './resource-search-help.component';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowseMaterialModule} from '../../../browse.material.module';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('ResourceSearchHelpComponent', () => {
    let component: ResourceSearchHelpComponent;
    let fixture: ComponentFixture<ResourceSearchHelpComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ResourceSearchHelpComponent ],
            imports: [
                RouterTestingModule,
                BrowseMaterialModule,
                HttpClientModule,
                NoopAnimationsModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ResourceSearchHelpComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
