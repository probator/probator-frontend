import {Component} from '@angular/core';
import {MetadataService} from '../../../../../services/metadata.service';
import {MatDialogRef} from '@angular/material/dialog';
import {ResourceListComponent} from '../resource-list/resource-list.component';
import {IFilterGroup} from '../../../../utils/interfaces';

@Component({
    selector: 'app-resource-search-help',
    templateUrl: './resource-search-help.component.html',
    styleUrls: ['./resource-search-help.component.scss']
})
export class ResourceSearchHelpComponent {
    columns = ['name', 'key'];
    resourceTypes: IFilterGroup[];
    properties = [];

    examples = {
        resourceId: 'i-91a6404c',
        property: 'property:instance_type=m4.large',
        tag: 'tag:application=MyCoolApplication\ntag:Name="My Cool Application Server"'
    };
    constructor(private metadata: MetadataService, public dialogRef: MatDialogRef<ResourceListComponent>) {
        this.resourceTypes = metadata.getResourceTypeFilters();
    }

    updateResourceProperties($event) {
        for (const rtype of this.metadata.resourceTypes) {
            if (rtype.resourceType !== $event.value) {
                continue;
            }
            this.properties = rtype.properties.map(prop => ({key: prop.key, name: prop.name}));
        }
    }

    onClose() {
        this.dialogRef.close();
    }
}
