import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ResourceDetailsInlineComponent} from './resource-details-inline.component';
import {PropertyDisplayComponent} from '../../property-display/property-display.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {BrowseMaterialModule} from '../../../browse.material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {UtilsModule} from '../../../../utils/utils.module';

describe('ResourceDetailsInlineComponent', () => {
    let component: ResourceDetailsInlineComponent;
    let fixture: ComponentFixture<ResourceDetailsInlineComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ResourceDetailsInlineComponent,
                PropertyDisplayComponent,
            ],
            imports: [
                UtilsModule,
                NoopAnimationsModule,
                BrowseMaterialModule,
                RouterTestingModule,
                HttpClientModule,
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ResourceDetailsInlineComponent);
        component = fixture.componentInstance;
        component.resource = {
            resourceType: 4,
            resourceId: 'cfd-4e915d98e5eb3771',
            accountId: 1,
            account: {
                accountId: 1,
                accountName: 'account-name',
                accountType: 'AWS',
                accountTypeId: 1,
                contacts: [
                    {
                        type: 'email',
                        value: 'name@domain.tld'
                    }
                ],
                enabled: true,
                requiredRoles: [],
                properties: [
                    {
                        propertyId: 1,
                        accountId: 1,
                        name: 'account_number',
                        value: '123456789012'
                    },
                    {
                        propertyId: 2,
                        accountId: 1,
                        name: 'ad_group_base',
                        value: ''
                    }
                ]
            },
            location: null,
            properties: {
                arn: 'arn:aws:cloudfront::123456789012:distribution/INVALID',
                domainName: 'dbogus.cloudfront.net',
                origins: [
                    {
                        type: 's3',
                        source: 'mybucket.s3.amazonaws.com'
                    }
                ],
                enabled: true,
                type: 'web'
            },
            tags: [
                {
                    key: 'Name',
                    value: 'My Cloudfront Distribution'
                }
            ],
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
