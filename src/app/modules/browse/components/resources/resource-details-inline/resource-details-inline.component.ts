import {Component, Input} from '@angular/core';
import {MetadataService} from '../../../../../services/metadata.service';
import {IDisplayProperty, IResource} from '../../../../../interfaces';

const hostname = window.location.hostname;

@Component({
    selector: 'app-resource-details-inline',
    templateUrl: './resource-details-inline.component.html',
    styleUrls: ['./resource-details-inline.component.scss']
})
export class ResourceDetailsInlineComponent {
    @Input() resource: IResource;
    getResourcePropertyName = (t, p) => this.metadata.getResourcePropertyName(t, p);

    constructor(private metadata: MetadataService) { }

    getDisplayProperty(property): IDisplayProperty {
        return {
            parentType: this.resource.resourceType,
            objectType: 'resource',
            propertyType: this.metadata.getResourcePropertyType(this.resource.resourceType, property.key),
            key: property.key,
            value: property.value
        };
    }

    filteredProperties() {
        const out = {};
        for (const [key, value] of Object.entries(this.resource.properties)) {
            const property = this.metadata.getResourceProperty(this.resource.resourceType, key);
            if (property && (!property.show || property.type === 'json')) {
                continue;
            }
            out[key] = value;
        }

        return out;
    }

    getPermanentLink(): string {
        return `https://${hostname}/browse/resources/details/${this.resource.resourceId}`;
    }
}
