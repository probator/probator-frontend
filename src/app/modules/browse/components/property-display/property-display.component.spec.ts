import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {PropertyDisplayComponent} from './property-display.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {BrowseMaterialModule} from '../../browse.material.module';
import {UtilsModule} from '../../../utils/utils.module';

describe('PropertyDisplayComponent', () => {
    let component: PropertyDisplayComponent;
    let fixture: ComponentFixture<PropertyDisplayComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ PropertyDisplayComponent ],
            imports: [
                UtilsModule,
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                BrowseMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PropertyDisplayComponent);
        component = fixture.componentInstance;
        component.property = {
            parentType: 1,
            objectType: 'resource',
            propertyType: 'string',
            key: 'property',
            value: 'value'
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
