import {Component, Input} from '@angular/core';
import {IDisplayProperty} from '../../../../interfaces';
import {MetadataService} from '../../../../services/metadata.service';
import {UtilsService} from '../../../utils/services/utils.service';

@Component({
    selector: 'app-property-display',
    templateUrl: './property-display.component.html',
    styleUrls: ['./property-display.component.scss']
})
export class PropertyDisplayComponent {
    @Input() property: IDisplayProperty;

    constructor(private metadata: MetadataService, private utils: UtilsService) { }

    public isResourceReference() {
        let prop;
        switch (this.property.objectType) {
            case 'resource':
                prop = this.metadata.getResourceProperty(this.property.parentType, this.property.key);
                return prop ? prop.resourceReference : false;
            case 'issue':
                prop = this.metadata.getIssueProperty(this.property.parentType, this.property.key);
                return prop ? prop.resourceReference : false;
        }
    }

    public displayValue() {
        if (this.property.key === 'accountId') {
            return `${this.metadata.getAccountName(this.property.value)} (ID: ${this.property.value})`;
        }

        if (!this.property.value) {
            return '<i>N/A</i>';
        }

        switch (this.property.propertyType) {
            case 'list':
            case 'array':
                let out = '<ul>';
                for (const value of this.property.value) {
                    out += `<li>${value}</li>`;
                }
                out += '</ul>';
                return out;

            case 'json':
                return this.property.value ? `<pre>${JSON.stringify(this.property.value, null, 4)}</pre>` : '';

            case 'datetime':
                return this.utils.formatDate(this.property.value);

            case 'string':
            case 'number':
            case 'int':
            case 'float':
            default:
                return this.property.value;
        }
    }
}
