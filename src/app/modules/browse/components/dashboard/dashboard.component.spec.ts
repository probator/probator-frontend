import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {DashboardComponent} from './dashboard.component';
import {UtilsModule} from '../../../utils/utils.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {BrowseMaterialModule} from '../../browse.material.module';
import {ActivatedRoute} from '@angular/router';

describe('DashboardComponent', () => {
    let component: DashboardComponent;
    let fixture: ComponentFixture<DashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ DashboardComponent ],
            imports: [
                UtilsModule,
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                BrowseMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                stats: {
                                    resources: [
                                        {resourceTypeId: 1, count: 4492},
                                        {resourceTypeId: 6, count: 29215},
                                        {resourceTypeId: 8, count: 4571},
                                        {resourceTypeId: 10, count: 1834},
                                        {resourceTypeId: 12, count: 422},
                                        {resourceTypeId: 14, count: 4458},
                                        {resourceTypeId: 17, count: 243},
                                        {resourceTypeId: 19, count: 692},
                                        {resourceTypeId: 20, count: 241},
                                        {resourceTypeId: 22, count: 29549},
                                        {resourceTypeId: 25, count: 267},
                                        {resourceTypeId: 28, count: 113}
                                    ],
                                    issues: [
                                        {issueTypeId: 2, count: 1790}
                                    ],
                                    resourceCount: 76097,
                                    issueCount: 1790,
                                    issueSummary: {
                                        '1': {issueCount: 5, resourceCount: 5, percentage: 0.0},
                                        '2': {issueCount: 140, resourceCount: 158, percentage: 11.39},
                                        '3': {issueCount: 640, resourceCount: 646, percentage: 92.87},
                                        '4': {issueCount: 75, resourceCount: 75, percentage: 0.0},
                                        '5': {issueCount: 57, resourceCount: 57, percentage: 0.0},
                                        '6': {issueCount: 240, resourceCount: 241, percentage: 41.49},
                                        '7': {issueCount: 175, resourceCount: 179, percentage: 22.34},
                                        '8': {issueCount: 102, resourceCount: 102, percentage: 0.0},
                                        '9': {issueCount: 187, resourceCount: 187, percentage: 0.0},
                                        '10': {issueCount: 74, resourceCount: 81, percentage: 8.64},
                                        '11': {issueCount: 95, resourceCount: 103, percentage: 77.66}
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
