import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MetadataService} from '../../../../services/metadata.service';
import {IDashboardStats} from '../../interfaces';
import {ChartConfiguration, generate as generateGraph} from 'c3';
import {GaugeConfig} from '../../../utils/interfaces';

const DEFAULT_DONUT_CONFIG = {
    legend: {
        show: true,
        position: 'right',
    },
    padding: {
        top: 0,
        right: 0,
        left: 0,
        bottom: 0
    },
    size: {
        width: 650,
    },
    donut: {
        width: 80,
        expand: true,
    },
    tooltip: {
        format: {}
    },
    data: {
        type: 'donut',
        columns: [],
    }
};

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    resourceCount = 0;
    issueCount = 0;
    gauges: GaugeConfig[] = [];
    resourceSummary: ChartConfiguration;
    issueSummary: ChartConfiguration;

    constructor(private route: ActivatedRoute, private metadata: MetadataService) {}

    ngOnInit() {
        // Sometimes the metadata is not loaded before the data is returned, so we have to check to ensure the graphs / gauges will
        // display correctly
        if (this.metadata.loaded) {
            this.setupGraphs();
        } else {
            this.metadata.metadataLoaded.subscribe(() => {
                this.setupGraphs();
            });
        }
    }

    setupGraphs() {
        const stats: IDashboardStats = this.route.snapshot.data.stats;

        this.resourceCount = stats.resourceCount;
        this.issueCount = stats.issueCount;

        for (const [acctId, data] of Object.entries(stats.issueSummary)) {
            const acctName = acctId !== '-1' ? this.metadata.getAccountName(parseInt(acctId, 10)) : 'Global';

            this.gauges.push({
                title: acctName,
                value: data.issueCount,
                percentage: data.percentage.toFixed(2),
            });
        }

        this.resourceSummary = this.getDonutConfig('#resourceSummary', 'Resources');
        this.issueSummary = this.getDonutConfig('#issueSummary', 'Issues');

        for (const resourceType of stats.resources) {
            this.resourceSummary.data.columns.push(
                [
                    this.metadata.getResourceTypeName(resourceType.resourceTypeId),
                    resourceType.count
                ]
            );
        }

        if (stats.issues.length > 0) {
            for (const issueType of stats.issues) {
                this.issueSummary.data.columns.push(
                    [
                        this.metadata.getIssueTypeName(issueType.issueTypeId),
                        issueType.count
                    ]
                );
            }
        } else {
            this.issueSummary.data.columns.push(['No Issues', 1]);
        }

        generateGraph(this.resourceSummary);
        generateGraph(this.issueSummary);
    }

    private getDonutConfig(bindTo: string, title: string): ChartConfiguration {
        const config: ChartConfiguration = JSON.parse(JSON.stringify(DEFAULT_DONUT_CONFIG));
        config.bindto = bindTo;
        config.donut.title = title;
        config.tooltip.format.value = (value) => `Count: ${value}`;

        return config;
    }
}
