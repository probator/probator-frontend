import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IDashboardStats} from '../interfaces';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {BrowseApiService} from '../services/browse-api.service';

@Injectable()
export class DashboardResolver implements Resolve<IDashboardStats> {
    constructor(private browseApi: BrowseApiService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<IDashboardStats> | Promise<IDashboardStats> | IDashboardStats {
        return this.browseApi.getDashboardStats();
    }

}
