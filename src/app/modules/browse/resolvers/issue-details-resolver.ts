import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IIssue} from '../interfaces';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {BrowseApiService} from '../services/browse-api.service';

@Injectable()
export class IssueDetailsResolver implements Resolve<IIssue> {
    constructor(private browseApi: BrowseApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IIssue> | Promise<IIssue> | IIssue {
        return this.browseApi.getIssue(route.params.issueId);
    }

}
