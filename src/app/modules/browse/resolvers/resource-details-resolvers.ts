import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {BrowseApiService} from '../services/browse-api.service';
import {IResource, IResourceList} from '../../../interfaces';

@Injectable()
export class ResourceDetailsResolver implements Resolve<IResource> {
    constructor(private browseApi: BrowseApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IResource> | Promise<IResource> | IResource {
        return this.browseApi.getResourceDetails(route.params.resourceId);
    }
}

@Injectable()
export class ResourceChildrenResolver implements Resolve<IResourceList> {
    constructor(private browseApi: BrowseApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IResourceList> | Promise<IResourceList> | IResourceList {
        return this.browseApi.getResourceChildren(route.params.resourceId, 10, 0);
    }
}
