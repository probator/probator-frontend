import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ResourceListComponent} from './components/resources/resource-list/resource-list.component';
import {BrowseMaterialModule} from './browse.material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UtilsModule} from '../utils/utils.module';
import {ResourceDetailsComponent} from './components/resources/resource-details/resource-details.component';
import {ResourceChildrenResolver, ResourceDetailsResolver} from './resolvers/resource-details-resolvers';
import {IssuesListComponent} from './components/issues/issues-list/issues-list.component';
import {IssuesDetailsComponent} from './components/issues/issues-details/issues-details.component';
import {IssueDetailsResolver} from './resolvers/issue-details-resolver';
import {PropertyDisplayComponent} from './components/property-display/property-display.component';
import {IssuesDetailsInlineComponent} from './components/issues/issues-details-inline/issues-details-inline.component';
import {ResourceDetailsInlineComponent} from './components/resources/resource-details-inline/resource-details-inline.component';
import {ResourceSearchHelpComponent} from './components/resources/resource-search-help/resource-search-help.component';
import {IssuesSearchHelpComponent} from './components/issues/issues-search-help/issues-search-help.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {DashboardResolver} from './resolvers/dashboard-resolver';

const routes: Routes = [
    {
        path: 'dashboard',
        component: DashboardComponent,
        resolve: {
            stats: DashboardResolver
        }
    },
    {
        path: 'resources',
        children: [
            {path: '', component: ResourceListComponent, pathMatch: 'full'},
            {
                path: 'details/:resourceId',
                component: ResourceDetailsComponent,
                resolve: {
                    resource: ResourceDetailsResolver,
                    children: ResourceChildrenResolver,
                }
            }
        ]
    },
    {
        path: 'issues',
        children: [
            {path: '', component: IssuesListComponent, pathMatch: 'full'},
            {
                path: 'details/:issueId',
                component: IssuesDetailsComponent,
                resolve: {
                    issue: IssueDetailsResolver
                }
            }
        ]
    }
];

@NgModule({
    declarations: [
        ResourceListComponent,
        ResourceDetailsComponent,
        IssuesListComponent,
        IssuesDetailsComponent,
        IssuesDetailsInlineComponent,
        PropertyDisplayComponent,
        ResourceDetailsInlineComponent,
        ResourceSearchHelpComponent,
        IssuesSearchHelpComponent,
        DashboardComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        RouterModule,
        BrowseMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        UtilsModule,
    ],
    providers: [
        ResourceDetailsResolver,
        ResourceChildrenResolver,
        IssueDetailsResolver,
        DashboardResolver,
    ],
    entryComponents: [
        ResourceSearchHelpComponent,
        IssuesSearchHelpComponent
    ]
})
export class BrowseModule { }
