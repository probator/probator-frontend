import {IAccount} from '../../interfaces';

export interface IIssueList {
    issueCount: number;
    issues: IIssue[];
}

export interface IIssue {
    issueType: number;
    issueId: string;
    accountId: number | null;
    account: IAccount | null;
    location: string | null;
    properties: { [key: string]: any };
}

export interface IIssueSummaryAccount {
    percentage: number;
    issueCount: number;
}

export interface IIssueSummary {
    [accountId: number]: IIssueSummaryAccount;
}

export interface IDashboardSummary {
    resourceTypeId?: number;
    issueTypeId?: number;
    count: number;
}

export interface IDashboardStats {
    resources: IDashboardSummary[];
    issues: IDashboardSummary[];
    resourceCount: number;
    issueCount: number;
    issueSummary: IIssueSummary;
}

export interface DetailsTableRow {
    key: string;
    value: string;
}
