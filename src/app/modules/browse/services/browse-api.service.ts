import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IResource, IResourceList} from '../../../interfaces';
import {AppSettings} from '../../../app-settings';
import {catchError} from 'rxjs/operators';
import {UtilsService} from '../../utils/services/utils.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {IDashboardStats, IIssue, IIssueList} from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class BrowseApiService {
    constructor(private utils: UtilsService, private http: HttpClient) { }

    getResources(count, page, keywords, resourceTypes, accounts, locations): Observable<IResourceList> {
        const params = {
            count: count,
            page: page,
            keywords: keywords,
            resourceTypes: resourceTypes,
            accounts: accounts,
            locations: locations
        };

        this.utils.cleanParams(params);

        return this.http.get<IResourceList>(`${AppSettings.API_V1}/resources`, {params: params})
            .pipe(catchError((err) => this.utils.handleError(err)));
    }

    getResourceDetails(resourceId): Observable<IResource> {
        return this.http.get<IResource>(`${AppSettings.API_V1}/resource/get/${resourceId}`)
            .pipe(catchError((err) => this.utils.handleError(err)));
    }

    getResourceChildren(resourceId, limit, page) {
        const params = {
            count: limit,
            page: page
        };
        this.utils.cleanParams(params);

        return this.http.get<IResourceList>(`${AppSettings.API_V1}/resource/children/${resourceId}`, {params: params})
            .pipe(catchError((err) => this.utils.handleError(err)));
    }

    public getIssues(params): Observable<IIssueList> {
        this.utils.cleanParams(params);

        return this.http.get<IIssueList>(`${AppSettings.API_V1}/issues`, {params: params as unknown as HttpParams})
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getIssue(issueId: string): Observable<IIssue> {
        return this.http.get<IIssue>(`${AppSettings.API_V1}/issues/details/${issueId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getDashboardStats(): Observable<IDashboardStats> {
        return this.http.get<IDashboardStats>(`${AppSettings.API_V1}/dashboard`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
}
