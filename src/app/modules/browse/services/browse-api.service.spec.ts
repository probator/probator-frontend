import {TestBed} from '@angular/core/testing';

import {BrowseApiService} from './browse-api.service';
import {HttpClientModule} from '@angular/common/http';
import {UtilsModule} from '../../utils/utils.module';

describe('BrowseApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            HttpClientModule,
            UtilsModule,
        ]
    }));

    it('should be created', () => {
        const service: BrowseApiService = TestBed.inject(BrowseApiService);
        expect(service).toBeTruthy();
    });
});
