import {Subject} from 'rxjs';

export class Upload {
    file: File;
    name: string;
    loaded = 0;
    uploadStart: Date;
    uploadEnd: Date;
    data: string;

    constructor(file: File) {
        this.file = file;
    }

    upload(): Subject<boolean> {
        let reader = new FileReader();
        const obs = new Subject<boolean>();

        reader.onloadend = () => {
            this.loaded = 100;
            this.uploadEnd = new Date();
            this.data = reader.result.toString();
            reader = null;
            obs.next(true);
        };

        reader.onprogress = evt => {
            this.loaded = evt.loaded;
        };

        this.uploadStart = new Date();
        reader.readAsText(this.file);

        return obs;
    }
}
