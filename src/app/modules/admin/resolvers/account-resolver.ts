import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {IAccount} from '../../../interfaces';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class AccountResolver implements Resolve<IAccount> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAccount> | Promise<IAccount> | IAccount {
        return this.adminApi.getAccount(route.params.accountId);
    }
}
