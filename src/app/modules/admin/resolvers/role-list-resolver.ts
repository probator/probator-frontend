import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {IRoleList} from '../../../interfaces';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class RoleListResolver implements Resolve<IRoleList> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRoleList> | Promise<IRoleList> | IRoleList {
        return this.adminApi.getRoles();
    }
}
