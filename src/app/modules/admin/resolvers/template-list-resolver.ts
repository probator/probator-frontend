import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ITemplateList} from '../interfaces';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class TemplateListResolver implements Resolve<ITemplateList> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITemplateList> | Promise<ITemplateList> | ITemplateList {
        return this.adminApi.getTemplates();
    }
}
