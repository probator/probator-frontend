import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {IConfigNamespace} from '../interfaces';
import {Observable} from 'rxjs';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class ConfigNamespaceResolver implements Resolve<IConfigNamespace>  {
    constructor(private adminApi: AdminApiService) {}

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<IConfigNamespace> | Promise<IConfigNamespace> | IConfigNamespace {
        return this.adminApi.getConfigNamespace(route.params.namespacePrefix);
    }

}
