import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IUserDetails} from '../interfaces';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class UserResolver implements Resolve<IUserDetails> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserDetails> | Promise<IUserDetails> | IUserDetails {
        return this.adminApi.getUser(route.params.userId);
    }
}
