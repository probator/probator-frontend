import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {IRole} from '../../../interfaces';
import {Observable} from 'rxjs';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class RoleResolver implements Resolve<IRole> {
    constructor(private adminApi: AdminApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRole> | Promise<IRole> | IRole {
        return this.adminApi.getRole(route.params.roleId);
    }
}
