import {AdminApiService} from '../services/admin-api.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {IConfigItem} from '../interfaces';
import {Injectable} from '@angular/core';

@Injectable()
export class ConfigItemResolver implements Resolve<IConfigItem> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IConfigItem> | Promise<IConfigItem> | IConfigItem {
        return this.adminApi.getConfigItem(route.params.namespacePrefix, route.params.key);
    }
}
