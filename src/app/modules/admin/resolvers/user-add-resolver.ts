import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AdminApiService} from '../services/admin-api.service';
import {Injectable} from '@angular/core';
import {IUserAddInfo} from '../interfaces';

@Injectable()
export class UserAddResolver implements Resolve<IUserAddInfo> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserAddInfo> | Promise<IUserAddInfo> | IUserAddInfo {
        return this.adminApi.getUserAddInfo();
    }
}
