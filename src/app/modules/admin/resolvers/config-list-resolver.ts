import {IConfigList} from '../interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {AdminApiService} from '../services/admin-api.service';
import {Observable} from 'rxjs';

@Injectable()
export class ConfigListResolver implements Resolve<IConfigList> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IConfigList> | Promise<IConfigList> | IConfigList {
        return this.adminApi.getConfigList();
    }
}
