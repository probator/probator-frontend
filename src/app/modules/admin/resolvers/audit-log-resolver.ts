import {IAuditLogEvent} from '../interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AdminApiService} from '../services/admin-api.service';
import {Injectable} from '@angular/core';

@Injectable()
export class AuditLogResolver implements Resolve<IAuditLogEvent> {
    constructor(private adminApi: AdminApiService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<IAuditLogEvent> | Promise<IAuditLogEvent> | IAuditLogEvent {
        return this.adminApi.getAuditLogEvent(route.params.auditLogEventId);
    }
}
