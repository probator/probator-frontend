import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ITemplate} from '../interfaces';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class TemplateResolver implements Resolve<ITemplate> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITemplate> | Promise<ITemplate> | ITemplate {
        return this.adminApi.getTemplate(route.params.templateName);
    }
}
