import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ILogEvent} from '../interfaces';
import {Injectable} from '@angular/core';
import {AdminApiService} from '../services/admin-api.service';
import {Observable} from 'rxjs';

@Injectable()
export class LogResolver implements Resolve<ILogEvent> {
    constructor(private adminApi: AdminApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ILogEvent> | Promise<ILogEvent> | ILogEvent {
        return this.adminApi.getLogEvent(route.params.logEventId);
    }
}
