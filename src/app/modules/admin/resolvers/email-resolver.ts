import {IEmail} from '../interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class EmailResolver implements Resolve<IEmail> {
    constructor(private adminApi: AdminApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IEmail> | Promise<IEmail> | IEmail {
        return this.adminApi.getEmail(route.params.emailId);
    }
}
