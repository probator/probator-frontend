import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AdminApiService} from '../services/admin-api.service';
import {IApiKeyList} from '../interfaces';

@Injectable()
export class ApiKeyListResolver implements Resolve<IApiKeyList> {
    constructor(private adminAdpi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IApiKeyList> | Promise<IApiKeyList> | IApiKeyList {
        return this.adminAdpi.getApiKeys();
    }
}
