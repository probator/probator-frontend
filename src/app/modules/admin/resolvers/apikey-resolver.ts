import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AdminApiService} from '../services/admin-api.service';
import {IApiKey} from '../interfaces';

@Injectable()
export class ApiKeyResolver implements Resolve<IApiKey> {
    constructor(private adminApi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IApiKey> | Promise<IApiKey> | IApiKey {
        return this.adminApi.getApiKey(route.params.apiKey);
    }
}
