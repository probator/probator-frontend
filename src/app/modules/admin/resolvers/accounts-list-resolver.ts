import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {IAccountList} from '../../../interfaces';
import {AdminApiService} from '../services/admin-api.service';

@Injectable()
export class AccountsListResolver implements Resolve<IAccountList> {
    constructor(private adminAdpi: AdminApiService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAccountList> | Promise<IAccountList> | IAccountList {
        return this.adminAdpi.getAccounts();
    }
}
