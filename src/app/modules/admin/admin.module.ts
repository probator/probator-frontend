import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminMaterialModule} from './admin.material.module';
import {AdminRoutingModule} from './admin-routing.module';
import {AccountAddComponent} from './components/accounts/account-add/account-add.component';
import {AccountDeleteComponent} from './components/accounts/account-delete/account-delete.component';
import {AccountEditComponent} from './components/accounts/account-edit/account-edit.component';
import {AccountImportExportComponent} from './components/accounts/account-import-export/account-import-export.component';
import {AccountListComponent} from './components/accounts/account-list/account-list.component';
import {AccountPropertyComponent} from './components/accounts/account-property/account-property.component';
import {AccountResolver} from './resolvers/account-resolver';
import {AccountSharedEditorComponent} from './components/accounts/account-shared-editor/account-shared-editor.component';
import {AccountsListResolver} from './resolvers/accounts-list-resolver';
import {AuditLogResolver} from './resolvers/audit-log-resolver';
import {AuditlogsDetailsComponent} from './components/auditlogs/auditlogs-details/auditlogs-details.component';
import {AuditlogsListComponent} from './components/auditlogs/auditlogs-list/auditlogs-list.component';
import {ConfigImportExportComponent} from './components/config/config-import-export/config-import-export.component';
import {ConfigItemAddComponent} from './components/config/config-item-add/config-item-add.component';
import {ConfigItemDeleteComponent} from './components/config/config-item-delete/config-item-delete.component';
import {ConfigItemEditComponent} from './components/config/config-item-edit/config-item-edit.component';
import {ConfigItemEditorChoiceComponent} from './components/config/config-item-editor-choice/config-item-editor-choice.component';
import {ConfigItemEditorListComponent} from './components/config/config-item-editor-list/config-item-editor-list.component';
import {ConfigItemResolver} from './resolvers/config-item-resolver';
import {ConfigItemSharedEditorComponent} from './components/config/config-item-shared-editor/config-item-shared-editor.component';
import {ConfigItemTypePipe} from './pipes/config-item-type.pipe';
import {ConfigListComponent} from './components/config/config-list/config-list.component';
import {ConfigListResolver} from './resolvers/config-list-resolver';
import {ConfigNamespaceAddComponent} from './components/config/config-namespace-add/config-namespace-add.component';
import {ConfigNamespaceDeleteComponent} from './components/config/config-namespace-delete/config-namespace-delete.component';
import {ConfigNamespaceEditComponent} from './components/config/config-namespace-edit/config-namespace-edit.component';
import {ConfigNamespaceListComponent} from './components/config/config-namespace-list/config-namespace-list.component';
import {ConfigNamespaceResolver} from './resolvers/config-namespace-resolver';
import {NamespaceSharedEditorComponent} from './components/config/namespace-shared-editor/namespace-shared-editor.component';
import {ContactEditorComponent} from './components/accounts/contact-editor/contact-editor.component';
import {ContactEditorPopupComponent} from './components/accounts/contact-editor/contact-editor-popup/contact-editor-popup.component';
import {EmailDetailsComponent} from './components/emails/email-details/email-details.component';
import {EmailListComponent} from './components/emails/email-list/email-list.component';
import {EmailResolver} from './resolvers/email-resolver';
import {ImportExportComponent} from './components/shared/import-export/import-export.component';
import {LogDetailsComponent} from './components/logs/log-details/log-details.component';
import {LogListComponent} from './components/logs/log-list/log-list.component';
import {LogResolver} from './resolvers/log-resolver';
import {ReactiveFormsModule} from '@angular/forms';
import {RoleAddComponent} from './components/roles/role-add/role-add.component';
import {RoleDeleteComponent} from './components/roles/role-delete/role-delete.component';
import {RoleEditComponent} from './components/roles/role-edit/role-edit.component';
import {RoleListComponent} from './components/roles/role-list/role-list.component';
import {RoleListResolver} from './resolvers/role-list-resolver';
import {RoleResolver} from './resolvers/role-resolver';
import {RoleSharedEditorComponent} from './components/roles/role-shared-editor/role-shared-editor.component';
import {TemplateAddComponent} from './components/templates/template-add/template-add.component';
import {TemplateDeleteComponent} from './components/templates/template-delete/template-delete.component';
import {TemplateEditComponent} from './components/templates/template-edit/template-edit.component';
import {TemplateListComponent} from './components/templates/template-list/template-list.component';
import {TemplateListResolver} from './resolvers/template-list-resolver';
import {TemplateResolver} from './resolvers/template-resolver';
import {TemplateSharedEditorComponent} from './components/templates/template-shared-editor/template-shared-editor.component';
import {UserAddComponent} from './components/users/user-add/user-add.component';
import {UserAddResolver} from './resolvers/user-add-resolver';
import {UserDeleteComponent} from './components/users/user-delete/user-delete.component';
import {UserEditComponent} from './components/users/user-edit/user-edit.component';
import {UserListComponent} from './components/users/user-list/user-list.component';
import {UserPasswordCreatedComponent} from './components/users/user-password-created/user-password-created.component';
import {UserPasswordResetComponent} from './components/users/user-password-reset/user-password-reset.component';
import {UserResolver} from './resolvers/user-resolver';
import {UserSharedEditorComponent} from './components/users/user-shared-editor/user-shared-editor.component';
import {UtilsModule} from '../utils/utils.module';
import {ColorCompactModule} from 'ngx-color/compact';
import {ColorGithubModule} from 'ngx-color/github';
import {ColorChromeModule} from 'ngx-color/chrome';
import {ApiKeyListComponent} from './components/api-keys/api-key-list/api-key-list.component';
import {ApiKeyListResolver} from './resolvers/apikey-list-resolver';
import {ApiKeyDeleteComponent} from './components/api-keys/api-key-delete/api-key-delete.component';
import {ApiKeyAddComponent} from './components/api-keys/api-key-add/api-key-add.component';
import {ApiKeyCredentialsComponent} from './components/api-keys/api-key-credentials/api-key-credentials.component';

@NgModule({
    declarations: [
        AccountListComponent,
        AccountEditComponent,
        AccountAddComponent,
        ConfigListComponent,
        ConfigItemEditComponent,
        ConfigItemAddComponent,
        UserListComponent,
        UserAddComponent,
        UserEditComponent,
        RoleListComponent,
        RoleEditComponent,
        RoleAddComponent,
        TemplateAddComponent,
        TemplateEditComponent,
        TemplateListComponent,
        EmailListComponent,
        EmailDetailsComponent,
        AuditlogsListComponent,
        AuditlogsDetailsComponent,
        AccountDeleteComponent,
        ContactEditorComponent,
        ContactEditorPopupComponent,
        AccountPropertyComponent,
        AccountSharedEditorComponent,
        ConfigItemSharedEditorComponent,
        ConfigNamespaceListComponent,
        ConfigItemTypePipe,
        ConfigItemEditorChoiceComponent,
        ConfigItemEditorListComponent,
        ConfigNamespaceEditComponent,
        ConfigNamespaceAddComponent,
        NamespaceSharedEditorComponent,
        ConfigNamespaceDeleteComponent,
        ConfigItemDeleteComponent,
        UserDeleteComponent,
        UserSharedEditorComponent,
        UserPasswordCreatedComponent,
        UserPasswordResetComponent,
        RoleSharedEditorComponent,
        RoleDeleteComponent,
        TemplateSharedEditorComponent,
        TemplateDeleteComponent,
        LogListComponent,
        LogDetailsComponent,
        ConfigImportExportComponent,
        AccountImportExportComponent,
        ImportExportComponent,
        ApiKeyListComponent,
        ApiKeyDeleteComponent,
        ApiKeyAddComponent,
        ApiKeyCredentialsComponent,
    ],
    imports: [
        AdminRoutingModule,
        CommonModule,
        AdminMaterialModule,
        UtilsModule,
        ReactiveFormsModule,
        ColorCompactModule,
        ColorGithubModule,
        ColorChromeModule,
    ],
    providers: [
        AccountsListResolver,
        AccountResolver,
        RoleListResolver,
        ConfigListResolver,
        ConfigItemResolver,
        ConfigNamespaceResolver,
        UserAddResolver,
        UserResolver,
        RoleResolver,
        TemplateListResolver,
        TemplateResolver,
        EmailResolver,
        AuditLogResolver,
        LogResolver,
        ApiKeyListResolver,
    ],
    entryComponents: [
        AccountDeleteComponent,
        ContactEditorPopupComponent,
        ConfigItemEditorChoiceComponent,
        ConfigNamespaceDeleteComponent,
        ConfigItemDeleteComponent,
        UserDeleteComponent,
        UserPasswordCreatedComponent,
        UserPasswordResetComponent,
        RoleDeleteComponent,
        TemplateDeleteComponent,
        ConfigImportExportComponent,
        AccountImportExportComponent,
        ApiKeyAddComponent,
        ApiKeyCredentialsComponent,
        ApiKeyDeleteComponent,
    ]
})
export class AdminModule { }
