import {Pipe, PipeTransform} from '@angular/core';
import {UtilsService} from '../../utils/services/utils.service';

@Pipe({
    name: 'configItemType'
})
export class ConfigItemTypePipe implements PipeTransform {
    transform(value: any, args?: any): any {
        const cls = UtilsService.getConfigItemClass(value);

        return `<span class="label ${cls}">${value}</span>`;
    }
}
