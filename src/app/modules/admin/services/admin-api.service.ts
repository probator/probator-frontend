import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IAccount, IAccountList, IRole, IRoleList, IUser} from '../../../interfaces';
import {catchError} from 'rxjs/operators';
import {
    IApiKey, IApiKeyList,
    IAuditLogEvent,
    IAuditLogEventList,
    IConfigItem,
    IConfigList,
    IConfigNamespace,
    IEmail,
    IEmailList,
    ILogEvent,
    ILogEventList,
    ITemplate,
    ITemplateList,
    IUserAddInfo,
    IUserDetails,
    IUserList,
    IUserListParams
} from '../interfaces';
import {UtilsService} from '../../utils/services/utils.service';
import {AppSettings} from '../../../app-settings';

@Injectable({
    providedIn: 'root'
})
export class AdminApiService {
    constructor(private http: HttpClient, private utils: UtilsService) { }

    // region Accounts
    public getAccounts(): Observable<IAccountList> {
        return this.http.get<IAccountList>(`${AppSettings.API_V1}/account`)
            .pipe(catchError((err) => this.utils.handleError(err)));
    }

    public getAccount(accountId): Observable<IAccount> {
        return this.http.get<IAccount>(`${AppSettings.API_V1}/account/${accountId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public addAccount(account: IAccount) {
        return this.http.post(`${AppSettings.API_V1}/account`, account)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public updateAccount(account: IAccount) {
        return this.http.put(`${AppSettings.API_V1}/account/${account.accountId}`, account)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public deleteAccount(account: IAccount) {
        return this.http.delete(`${AppSettings.API_V1}/account/${account.accountId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public exportAccounts() {
        return this.http.get(`${AppSettings.API_V1}/account/imex`, {responseType: 'text'})
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public importAccounts(data: string) {
        return this.http.post(`${AppSettings.API_V1}/account/imex`, {accounts: data})
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Config
    // region Items
    public getConfigList(): Observable<IConfigList> {
        return this.http.get<IConfigList>(`${AppSettings.API_V1}/config`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getConfigItem(namespacePrefix: string, key: string): Observable<IConfigItem> {
        return this.http.get<IConfigItem>(`${AppSettings.API_V1}/config/${namespacePrefix}/${key}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public updateConfigItem(configItem: IConfigItem) {
        return this.http.put(`${AppSettings.API_V1}/config/${configItem.namespacePrefix}/${configItem.key}`, configItem)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public addConfigItem(configItem: IConfigItem) {
        return this.http.post(`${AppSettings.API_V1}/config`, configItem)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public deleteConfigItem(configItem: IConfigItem) {
        return this.http.delete(`${AppSettings.API_V1}/config/${configItem.namespacePrefix}/${configItem.key}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Namespaces
    public getConfigNamespace(namespace: string): Observable<IConfigNamespace> {
        return this.http.get<IConfigNamespace>(`${AppSettings.API_V1}/namespace/${namespace}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public createConfigNamespace(namespace: IConfigNamespace) {
        return this.http.post(`${AppSettings.API_V1}/namespace`, namespace)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public updateConfigNamespace(namespace: IConfigNamespace) {
        return this.http.put(`${AppSettings.API_V1}/namespace/${namespace.namespacePrefix}`, namespace)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public deleteConfigNamespace(namespace: IConfigNamespace) {
        return this.http.delete(`${AppSettings.API_V1}/namespace/${namespace.namespacePrefix}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    public exportConfig() {
        return this.http.get(`${AppSettings.API_V1}/config/imex`, {responseType: 'text'})
            .pipe(catchError(err => this.utils.handleError((err))));
    }

    public importConfig(config: string) {
        return this.http.post(`${AppSettings.API_V1}/config/imex`, {config: config})
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Users
    public getUsers(params: IUserListParams): Observable<IUserList> {
        return this.http.get<IUserList>(`${AppSettings.API_V1}/users`, {params: params as unknown as HttpParams})
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getUser(userId: number): Observable<IUserDetails> {
        return this.http.get<IUserDetails>(`${AppSettings.API_V1}/user/${userId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getUserAddInfo(): Observable<IUserAddInfo> {
        return this.http.options<IUserAddInfo>(`${AppSettings.API_V1}/users`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public addUser(user: IUser) {
        return this.http.post(`${AppSettings.API_V1}/users`, user)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public updateUser(userId: number, user: IUser) {
        return this.http.put(`${AppSettings.API_V1}/user/${userId}`, user)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public resetPassword(user: IUser) {
        return this.http.put(`${AppSettings.API_V1}/user/password/${user.userId}`, {})
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public deleteUser(user: IUser) {
        return this.http.delete(`${AppSettings.API_V1}/user/${user.userId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Roles
    public getRoles(): Observable<IRoleList> {
        return this.http.get<IRoleList>(`${AppSettings.API_V1}/roles`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getRole(roleId: number): Observable<IRole> {
        return this.http.get<IRole>(`${AppSettings.API_V1}/role/${roleId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public createRole(role: IRole) {
        return this.http.post(`${AppSettings.API_V1}/roles`, role)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public updateRole(role: IRole) {
        return this.http.put(`${AppSettings.API_V1}/role/${role.roleId}`, role)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public deleteRole(role: IRole) {
        return this.http.delete(`${AppSettings.API_V1}/role/${role.roleId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Templates
    public getTemplates(): Observable<ITemplateList> {
        return this.http.get<ITemplateList>(`${AppSettings.API_V1}/templates`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getTemplate(name: string): Observable<ITemplate> {
        return this.http.get<ITemplate>(`${AppSettings.API_V1}/template/${name}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public createTemplate(template: ITemplate) {
        return this.http.post(`${AppSettings.API_V1}/templates`, template)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public updateTemplate(template: ITemplate) {
        return this.http.put(`${AppSettings.API_V1}/template/${template.templateName}`, template)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public deleteTemplate(name: string) {
        return this.http.delete(`${AppSettings.API_V1}/template/${name}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Emails
    public getEmails(params): Observable<IEmailList> {
        return this.http.get<IEmailList>(`${AppSettings.API_V1}/emails`, {params: params as unknown as HttpParams})
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getEmail(emailId: number): Observable<IEmail> {
        return this.http.get<IEmail>(`${AppSettings.API_V1}/emails/${emailId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public resendEmail(emailId: number) {
        return this.http.put(`${AppSettings.API_V1}/emails/${emailId}`, {})
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Audit Log
    public getAuditLogEvents(params): Observable<IAuditLogEventList> {
        return this.http.get<IAuditLogEventList>(`${AppSettings.API_V1}/auditlog`, {params: params as unknown as HttpParams})
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getAuditLogEvent(auditLogEventId): Observable<IAuditLogEvent> {
        return this.http.get<IAuditLogEvent>(`${AppSettings.API_V1}/auditlog/${auditLogEventId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region Logs
    public getLogEvents(params): Observable<ILogEventList> {
        return this.http.get<ILogEventList>(`${AppSettings.API_V1}/logs`, {params: params as unknown as HttpParams})
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getLogEvent(logEventId): Observable<ILogEvent> {
        return this.http.get<ILogEvent>(`${AppSettings.API_V1}/logs/${logEventId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion

    // region API Keys
    public getApiKey(apiKey: string): Observable<IApiKey> {
        return this.http.get<IApiKey>(`${AppSettings.API_V1}/api-key/${apiKey}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public getApiKeys(): Observable<IApiKeyList> {
        return this.http.get<IApiKeyList>(`${AppSettings.API_V1}/api-keys`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public deleteApiKey(apiKey: IApiKey) {
        return this.http.delete(`${AppSettings.API_V1}/api-key/${apiKey.apiKeyId}`)
            .pipe(catchError(err => this.utils.handleError(err)));
    }

    public createApiKey(apiKey: IApiKey): Observable<IApiKey> {
        return this.http.post(`${AppSettings.API_V1}/api-keys`, apiKey)
            .pipe(catchError(err => this.utils.handleError(err)));
    }
    // endregion
}
