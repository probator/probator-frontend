import {TestBed} from '@angular/core/testing';
import {AdminApiService} from './admin-api.service';
import {HttpClientModule} from '@angular/common/http';
import {UtilsModule} from '../../utils/utils.module';

describe('AdminApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            HttpClientModule,
            UtilsModule,
        ]
    }));

    it('should be created', () => {
        const service: AdminApiService = TestBed.inject(AdminApiService);
        expect(service).toBeTruthy();
    });
});
