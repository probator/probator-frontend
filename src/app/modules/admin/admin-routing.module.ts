import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountListComponent} from './components/accounts/account-list/account-list.component';
import {AccountsListResolver} from './resolvers/accounts-list-resolver';
import {AccountAddComponent} from './components/accounts/account-add/account-add.component';
import {RoleListResolver} from './resolvers/role-list-resolver';
import {AccountEditComponent} from './components/accounts/account-edit/account-edit.component';
import {AccountResolver} from './resolvers/account-resolver';
import {ConfigListComponent} from './components/config/config-list/config-list.component';
import {ConfigListResolver} from './resolvers/config-list-resolver';
import {ConfigItemAddComponent} from './components/config/config-item-add/config-item-add.component';
import {ConfigItemEditComponent} from './components/config/config-item-edit/config-item-edit.component';
import {ConfigItemResolver} from './resolvers/config-item-resolver';
import {ConfigNamespaceAddComponent} from './components/config/config-namespace-add/config-namespace-add.component';
import {ConfigNamespaceEditComponent} from './components/config/config-namespace-edit/config-namespace-edit.component';
import {ConfigNamespaceResolver} from './resolvers/config-namespace-resolver';
import {UserListComponent} from './components/users/user-list/user-list.component';
import {UserAddComponent} from './components/users/user-add/user-add.component';
import {UserAddResolver} from './resolvers/user-add-resolver';
import {UserEditComponent} from './components/users/user-edit/user-edit.component';
import {UserResolver} from './resolvers/user-resolver';
import {RoleListComponent} from './components/roles/role-list/role-list.component';
import {RoleAddComponent} from './components/roles/role-add/role-add.component';
import {RoleEditComponent} from './components/roles/role-edit/role-edit.component';
import {RoleResolver} from './resolvers/role-resolver';
import {TemplateListComponent} from './components/templates/template-list/template-list.component';
import {TemplateListResolver} from './resolvers/template-list-resolver';
import {TemplateAddComponent} from './components/templates/template-add/template-add.component';
import {TemplateEditComponent} from './components/templates/template-edit/template-edit.component';
import {TemplateResolver} from './resolvers/template-resolver';
import {EmailListComponent} from './components/emails/email-list/email-list.component';
import {EmailDetailsComponent} from './components/emails/email-details/email-details.component';
import {EmailResolver} from './resolvers/email-resolver';
import {AuditlogsListComponent} from './components/auditlogs/auditlogs-list/auditlogs-list.component';
import {AuditlogsDetailsComponent} from './components/auditlogs/auditlogs-details/auditlogs-details.component';
import {AuditLogResolver} from './resolvers/audit-log-resolver';
import {LogListComponent} from './components/logs/log-list/log-list.component';
import {LogDetailsComponent} from './components/logs/log-details/log-details.component';
import {LogResolver} from './resolvers/log-resolver';
import {ApiKeyListComponent} from './components/api-keys/api-key-list/api-key-list.component';
import {ApiKeyListResolver} from './resolvers/apikey-list-resolver';

const routes: Routes = [
    {
        path: 'accounts',
        children: [
            {path: '', component: AccountListComponent, pathMatch: 'full', resolve: {accounts: AccountsListResolver}},
            {path: 'add', component: AccountAddComponent, resolve: {roles: RoleListResolver}},
            {path: 'edit/:accountId', component: AccountEditComponent, resolve: {account: AccountResolver, roles: RoleListResolver}},
        ]
    },
    {
        path: 'config',
        children: [
            {path: '', component: ConfigListComponent, pathMatch: 'full', resolve: {config: ConfigListResolver}},
            {path: 'add/:namespacePrefix', component: ConfigItemAddComponent},
            {path: 'edit/:namespacePrefix/:key', component: ConfigItemEditComponent, resolve: {configItem: ConfigItemResolver}},
            {
                path: 'namespace',
                children: [
                    {path: 'add', component: ConfigNamespaceAddComponent},
                    {path: 'edit/:namespacePrefix', component: ConfigNamespaceEditComponent, resolve: {namespace: ConfigNamespaceResolver}},
                ]
            },
        ]
    },
    {
        path: 'users',
        children: [
            {path: '', component: UserListComponent, pathMatch: 'full'},
            {path: 'add', component: UserAddComponent, resolve: {info: UserAddResolver}},
            {path: 'edit/:userId', component: UserEditComponent, resolve: {info: UserResolver}},
        ]
    },
    {
        path: 'roles',
        children: [
            {path: '', component: RoleListComponent, pathMatch: 'full', resolve: {roles: RoleListResolver}},
            {path: 'add', component: RoleAddComponent},
            {path: 'edit/:roleId', component: RoleEditComponent, resolve: {role: RoleResolver}},
        ]
    },
    {
        path: 'templates',
        children: [
            {path: '', component: TemplateListComponent, pathMatch: 'full', resolve: {templates: TemplateListResolver}},
            {path: 'add', component: TemplateAddComponent},
            {path: 'edit/:templateName', component: TemplateEditComponent, resolve: {template: TemplateResolver}},
        ]
    },
    {
        path: 'emails',
        children: [
            {path: '', component: EmailListComponent, pathMatch: 'full'},
            {path: 'details/:emailId', component: EmailDetailsComponent, resolve: {email: EmailResolver}},
        ]
    },
    {
        path: 'auditlog',
        children: [
            {path: '', component: AuditlogsListComponent, pathMatch: 'full'},
            {path: 'details/:auditLogEventId', component: AuditlogsDetailsComponent, resolve: {auditLogEvent: AuditLogResolver}}
        ]
    },
    {
        path: 'logs',
        children: [
            {path: '', component: LogListComponent, pathMatch: 'full'},
            {path: 'details/:logEventId', component: LogDetailsComponent, resolve: {logEvent: LogResolver}}
        ],
    },
    {
        path: 'apikeys',
        children: [
            {path: '', component: ApiKeyListComponent, pathMatch: 'full', resolve: {apiKeys: ApiKeyListResolver}},
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule { }
