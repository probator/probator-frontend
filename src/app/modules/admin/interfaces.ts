import {BaseResponse, IAccountContact, IRole, IUser} from '../../interfaces';

export interface IConfigList extends BaseResponse {
    namespaces: IConfigNamespace[];
}

export interface IConfigNamespace {
    name: string;
    namespacePrefix: string;
    sortOrder: number;
    configItems: IConfigItem[];
}

export interface IConfigItem {
    configItemId: number;
    description: string;
    key: string;
    namespacePrefix: string;
    type: 'string' | 'int' | 'float' | 'array' | 'json' | 'bool' | 'choice';
    value: any;
}

export interface IConfigChoice {
    available: string[];
    enabled: string[];
    max_items: number;
    min_items: number;
}

export interface IUserListParams {
    page?: number;
    count?: number;
    authSystem?: string[];
}

export interface IUserList extends BaseResponse {
    userCount: number;
    users: IUser[];
    activeAuthSystem: string;
    authSystems: string[];
}

export interface IUserAddInfo extends BaseResponse {
    roles: IRole[];
    authSystems: string[];
    activeAuthSystem: string;
}

export interface IUserCreateInfo extends BaseResponse {
    user: IUser;
    password?: string;
}

export interface IUserDetails extends BaseResponse {
    user: IUser;
    roles: IRole[];
}

export interface ITemplate {
    templateName: string;
    template: string;
}

export interface ITemplateList extends BaseResponse {
    templateCount: number;
    templates: ITemplate[];
}

export interface IEmail {
    emailId: number;
    timestamp: Date;
    subsystem: string;
    subject: string;
    sender: string;
    recipients: IAccountContact[];
    uuid: string;
    messageHtml?: string;
    messageText?: string;

}

export interface IEmailList extends BaseResponse {
    emailCount: number;
    emails: IEmail[];
    subsystems: string[];
}

export interface IAuditLogEvent {
    auditLogEventId: number;
    timestamp: Date;
    actor: string;
    event: string;
    data: object;
}

export interface IAuditLogEventList extends BaseResponse {
    auditLogEventCount: number;
    auditLogEvents: IAuditLogEvent[];
}

export interface ILogEvent {
    logEventId: number;
    level: string;
    levelno: number;
    timestamp: Date;
    message: string;
    module: string;
    filename: string;
    lineno: number;
    funcname: string;
    pathname: string;
    processId: number;
    stacktrace: string;
}

export interface ILogEventList extends BaseResponse {
    logEventCount: number;
    logEvents: ILogEvent[];
}

export interface IApiKey {
    apiKeyId: string;
    secretKey: string;
    created: Date;
    lastUsed: Date;
    description: string;
    roles: IRole[];
}

export interface IApiKeyList extends BaseResponse {
    apiKeys: IApiKey[];
}

export interface IApiKeyCreateResult extends BaseResponse {
    apiKey: IApiKey;
    secretKey: string;
}
