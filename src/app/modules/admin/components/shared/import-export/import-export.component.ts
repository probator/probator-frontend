import {Component, ElementRef, ViewChild} from '@angular/core';
import {Upload} from '../../../classes/upload';
import {AppSettings} from '../../../../../app-settings';
import {AdminApiService} from '../../../services/admin-api.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {FormControl, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';
import {ValidateJSON} from '../../../validators/json-validator';

@Component({
    selector: 'app-import-export',
    templateUrl: './import-export.component.html',
    styleUrls: ['./import-export.component.scss']
})
export class ImportExportComponent {
    currentUpload: Upload;
    dropzoneActive = false;
    exportControl = new FormControl();
    importControl = new FormControl(null, {validators: [ValidateJSON, Validators.required]});
    title = 'Import / Export';

    @ViewChild('dropBox', { read: ElementRef, static: true }) dropBox: ElementRef;

    constructor(
        protected adminApi: AdminApiService,
        protected utils: UtilsService,
        protected dialogRef: MatDialogRef<ImportExportComponent>,
    ) { }

    download() {
        this.doExport();
    }

    prepareImport() {
        const value = this.dropBox.nativeElement.value;
        if (this.utils.validateJson(value)) {
            this.doImport(value);
        }
    }

    doImport(data: string) { }

    doExport() { }

    dropzoneState($event) {
        this.dropzoneActive = $event;
    }

    handleDrop(fileList: FileList) {
        if (fileList.length !== 1) {
            this.utils.showError('You can only upload a single file');
            return;
        }

        const file = fileList[0];

        if (file.type !== 'application/json') {
            this.utils.showError('Only JSON files are supported');
            return;
        }

        if (file.size > AppSettings.MaxUploadFileSize) {
            const mb = AppSettings.MaxUploadFileSize / 1024 / 1024;
            this.utils.showError(`File size must be less than ${mb}MB`);
            return;
        }

        this.currentUpload = new Upload(file);
        this.currentUpload.upload().subscribe(data => {
            if (data) {
                this.importControl.setValue(this.currentUpload.data);
            }
        });
    }

    close() {
        this.dialogRef.close();
    }
}
