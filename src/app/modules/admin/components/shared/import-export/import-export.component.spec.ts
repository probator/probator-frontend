import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ImportExportComponent} from './import-export.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {UtilsModule} from '../../../../utils/utils.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

describe('ImportExportComponent', () => {
    let component: ImportExportComponent;
    let fixture: ComponentFixture<ImportExportComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ImportExportComponent ],
            imports: [
                NoopAnimationsModule,
                HttpClientModule,
                UtilsModule,
                AdminMaterialModule,
                ReactiveFormsModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ImportExportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
