import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IApiKey} from '../../../interfaces';
import {ApiKeyListComponent} from '../api-key-list/api-key-list.component';

@Component({
  selector: 'app-api-key-delete',
  templateUrl: './api-key-delete.component.html',
  styleUrls: ['./api-key-delete.component.scss']
})
export class ApiKeyDeleteComponent {
    apikey: IApiKey;

    constructor(public dialogRef: MatDialogRef<ApiKeyListComponent>, @Inject(MAT_DIALOG_DATA) apikey: IApiKey) {
        this.apikey = apikey;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
