import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ApiKeyDeleteComponent} from './api-key-delete.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('ApiKeyDeleteComponent', () => {
    let component: ApiKeyDeleteComponent;
    let fixture: ComponentFixture<ApiKeyDeleteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ApiKeyDeleteComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: {
                        apiKey: {
                            apiKeyId: 'API_KEY_ID',
                            secretKey: 'API_SECRET_KEY',
                            created: '2020-01-23T18:40:18',
                            lastUsed: '2020-01-28T16:24:38',
                            description: 'Test API Key',
                            roles: [
                                {roleId: 1, 'name': 'Admin', 'color': '#BD0000'},
                                {roleId: 3, 'name': 'User', 'color': '#008000'}
                            ]
                        },
                        secretKey: 'ClearTextSecretKey'
                    }
                },
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ApiKeyDeleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
