import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ApiKeyListComponent} from '../api-key-list/api-key-list.component';
import {IApiKey, IApiKeyCreateResult} from '../../../interfaces';

@Component({
    selector: 'app-api-key-credentials',
    templateUrl: './api-key-credentials.component.html',
    styleUrls: ['./api-key-credentials.component.scss']
})
export class ApiKeyCredentialsComponent {
    apikey: IApiKey;
    secretKey:  string;

    constructor(public dialogRef: MatDialogRef<ApiKeyListComponent>, @Inject(MAT_DIALOG_DATA) createData: IApiKeyCreateResult) {
        this.apikey = createData.apiKey;
        this.secretKey = createData.secretKey;
    }

    close(): void {
        this.dialogRef.close();
    }
}
