import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ApiKeyAddComponent} from './api-key-add.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('ApiKeyAddComponent', () => {
    let component: ApiKeyAddComponent;
    let fixture: ComponentFixture<ApiKeyAddComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ApiKeyAddComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: [
                        {roleId: 1, name: 'Admin', color: '#BD0000'},
                        {roleId: 2, name: 'NOC', color: '#5B5BFF'},
                        {roleId: 3, name: 'User', color: '#008000'}
                    ]
                },
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ApiKeyAddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
