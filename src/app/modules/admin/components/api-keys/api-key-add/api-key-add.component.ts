import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {IRole} from '../../../../../interfaces';
import {ApiKeyListComponent} from '../api-key-list/api-key-list.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AdminApiService} from '../../../services/admin-api.service';
import {Observable} from 'rxjs';
import {ENTER} from '@angular/cdk/keycodes';
import {MatAutocomplete} from '@angular/material/autocomplete';
import {map, startWith} from 'rxjs/operators';
import {UtilsService} from '../../../../utils/services/utils.service';

@Component({
    selector: 'app-api-key-add',
    templateUrl: './api-key-add.component.html',
    styleUrls: ['./api-key-add.component.scss']
})
export class ApiKeyAddComponent implements OnInit {
    apikeyForm: FormGroup;
    allRoles: string[];
    roleCtrl = new FormControl();
    filteredRoles: Observable<string[]>;

    readonly separatorKeyCodes: number[] = [ENTER];

    @ViewChild('roleInput', { static: true }) roleInput: ElementRef<HTMLInputElement>;
    @ViewChild('roleAuto', { static: true }) roleAuto: MatAutocomplete;

    constructor(
        public dialogRef: MatDialogRef<ApiKeyListComponent>,
        @Inject(MAT_DIALOG_DATA) roles: IRole[],
        private fb: FormBuilder,
        private adminApi: AdminApiService,
        private utils: UtilsService,
        private dialog: MatDialog,
    ) {
        this.allRoles = roles.map(role => role.name);
        this.apikeyForm = fb.group(
            {
                description: ['', Validators.required],
                roles: [null, Validators.required],
            }
        );
    }

    ngOnInit() {
        this.filteredRoles = this.roleCtrl.valueChanges.pipe(
            startWith(null),
            map((role: string | null) => role ? this._filter(role) : this.allRoles)
        );
    }

    create() {
        this.adminApi.createApiKey(this.apikeyForm.value).subscribe(result => {
            this.dialogRef.close(result);
        });
    }

    cancel() {
        this.dialogRef.close();
    }

    addRole(event) {
        if (!this.roleAuto.isOpen) {
            const input = event.input;
            const value = event.value;

            if ((value || '').trim()) {
                if (this.allRoles.indexOf(value) === -1) {
                    this.utils.showError(`Invalid role ${value}`);
                    return;
                }

                const roles: string[] = this.apikeyForm.get('roles').value || [];
                roles.push(value);
                this.apikeyForm.get('roles').setValue(roles);
            }

            if (input) {
                input.value = '';
            }
        }
    }

    selectedRole($event) {
        const roles: string[] = this.apikeyForm.get('roles').value || [];
        roles.push($event.option.viewValue);
        this.apikeyForm.get('roles').setValue(roles);
        this.roleInput.nativeElement.value = '';
        this.roleCtrl.setValue(null);
    }

    protected _filter(roleFilter: string): string[] {
        const filterValue = roleFilter.toLowerCase();

        return this.allRoles.filter(role => role.toLowerCase().indexOf(filterValue) === 0);
    }

    removeRole(role: IRole) {
        const idx = this.apikeyForm.controls.roles.value.indexOf(role);

        if (idx >= 0) {
            this.apikeyForm.controls.roles.value.splice(idx, 1);
        }
    }
}
