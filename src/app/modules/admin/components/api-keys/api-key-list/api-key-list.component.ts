import {Component, OnInit} from '@angular/core';
import {IApiKey} from '../../../interfaces';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilsService} from '../../../../utils/services/utils.service';
import {AdminApiService} from '../../../services/admin-api.service';
import {MatDialog} from '@angular/material/dialog';
import {ApiKeyDeleteComponent} from '../api-key-delete/api-key-delete.component';
import {ApiKeyAddComponent} from '../api-key-add/api-key-add.component';
import {ApiKeyCredentialsComponent} from '../api-key-credentials/api-key-credentials.component';

@Component({
    selector: 'app-api-key-list',
    templateUrl: './api-key-list.component.html',
    styleUrls: ['./api-key-list.component.scss']
})
export class ApiKeyListComponent implements OnInit {
    apiKeys: IApiKey[];
    columns = ['apiKeyId', 'description', 'created', 'lastUsed', 'roles', 'actions'];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private utils: UtilsService,
        private adminApi: AdminApiService,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.apiKeys = this.route.snapshot.data.apiKeys.apiKeys;
    }

    delete(apiKey: IApiKey) {
        const confirm = this.dialog.open(ApiKeyDeleteComponent, {
            data: apiKey,
            width: '800px'
        });

        confirm.afterClosed().subscribe(doDelete => {
            if (doDelete) {
                this.adminApi.deleteApiKey(apiKey).subscribe(data => {
                    for (const key of this.apiKeys) {
                        if (key === apiKey) {
                            this.apiKeys = this.apiKeys.filter(x => x !== apiKey);
                        }
                    }
                    this.utils.showMessage(data['message']);
                });
            }
        });
    }

    create() {
        this.adminApi.getRoles().subscribe(data => {
            const createDialog = this.dialog.open(ApiKeyAddComponent, {
                data: data.roles,
                width: '800px'
            });

            createDialog.afterClosed().subscribe(createData => {
                if (createData) {
                    const credsDialog = this.dialog.open(ApiKeyCredentialsComponent, {
                        data: createData,
                        width: '800px'
                    });

                    credsDialog.afterClosed().subscribe(() => {
                        this.apiKeys = [...this.apiKeys, createData.apiKey];
                        this.utils.showMessage(`Created API Key`);
                    });
                }
            });
        });
    }
}
