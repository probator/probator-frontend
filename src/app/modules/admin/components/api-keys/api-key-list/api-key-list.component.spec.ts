import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ApiKeyListComponent} from './api-key-list.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';

describe('ApiKeyListComponent', () => {
    let component: ApiKeyListComponent;
    let fixture: ComponentFixture<ApiKeyListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ApiKeyListComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                message: null,
                                apiKeys: [
                                    {
                                        apiKeyId: 'API_KEY_ID',
                                        secretKey: 'API_SECRET_KEY',
                                        created: '2020-01-23T18:40:18',
                                        lastUsed: '2020-01-28T16:24:38',
                                        description: 'Test API key',
                                        roles: [
                                            {
                                                'roleId': 1,
                                                'name': 'Admin',
                                                'color': '#BD0000'
                                            },
                                            {
                                                'roleId': 3,
                                                'name': 'User',
                                                'color': '#008000'
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    }
                },
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ApiKeyListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
