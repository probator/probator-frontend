import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IAuditLogEvent, IAuditLogEventList} from '../../../interfaces';
import {MatPaginator} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {AdminApiService} from '../../../services/admin-api.service';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {Observable, of} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-auditlogs-list',
    templateUrl: './auditlogs-list.component.html',
    styleUrls: ['./auditlogs-list.component.scss']
})
export class AuditlogsListComponent implements OnInit {
    auditLogEventCount = 0;
    auditLogEvents: IAuditLogEvent[];
    displayedColumns = ['timestamp', 'event', 'actor', 'actions'];
    loading = false;

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild('top', { read: ElementRef, static: true }) pageTop: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private adminApi: AdminApiService,
        private metadata: MetadataService,
        private router: Router,
        public utils: UtilsService
    ) { }

    ngOnInit() {
        const page = this.route.snapshot.queryParams.page;
        if (page) {
            this.paginator.pageIndex = page;
        }

        const limit = this.route.snapshot.queryParams.limit;
        if (limit) {
            this.paginator.pageSize = limit;
        }

        this.paginator.page
            .pipe(
                switchMap(() => {
                    const resources = this.loadEvents();
                    this.updateQueryParams();
                    return resources;
                }),
                catchError(this.handleError)
            )
            .subscribe(data => this.updateEvents(data))
        ;

        this.loadEvents().subscribe(data => {
            this.updateEvents(data);
        });
    }

    loadEvents(): Observable<IAuditLogEventList> {
        this.loading = true;
        return this.adminApi.getAuditLogEvents(
            {
                count: this.paginator.pageSize || 25,
                page: this.paginator.pageIndex
            }
        );
    }

    updateEvents(data: IAuditLogEventList) {
        this.loading = false;
        if (data) {
            this.auditLogEventCount = data.auditLogEventCount;
            this.auditLogEvents = data.auditLogEvents;
        }

        this.pageTop.nativeElement.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    }

    updateQueryParams(): void {
        const queryParams: any = {};

        if (this.paginator.pageSize !== 25) {
            queryParams.limit = this.paginator.pageSize;
        }

        if (this.paginator.pageIndex !== 0) {
            queryParams.page = this.paginator.pageIndex;
        }

        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: queryParams
        }).then();
    }

    private handleError(err): Observable<any> {
        this.loading = false;
        this.utils.showError(err);
        return of([]);
    }
}
