import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuditlogsListComponent} from './auditlogs-list.component';
import {ActivatedRoute} from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {AdminMaterialModule} from '../../../admin.material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';

describe('AuditlogsListComponent', () => {
    let component: AuditlogsListComponent;
    let fixture: ComponentFixture<AuditlogsListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AuditlogsListComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            queryParams: {
                                page: 0,
                                limit: 25
                            },
                            data: {
                                auditLogEvents: {
                                    auditLogEventCount: 1,
                                    auditLogEvents: [{
                                        auditLogEventId: 232,
                                        timestamp: '2018-12-05T16:55:32',
                                        actor: 'admin',
                                        event: 'user.passwordReset',
                                        data: {
                                            password: '*REDACTED*'
                                        }
                                    }]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AuditlogsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
