import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuditlogsDetailsComponent} from './auditlogs-details.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';

describe('AuditlogsDetailsComponent', () => {
    let component: AuditlogsDetailsComponent;
    let fixture: ComponentFixture<AuditlogsDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AuditlogsDetailsComponent ],
            imports: [
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                auditLogEvent: {
                                    auditLogEvent: {
                                        auditLogEventId: 232,
                                        timestamp: '2018-12-05T16:55:32',
                                        actor: 'admin',
                                        event: 'user.passwordReset',
                                        data: {
                                            password: '*REDACTED*'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AuditlogsDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
