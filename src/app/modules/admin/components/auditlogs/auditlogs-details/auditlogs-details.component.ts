import {Component, OnInit} from '@angular/core';
import {IAuditLogEvent} from '../../../interfaces';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'app-auditlogs-details',
    templateUrl: './auditlogs-details.component.html',
    styleUrls: ['./auditlogs-details.component.scss']
})
export class AuditlogsDetailsComponent implements OnInit {
    auditLogEvent: IAuditLogEvent;

    constructor(private route: ActivatedRoute, private location: Location) { }

    ngOnInit() {
        this.auditLogEvent = this.route.snapshot.data.auditLogEvent.auditLogEvent;
    }

    formatData() {
        return JSON.stringify(this.auditLogEvent.data, null, 4);
    }

    back() {
        this.location.back();
    }
}
