import {Component, OnInit} from '@angular/core';
import {TemplateSharedEditorComponent} from '../template-shared-editor/template-shared-editor.component';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-template-add',
    templateUrl: '../template-shared-editor/template-shared-editor.component.html',
    styleUrls: ['../template-shared-editor/template-shared-editor.component.scss']
})
export class TemplateAddComponent extends TemplateSharedEditorComponent implements OnInit {
    ngOnInit() {
        this.buttonText = 'Add Template';
        this.templateForm = this.fb.group({
            templateName: ['', Validators.required],
            template: ['', Validators.required],
        });
    }

    save() {
        this.adminApi.createTemplate(this.templateForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../'], {relativeTo: this.route}).then();
            }
        );
    }
}
