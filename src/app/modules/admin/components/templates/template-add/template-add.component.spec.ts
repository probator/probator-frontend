import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TemplateAddComponent} from './template-add.component';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';

describe('TemplateAddComponent', () => {
    let component: TemplateAddComponent;
    let fixture: ComponentFixture<TemplateAddComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TemplateAddComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(TemplateAddComponent);
        component = fixture.componentInstance;
        component.templateForm = fb.group({
            templateName: ['', Validators.required],
            template: ['', Validators.required],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
