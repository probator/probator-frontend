import {Component, OnInit} from '@angular/core';
import {TemplateSharedEditorComponent} from '../template-shared-editor/template-shared-editor.component';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-template-edit',
    templateUrl: '../template-shared-editor/template-shared-editor.component.html',
    styleUrls: ['../template-shared-editor/template-shared-editor.component.scss']
})
export class TemplateEditComponent extends TemplateSharedEditorComponent implements OnInit {
    ngOnInit() {
        const template = this.route.snapshot.data.template.template;
        this.buttonText = 'Update Template';
        this.templateForm = this.fb.group({
            templateName: [{value: template.templateName, disabled: true}, Validators.required],
            template: [template.template, Validators.required],
        });
    }

    save() {
        this.adminApi.updateTemplate(this.templateForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../../'], {relativeTo: this.route}).then();
            }
        );
    }
}
