import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TemplateEditComponent} from './template-edit.component';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';

describe('TemplateEditComponent', () => {
    let component: TemplateEditComponent;
    let fixture: ComponentFixture<TemplateEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TemplateEditComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                template: {
                                    template: {
                                        templateName: 'template.txt',
                                        template: 'My Template Here',
                                        isModified: 0
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(TemplateEditComponent);
        component = fixture.componentInstance;
        component.templateForm = fb.group({
            templateName: ['', Validators.required],
            template: ['', Validators.required],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
