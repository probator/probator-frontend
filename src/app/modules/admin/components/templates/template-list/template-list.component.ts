import {Component, OnInit} from '@angular/core';
import {ITemplate} from '../../../interfaces';
import {ActivatedRoute} from '@angular/router';
import {AdminApiService} from '../../../services/admin-api.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {MatDialog} from '@angular/material/dialog';
import {TemplateDeleteComponent} from '../template-delete/template-delete.component';

@Component({
    selector: 'app-template-list',
    templateUrl: './template-list.component.html',
    styleUrls: ['./template-list.component.scss']
})
export class TemplateListComponent implements OnInit {
    templates: ITemplate[];
    displayedColumns = ['name', 'template', 'actions'];

    constructor(
        private adminApi: AdminApiService,
        private utils: UtilsService,
        private route: ActivatedRoute,
        private dialogRef: MatDialog,
    ) { }

    ngOnInit() {
        this.templates = this.route.snapshot.data.templates.templates;
    }

    delete(template) {
        const confirm = this.dialogRef.open(TemplateDeleteComponent, {
            data: template,
            width: '600px'
        });

        confirm.afterClosed().subscribe(
            doDelete => {
                if (doDelete) {
                    this.adminApi.deleteTemplate(template.templateName).subscribe(
                        data => {
                            const idx = this.templates.indexOf(template);

                            if (idx !== -1) {
                                this.utils.showMessage(data['message']);
                                this.templates.splice(idx, 1);
                                this.templates = [...this.templates];
                            }
                        }
                    );
                }
            }
        );
    }
}
