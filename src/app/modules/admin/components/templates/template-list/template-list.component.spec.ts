import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TemplateListComponent} from './template-list.component';
import {ActivatedRoute} from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {HttpClientModule} from '@angular/common/http';

describe('TemplateListComponent', () => {
    let component: TemplateListComponent;
    let fixture: ComponentFixture<TemplateListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TemplateListComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                templates: {
                                    templateCount: 1,
                                    templates: [
                                        {
                                            templateName: 'template.txt',
                                            template: 'My Template Here',
                                            isModified: 0
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TemplateListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
