import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TemplateSharedEditorComponent} from './template-shared-editor.component';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';

describe('TemplateSharedEditorComponent', () => {
    let component: TemplateSharedEditorComponent;
    let fixture: ComponentFixture<TemplateSharedEditorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TemplateSharedEditorComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(TemplateSharedEditorComponent);
        component = fixture.componentInstance;
        component.templateForm = fb.group({
            templateName: ['', Validators.required],
            template: ['', Validators.required],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
