import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AdminApiService} from '../../../services/admin-api.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {throwError} from 'rxjs';

@Component({
    selector: 'app-template-shared-editor',
    templateUrl: './template-shared-editor.component.html',
    styleUrls: ['./template-shared-editor.component.scss']
})
export class TemplateSharedEditorComponent {
    templateForm: FormGroup;
    buttonText: string;

    constructor(
        protected fb: FormBuilder,
        protected adminApi: AdminApiService,
        protected utils: UtilsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected location: Location,
        protected dialogRef: MatDialog,
    ) { }

    save() {
        throwError('You must override the save method');
    }

    cancel() {
        this.location.back();
    }

}
