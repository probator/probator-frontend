import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ITemplate} from '../../../interfaces';
import {TemplateListComponent} from '../template-list/template-list.component';

@Component({
    selector: 'app-template-delete',
    templateUrl: './template-delete.component.html',
    styleUrls: ['./template-delete.component.scss']
})
export class TemplateDeleteComponent {
    template: ITemplate;

    constructor(public dialogRef: MatDialogRef<TemplateListComponent>, @Inject(MAT_DIALOG_DATA) template: ITemplate) {
        this.template = template;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }

}
