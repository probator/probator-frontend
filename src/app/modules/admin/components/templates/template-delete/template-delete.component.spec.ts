import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TemplateDeleteComponent} from './template-delete.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AdminMaterialModule} from '../../../admin.material.module';
import {HttpClientModule} from '@angular/common/http';

describe('TemplateDeleteComponent', () => {
    let component: TemplateDeleteComponent;
    let fixture: ComponentFixture<TemplateDeleteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ TemplateDeleteComponent ],
            imports: [
                HttpClientModule,
                AdminMaterialModule
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TemplateDeleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
