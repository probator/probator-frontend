import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserSharedEditorComponent} from './user-shared-editor.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';

describe('UserSharedEditorComponent', () => {
    let component: UserSharedEditorComponent;
    let fixture: ComponentFixture<UserSharedEditorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserSharedEditorComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                info: {
                                    activeAuthSystem: 'Local Authentication',
                                    authSystems: [
                                        'Local Authentication',
                                        'OneLoginSAML',
                                    ],
                                    name: 'Administrator',
                                    email: 'admin@domain.tld',
                                    roles: [
                                        {
                                            roleId: 1,
                                            name: 'Admin',
                                            color: '#BD0000'
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(UserSharedEditorComponent);
        component = fixture.componentInstance;
        component.userForm = fb.group({
            username: ['', Validators.required],
            authSystem: ['', Validators.required],
            name: [''],
            email: ['', Validators.pattern('^(|([a-zA-Z0-9._%+-]+[^+]@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}))$')],
            roles: [['Admin']],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
