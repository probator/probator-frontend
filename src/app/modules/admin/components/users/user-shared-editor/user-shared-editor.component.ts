import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {IRole} from '../../../../../interfaces';
import {AdminApiService} from '../../../services/admin-api.service';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {throwError} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';

@Component({
    selector: 'app-user-shared-editor',
    templateUrl: './user-shared-editor.component.html',
    styleUrls: ['./user-shared-editor.component.scss']
})
export class UserSharedEditorComponent {
    userForm: FormGroup;
    roles: IRole[];
    authSystems: string[];
    activeAuthSystem: string;
    buttonText: string;

    constructor(
        protected fb: FormBuilder,
        protected adminApi: AdminApiService,
        protected metadata: MetadataService,
        protected utils: UtilsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected location: Location,
        protected dialogRef: MatDialog,
    ) { }

    save() {
        throwError('You must override the save method');
    }

    cancel() {
        this.location.back();
    }
}
