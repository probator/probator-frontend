import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserDeleteComponent} from './user-delete.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('UserDeleteComponent', () => {
    let component: UserDeleteComponent;
    let fixture: ComponentFixture<UserDeleteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserDeleteComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserDeleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
