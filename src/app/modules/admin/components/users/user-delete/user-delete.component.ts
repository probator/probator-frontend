import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IUser} from '../../../../../interfaces';
import {UserListComponent} from '../user-list/user-list.component';

@Component({
  selector: 'app-user-delete',
  templateUrl: './user-delete.component.html',
  styleUrls: ['./user-delete.component.scss']
})
export class UserDeleteComponent {
    user: IUser;

    constructor(public dialogRef: MatDialogRef<UserListComponent>, @Inject(MAT_DIALOG_DATA) user: IUser) {
        this.user = user;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
