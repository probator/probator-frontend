import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IUser} from '../../../../../interfaces';
import {UserListComponent} from '../user-list/user-list.component';

@Component({
    selector: 'app-user-password-change',
    templateUrl: './user-password-reset.component.html',
    styleUrls: ['./user-password-reset.component.scss']
})
export class UserPasswordResetComponent {
    user: IUser;

    constructor(public dialogRef: MatDialogRef<UserListComponent>, @Inject(MAT_DIALOG_DATA) user: IUser) {
        this.user = user;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
