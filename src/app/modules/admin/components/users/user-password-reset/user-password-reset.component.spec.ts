import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserPasswordResetComponent} from './user-password-reset.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('UserPasswordResetComponent', () => {
    let component: UserPasswordResetComponent;
    let fixture: ComponentFixture<UserPasswordResetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserPasswordResetComponent ],
            imports: [
                AdminMaterialModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserPasswordResetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
