import {Component, OnInit} from '@angular/core';
import {UserSharedEditorComponent} from '../user-shared-editor/user-shared-editor.component';
import {Validators} from '@angular/forms';
import {IUserAddInfo} from '../../../interfaces';
import {UserPasswordCreatedComponent} from '../user-password-created/user-password-created.component';

@Component({
    selector: 'app-user-add',
    templateUrl: '../user-shared-editor/user-shared-editor.component.html',
    styleUrls: ['../user-shared-editor/user-shared-editor.component.scss']
})
export class UserAddComponent extends UserSharedEditorComponent implements OnInit {
    ngOnInit() {
        const info: IUserAddInfo = this.route.snapshot.data.info;
        this.roles = info.roles;
        this.authSystems = info.authSystems;
        this.activeAuthSystem = info.activeAuthSystem;
        this.buttonText = 'Add User';

        this.userForm = this.fb.group({
            username: ['', Validators.required],
            authSystem: ['', Validators.required],
            name: [''],
            email: ['', Validators.pattern('^(|([a-zA-Z0-9._%+-]+[^+]@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}))$')],
            roles: [['User']],
        });
    }

    save() {
        this.adminApi.addUser(this.userForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                const pw = this.dialogRef.open(UserPasswordCreatedComponent, {
                    data: data,
                    width: '600px',
                });

                pw.afterClosed().subscribe(() => {
                    this.router.navigate(['../'], {relativeTo: this.route}).then();
                });
            }
        );
    }
}
