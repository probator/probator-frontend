import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserAddComponent} from './user-add.component';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';

describe('UserAddComponent', () => {
    let component: UserAddComponent;
    let fixture: ComponentFixture<UserAddComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserAddComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                info: {
                                    activeAuthSystem: 'Local Authentication',
                                    authSystems: [
                                        'Local Authentication',
                                        'OneLoginSAML',
                                    ],
                                    roles: [
                                        {
                                            roleId: 1,
                                            name: 'Admin',
                                            color: '#BD0000'
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(UserAddComponent);
        component = fixture.componentInstance;
        component.userForm = fb.group({
            username: ['', Validators.required],
            authSystem: ['', Validators.required],
            roles: [['Admin']],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
