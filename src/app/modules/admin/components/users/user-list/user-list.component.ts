import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AdminApiService} from '../../../services/admin-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {IUser} from '../../../../../interfaces';
import {FilterComponent} from '../../../../utils/components/filter/filter.component';
import {Observable} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {UtilsService} from '../../../../utils/services/utils.service';
import {IUserList} from '../../../interfaces';
import {MatDialog} from '@angular/material/dialog';
import {MatPaginator} from '@angular/material/paginator';
import {UserDeleteComponent} from '../user-delete/user-delete.component';
import {UserPasswordResetComponent} from '../user-password-reset/user-password-reset.component';
import {UserPasswordCreatedComponent} from '../user-password-created/user-password-created.component';
import {IFilter, IFilterValue} from '../../../../utils/interfaces';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    userCount = 0;
    users: IUser[];
    loading = false;
    filters: IFilter[];
    authSystems: IFilterValue[] = [];
    activeAuthSystem; string;
    displayedColumns = ['username', 'authSystem', 'name', 'roles', 'actions'];

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(FilterComponent, { static: true }) filterComp: FilterComponent;
    @ViewChild('top', { read: ElementRef, static: true }) pageTop: ElementRef;

    constructor(
        private adminApi: AdminApiService,
        private utils: UtilsService,
        private route: ActivatedRoute,
        private router: Router,
        private dialogRef: MatDialog,
    ) { }

    ngOnInit() {
        const page = this.route.snapshot.queryParams.page;
        if (page) {
            this.paginator.pageIndex = page;
        }

        const limit = this.route.snapshot.queryParams.limit;
        if (limit) {
            this.paginator.pageSize = limit;
        }

        this.setupFilters();
        this.loadUsers(true).subscribe(data => this.updateUsers(data));
    }

    loadUsers(initial: boolean): Observable<IUserList> {
        this.loading = true;

        const params = {
            count: this.paginator.pageSize || 25,
            page: this.paginator.pageIndex,
        };

        // We need to have a separate use case for the initial page load, to properly handle people
        // bookmarking links with filters enabled.
        if (initial) {
            const queryParams = this.route.snapshot.queryParams;
            params['authSystem'] = queryParams.hasOwnProperty('authSystem') ? queryParams.authSystem : [];
            params['search'] = queryParams.hasOwnProperty('search') ? queryParams.search : '';
        } else {
            params['authSystem'] = this.filterComp.getValues('authSystem');
            params['search'] = this.filterComp.getValues('search');
        }

        this.utils.cleanParams(params);
        return this.adminApi.getUsers(params);
    }

    updateUsers(data: IUserList) {
        this.loading = false;
        if (data) {
            this.activeAuthSystem = data.activeAuthSystem;
            for (const authSys of data.authSystems) {
                this.authSystems.push({
                    name: authSys,
                    value: authSys,
                });
            }
            this.userCount = data.userCount;
            this.users = data.users;
        }

        this.pageTop.nativeElement.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    }

    updateQueryParams(): void {
        const queryParams: any = {};

        if (this.paginator.pageSize !== 25) {
            queryParams.limit = this.paginator.pageSize;
        }

        if (this.paginator.pageIndex !== 0) {
            queryParams.page = this.paginator.pageIndex;
        }

        for (const filter of this.filters) {
            if (filter.values.length > 0) {
                queryParams[filter.key] = filter.values;
            }
        }

        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: queryParams
        }).then();
    }

    setupFilters(): void {
        const listify = a => { if (!Array.isArray(a)) { return [a]; } else { return ''; }};
        const params = this.route.snapshot.queryParams;

        this.filters = [
            {
                name: 'Auth system',
                key: 'authSystem',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('authSystem') ? listify(params.authSystem) : [],
                options: this.authSystems
            },
            {
                name: 'Search',
                key: 'search',
                type: 'input',
                values: params.hasOwnProperty('search') ? params.search : '',
            }
        ];

        // region Event subscriptions
        this.paginator.page
            .pipe(
                switchMap(() => {
                    const users = this.loadUsers(false);
                    this.updateQueryParams();
                    return users;
                }),
                map(data => this.updateUsers(data)),
                catchError(this.utils.handleError)
            )
            .subscribe(() => {})
        ;
        // endregion
    }

    delete(user: IUser) {
        const confirm = this.dialogRef.open(UserDeleteComponent, {
            data: user,
            width: '600px',
        });

        confirm.afterClosed().subscribe(
            doDelete => {
                if (doDelete) {
                    this.adminApi.deleteUser(user).subscribe(
                        data => {
                            const idx = this.users.indexOf(user);
                            if (idx) {
                                this.utils.showMessage(data['message']);
                                this.users.splice(idx, 1);
                                this.users = [...this.users];
                                this.userCount--;
                            }
                        }
                    );
                }
            }
        );
    }

    resetPassword(user: IUser) {
        const confirm = this.dialogRef.open(UserPasswordResetComponent, {
            data: user,
            width: '600px'
        });

        confirm.afterClosed().subscribe(
            doReset => {
                if (doReset) {
                    this.adminApi.resetPassword(user).subscribe(
                        data => {
                            this.utils.showMessage(data['message']);

                            this.dialogRef.open(UserPasswordCreatedComponent, {
                                data: data,
                                width: '600px',
                            });
                        }
                    );
                }
            }
        );
    }

    filterChange() {
        if (this.route.snapshot.queryParams.hasOwnProperty('page')) {
            this.paginator.pageIndex = this.route.snapshot.queryParams.page;
        }

        const users = this.loadUsers(false);
        this.updateQueryParams();
        users.subscribe(data => this.updateUsers(data));
    }
}
