import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserListComponent} from './user-list.component';
import {ActivatedRoute} from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {UtilsModule} from '../../../../utils/utils.module';

describe('UserListComponent', () => {
    let component: UserListComponent;
    let fixture: ComponentFixture<UserListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserListComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule,
                UtilsModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            queryParams: {
                                page: 0,
                                limit: 25,
                            },
                            data: {
                                users: {
                                    activeAuthSystem: 'Local Authentication',
                                    authSystems: [
                                        'Local Authentication',
                                        'OneLoginSAML',
                                    ],
                                    userCount: 1,
                                    users: [
                                        {
                                            userId: 1,
                                            username: 'admin',
                                            authSystem: 'Local Authentication',
                                            roles: [
                                                {
                                                    roleId: 1,
                                                    name: 'Admin',
                                                    color: '#BD0000'
                                                },
                                                {
                                                    roleId: 3,
                                                    name: 'User',
                                                    color: '#008000'
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
