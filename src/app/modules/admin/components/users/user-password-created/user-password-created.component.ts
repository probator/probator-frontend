import {Component, Inject} from '@angular/core';
import {IUser} from '../../../../../interfaces';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IUserCreateInfo} from '../../../interfaces';
import {UserListComponent} from '../user-list/user-list.component';

@Component({
    selector: 'app-user-password-created',
    templateUrl: './user-password-created.component.html',
    styleUrls: ['./user-password-created.component.scss']
})
export class UserPasswordCreatedComponent {
    user: IUser;
    password: string;

    constructor(public dialogRef: MatDialogRef<UserListComponent>, @Inject(MAT_DIALOG_DATA) info: IUserCreateInfo) {
        this.user = info.user;
        this.password = info.password;
    }

    close() {
        this.dialogRef.close();
    }
}
