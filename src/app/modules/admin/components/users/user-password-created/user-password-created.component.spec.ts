import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserPasswordCreatedComponent} from './user-password-created.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('UserPasswordCreatedComponent', () => {
    let component: UserPasswordCreatedComponent;
    let fixture: ComponentFixture<UserPasswordCreatedComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserPasswordCreatedComponent ],
            imports: [
                AdminMaterialModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: {
                        user: {
                            userId: 1,
                            username: 'admin',
                            authSystem: 'Local Authentication',
                            roles: [
                                {
                                    roleId: 1,
                                    name: 'Admin',
                                    color: '#BD0000'
                                },
                                {
                                    roleId: 3,
                                    name: 'User',
                                    color: '#008000'
                                }
                            ]
                        },
                        password: 'SecretPassword'
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserPasswordCreatedComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
