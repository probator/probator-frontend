import {Component, OnInit} from '@angular/core';
import {IUserDetails} from '../../../interfaces';
import {Validators} from '@angular/forms';
import {UserSharedEditorComponent} from '../user-shared-editor/user-shared-editor.component';
import {IRole} from '../../../../../interfaces';

@Component({
    selector: 'app-user-edit',
    templateUrl: '../user-shared-editor/user-shared-editor.component.html',
    styleUrls: ['../user-shared-editor/user-shared-editor.component.scss']
})
export class UserEditComponent extends UserSharedEditorComponent implements OnInit {
    ngOnInit() {
        const info: IUserDetails = this.route.snapshot.data.info;
        this.roles = info.roles;
        this.buttonText = 'Update User';

        this.userForm = this.fb.group({
            username: [{value: info.user.username, disabled: true}, Validators.required],
            authSystem: [{value: info.user.authSystem, disabled: true}, Validators.required],
            name: [info.user.name],
            email: [info.user.email, Validators.pattern('^(|([a-zA-Z0-9._%+-]+[^+]@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}))$')],
            roles: [(info.user.roles as IRole[]).map(role => role.name)],
        });
    }

    save() {
        this.adminApi.updateUser(this.route.snapshot.params.userId, this.userForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../../'], {relativeTo: this.route}).then();
            }
        );
    }
}
