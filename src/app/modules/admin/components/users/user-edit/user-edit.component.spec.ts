import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserEditComponent} from './user-edit.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';
import {UtilsModule} from '../../../../utils/utils.module';

describe('UserEditComponent', () => {
    let component: UserEditComponent;
    let fixture: ComponentFixture<UserEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserEditComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
                UtilsModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                info: {
                                    user: {
                                        userId: 1,
                                        username: 'admin',
                                        authSystem: 'Local Authentication',
                                        roles: [
                                            {
                                                roleId: 1,
                                                name: 'Admin',
                                                color: '#BD0000'
                                            },
                                            {
                                                roleId: 3,
                                                name: 'User',
                                                color: '#008000'
                                            }
                                        ]
                                    },
                                    roles: [
                                        {
                                            roleId: 1,
                                            name: 'Admin',
                                            color: '#BD0000'
                                        },
                                        {
                                            roleId: 2,
                                            name: 'NOC',
                                            color: '#5B5BFF'
                                        },
                                        {
                                            roleId: 3,
                                            name: 'User',
                                            color: '#008000'
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(UserEditComponent);
        component = fixture.componentInstance;
        component.userForm = fb.group({
            username: ['', Validators.required],
            authSystem: ['', Validators.required],
            roles: [['Admin']],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
