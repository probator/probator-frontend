import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IRole} from '../../../../../interfaces';
import {RoleListComponent} from '../role-list/role-list.component';

@Component({
    selector: 'app-role-delete',
    templateUrl: './role-delete.component.html',
    styleUrls: ['./role-delete.component.scss']
})
export class RoleDeleteComponent {
    role: IRole;

    constructor(public dialogRef: MatDialogRef<RoleListComponent>, @Inject(MAT_DIALOG_DATA) role: IRole) {
        this.role = role;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
