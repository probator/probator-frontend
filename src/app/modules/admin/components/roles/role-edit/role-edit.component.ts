import {Component, OnInit} from '@angular/core';
import {RoleSharedEditorComponent} from '../role-shared-editor/role-shared-editor.component';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-role-edit',
    templateUrl: '../role-shared-editor/role-shared-editor.component.html',
    styleUrls: ['../role-shared-editor/role-shared-editor.component.scss']
})
export class RoleEditComponent extends RoleSharedEditorComponent implements OnInit {
    ngOnInit() {
        const role = this.route.snapshot.data.role.role;
        this.buttonText = 'Update Role';
        this.roleForm = this.fb.group({
            'roleId': [{value: role.roleId, disabled: true}, Validators.required],
            'name': [{value: role.name, disabled: true}, Validators.required],
            'color': [role.color, [Validators.required, Validators.pattern(/^#[a-fA-F0-9]{6}$/)]],
        });
    }

    save() {
        this.adminApi.updateRole(this.roleForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../../'], {relativeTo: this.route}).then();
            }
        );
    }
}
