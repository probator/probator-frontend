import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RoleEditComponent} from './role-edit.component';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';

describe('RoleEditComponent', () => {
    let component: RoleEditComponent;
    let fixture: ComponentFixture<RoleEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ RoleEditComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                ReactiveFormsModule,
                HttpClientModule,
                AdminMaterialModule
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                role: {
                                    role: {
                                        roleId: 1,
                                        name: 'Admin',
                                        color: '#BD0000'
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(RoleEditComponent);
        component = fixture.componentInstance;
        component.roleForm = fb.group({
            'roleId': [{value: 1, disabled: true}, Validators.required],
            'name': [{value: 'Admin', disabled: true}, Validators.required],
            'color': ['#BD0000', [Validators.required, Validators.pattern(/^#[a-fA-F0-9]{6}$/)]],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
