import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RoleListComponent} from './role-list.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';

describe('RoleListComponent', () => {
    let component: RoleListComponent;
    let fixture: ComponentFixture<RoleListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ RoleListComponent ],
            imports: [
                NoopAnimationsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                roles: {
                                    roleCount: 1,
                                    roles: [
                                        {
                                            roleId: 1,
                                            name: 'Admin',
                                            color: '#BD0000'
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RoleListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
