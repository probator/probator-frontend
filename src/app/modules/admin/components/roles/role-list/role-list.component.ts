import {Component} from '@angular/core';
import {IRole} from '../../../../../interfaces';
import {ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {RoleDeleteComponent} from '../role-delete/role-delete.component';
import {UtilsService} from '../../../../utils/services/utils.service';
import {AdminApiService} from '../../../services/admin-api.service';

@Component({
    selector: 'app-role-list',
    templateUrl: './role-list.component.html',
    styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent {
    roles: IRole[];
    displayedColumns = ['name', 'color', 'actions'];

    constructor(
        private adminApi: AdminApiService,
        private utils: UtilsService,
        private route: ActivatedRoute,
        private dialogRef: MatDialog
    ) {
        this.roles = this.route.snapshot.data.roles.roles;
    }

    delete(role: IRole) {
        const confirm = this.dialogRef.open(RoleDeleteComponent, {
            data: role,
            width: '600px',
        });

        confirm.afterClosed().subscribe(
            doDelete => {
                if (doDelete) {
                    this.adminApi.deleteRole(role).subscribe(
                        data => {const idx = this.roles.indexOf(role);

                            if (idx) {
                                this.utils.showMessage(data['message']);
                                this.roles.splice(idx, 1);
                                this.roles = [...this.roles];
                            }
                        }
                    );
                }
            }
        );
    }
}
