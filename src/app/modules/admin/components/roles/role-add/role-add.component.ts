import {Component, OnInit} from '@angular/core';
import {RoleSharedEditorComponent} from '../role-shared-editor/role-shared-editor.component';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-role-add',
    templateUrl: '../role-shared-editor/role-shared-editor.component.html',
    styleUrls: ['../role-shared-editor/role-shared-editor.component.scss']
})
export class RoleAddComponent extends RoleSharedEditorComponent implements OnInit {
    ngOnInit() {
        this.buttonText = 'Create Role';
        this.roleForm = this.fb.group({
            'name': ['', Validators.required],
            'color': ['', [Validators.required, Validators.pattern(/^#[a-fA-F0-9]{6}$/)]],
        });
    }

    save() {
        this.adminApi.createRole(this.roleForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../'], {relativeTo: this.route}).then();
            }
        );
    }
}
