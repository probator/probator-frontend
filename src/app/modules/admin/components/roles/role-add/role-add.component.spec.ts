import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RoleAddComponent} from './role-add.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';

describe('RoleAddComponent', () => {
    let component: RoleAddComponent;
    let fixture: ComponentFixture<RoleAddComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ RoleAddComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(RoleAddComponent);
        component = fixture.componentInstance;
        component.roleForm = fb.group({
            'name': ['', Validators.required],
            'color': ['', [Validators.required, Validators.pattern(/^#[a-fA-F0-9]{6}$/)]],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
