import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RoleSharedEditorComponent} from './role-shared-editor.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';

describe('RoleSharedEditorComponent', () => {
    let component: RoleSharedEditorComponent;
    let fixture: ComponentFixture<RoleSharedEditorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ RoleSharedEditorComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(RoleSharedEditorComponent);
        component = fixture.componentInstance;
        component.roleForm = fb.group({
            'roleId': [{value: 1, disabled: true}, Validators.required],
            'name': [{value: 'Admin', disabled: true}, Validators.required],
            'color': ['#BD0000', [Validators.required, Validators.pattern(/^#[a-fA-F0-9]{6}$/)]],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
