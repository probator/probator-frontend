import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AdminApiService} from '../../../services/admin-api.service';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {MatDialog} from '@angular/material/dialog';
import {throwError} from 'rxjs';

@Component({
  selector: 'app-role-shared-editor',
  templateUrl: './role-shared-editor.component.html',
  styleUrls: ['./role-shared-editor.component.scss']
})
export class RoleSharedEditorComponent {
    roleForm: FormGroup;
    color = '#bcbcbc';
    buttonText: string;

    constructor(
        protected fb: FormBuilder,
        protected adminApi: AdminApiService,
        protected metadata: MetadataService,
        protected utils: UtilsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected location: Location,
        protected dialogRef: MatDialog,
    ) { }

    save() {
        throwError('You must override the save method');
    }

    cancel() {
        this.location.back();
    }

    updateColor($event) {
        this.color = $event.color.hex;
        this.roleForm.get('color').setValue($event.color.hex);
    }
}
