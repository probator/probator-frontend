import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LogDetailsComponent} from './log-details.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';

describe('LogDetailsComponent', () => {
    let component: LogDetailsComponent;
    let fixture: ComponentFixture<LogDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ LogDetailsComponent ],
            imports: [
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                logEvent: {
                                    logEvent: {
                                        logEventId: 1,
                                        level: 'WARNING',
                                        levelno: 30,
                                        timestamp: '2018-12-05T16:47:29',
                                        message: 'Failed verifying password for admin from 127.0.0.1',
                                        module: '__init__',
                                        filename: '__init__.py',
                                        lineno: 59,
                                        funcname: 'post',
                                        pathname: '/opt/probator/probator-auth-local/probator_auth_local/__init__.py',
                                        processId: 47594,
                                        stacktrace: null
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LogDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
