import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {ILogEvent} from '../../../interfaces';

@Component({
    selector: 'app-log-details',
    templateUrl: './log-details.component.html',
    styleUrls: ['./log-details.component.scss']
})
export class LogDetailsComponent implements OnInit {
    logEvent: ILogEvent;

    constructor(private route: ActivatedRoute, private location: Location) { }

    ngOnInit() {
        this.logEvent = this.route.snapshot.data.logEvent.logEvent;
    }

    back() {
        this.location.back();
    }
}
