import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ILogEvent, ILogEventList} from '../../../interfaces';
import {FilterComponent} from '../../../../utils/components/filter/filter.component';
import {MatPaginator} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {AdminApiService} from '../../../services/admin-api.service';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {IFilter, IFilterValue} from '../../../../utils/interfaces';

@Component({
    selector: 'app-log-list',
    templateUrl: './log-list.component.html',
    styleUrls: ['./log-list.component.scss']
})
export class LogListComponent implements OnInit {
    logEventCount = 0;
    logEvents: ILogEvent[];
    loglevels: IFilterValue[] = [
        {value: null, name: 'All'},
        {value: '40', name: 'Error'},
        {value: '30', name: 'Warning'},
        {value: '20', name: 'Informational'}
    ];
    displayedColumns = ['timestamp', 'level', 'message', 'actions'];
    loading = false;
    filters: IFilter[];

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(FilterComponent, { static: true }) filterComp: FilterComponent;
    @ViewChild('top', { read: ElementRef, static: true }) pageTop: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private adminApi: AdminApiService,
        private metadata: MetadataService,
        private router: Router,
        public utils: UtilsService
    ) { }

    ngOnInit() {
        const page = this.route.snapshot.queryParams.page;
        if (page) {
            this.paginator.pageIndex = page;
        }

        const limit = this.route.snapshot.queryParams.limit;
        if (limit) {
            this.paginator.pageSize = limit;
        }

        this.loadEvents(true).subscribe(data => {
            this.updateEvents(data);
            this.setupFilters();
        });
    }

    loadEvents(initial: boolean): Observable<ILogEventList> {
        this.loading = true;
        const params = {
            count: this.paginator.pageSize || 25,
            page: this.paginator.pageIndex,
        };
        let levelno;

        // We need to have a separate use case for the initial page load, to properly handle people
        // bookmarking links with filters enabled.
        if (initial) {
            const queryParams = this.route.snapshot.queryParams;
            levelno = queryParams.hasOwnProperty('levelno') ? queryParams.levelno : null;
        } else {
            levelno = this.filterComp.getValues('levelno');
        }

        if (levelno) {
            params['levelno'] = levelno;
        }

        return this.adminApi.getLogEvents(params);
    }

    updateEvents(data: ILogEventList) {
        this.loading = false;
        if (data) {
            this.logEventCount = data.logEventCount;
            this.logEvents = data.logEvents;
        }

        this.pageTop.nativeElement.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    }

    updateQueryParams(): void {
        const queryParams: any = {};

        if (this.paginator.pageSize !== 25) {
            queryParams.limit = this.paginator.pageSize;
        }

        if (this.paginator.pageIndex !== 0) {
            queryParams.page = this.paginator.pageIndex;
        }

        for (const filter of this.filters) {
            if (filter.values) {
                if (filter.values.length > 0) {
                    queryParams[filter.key] = filter.values;
                }
            }
        }

        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: queryParams
        }).then();
    }

    setupFilters(): void {
        const listify = a => { if (!Array.isArray(a)) { return [a]; } else { return ''; }};
        const params = this.route.snapshot.queryParams;

        this.filters = [
            {
                name: 'Log Level',
                key: 'levelno',
                type: 'select',
                multiple: false,
                values: params.hasOwnProperty('levelno') ? listify(params.levelno) : [],
                options: this.loglevels
            },
        ];

        // region Event subscriptions
        this.paginator.page
            .pipe(
                switchMap(() => {
                    const events = this.loadEvents(false);
                    this.updateQueryParams();
                    return events;
                }),
                map(data => this.updateEvents(data)),
                catchError(this.handleError)
            )
            .subscribe(() => {})
        ;
        // endregion
    }

    filterChange() {
        if (this.route.snapshot.queryParams.hasOwnProperty('page')) {
            this.paginator.pageIndex = this.route.snapshot.queryParams.page;
        }

        const emails = this.loadEvents(false);
        this.updateQueryParams();
        emails.subscribe(data => this.updateEvents(data));
    }

    getLevelColor(level: string): string {
        switch (level) {
            case 'ERROR':
                return 'red';

            case 'WARNING':
                return 'orange';

            case 'INFO':
                return 'blue';

            default:
                return '';
        }
    }

    private handleError(err): Observable<any> {
        this.loading = false;
        this.utils.showError(err);
        return of([]);
    }
}
