import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LogListComponent} from './log-list.component';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {UtilsModule} from '../../../../utils/utils.module';
import {ActivatedRoute} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('LogListComponent', () => {
    let component: LogListComponent;
    let fixture: ComponentFixture<LogListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ LogListComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule,
                UtilsModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            queryParams: {
                                page: 0,
                                limit: 25,
                            },
                            data: {
                                logEvents: {
                                    logEventCount: 1,
                                    logEvents: [
                                        {
                                            logEventId: 1,
                                            level: 'WARNING',
                                            levelno: 30,
                                            timestamp: '2018-12-05T16:47:29',
                                            message: 'Failed verifying password for admin from 127.0.0.1',
                                            module: '__init__',
                                            filename: '__init__.py',
                                            lineno: 59,
                                            funcname: 'post',
                                            pathname: '/opt/probator/probator-auth-local/probator_auth_local/__init__.py',
                                            processId: 47594,
                                            stacktrace: null
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LogListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
