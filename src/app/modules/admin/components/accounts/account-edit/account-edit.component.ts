import {Component, OnInit} from '@angular/core';
import {AccountSharedEditorComponent} from '../account-shared-editor/account-shared-editor.component';
import {map} from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';
import {IAccount} from '../../../../../interfaces';

@Component({
    selector: 'app-accounts-edit',
    templateUrl: '../account-shared-editor/account-shared-editor.component.html',
    styleUrls: ['../account-shared-editor/account-shared-editor.component.scss']
})
export class AccountEditComponent extends AccountSharedEditorComponent implements OnInit {
    ngOnInit() {
        this.buttonText = 'Update Account';
        this.allRoles = this.route.snapshot.data.roles.roles.map(role => role.name);
        const account: IAccount = this.route.snapshot.data.account.account;

        this.filteredRoles = this.roleCtrl.valueChanges.pipe(
            map((role: string | null) => role ? this._filter(role) : this.allRoles)
        );

        if (this.metadata.loaded) {
            this.accountTypes = this.metadata.accountTypes;
        } else {
            this.metadata.metadataLoaded.subscribe(() => {
                this.accountTypes = this.metadata.accountTypes;
            });
        }

        const properties = this.fb.group({});
        for (const [key, value] of Object.entries(account.properties)) {
            const opts: any = {validators: []};
            const property = this.metadata.getAccountProperty(account.accountType, key);

            if (property) {
                if (property.required) {
                    opts.validators.push(Validators.required);
                }

                if (property.pattern) {
                    opts.validators.push(Validators.pattern(property.pattern));
                }

                const ctrl = new FormControl(value, opts);
                properties.addControl(key, ctrl);
            }
        }

        this.accountForm = this.fb.group(
            {
                accountId: [{value: account.accountId, disabled: true}, Validators.required],
                accountName: [account.accountName, Validators.required],
                accountType: [{value: account.accountType, disabled: true}, Validators.required],
                requiredRoles: [account.requiredRoles],
                contacts: [account.contacts, Validators.required],
                enabled: [account.enabled],
                properties: properties,
            }
        );
        this.showProperties = true;
        this.selectedAccountType = account.accountType;
    }

    save() {
        this.adminApi.updateAccount(this.accountForm.getRawValue()).subscribe((data) => {
            this.utils.showMessage(data['message']);
            this.router.navigate(['../../'], {relativeTo: this.route}).then();
        });
    }
}
