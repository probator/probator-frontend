import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountEditComponent} from './account-edit.component';
import {AccountSharedEditorComponent} from '../account-shared-editor/account-shared-editor.component';
import {ContactEditorComponent} from '../contact-editor/contact-editor.component';
import {AccountPropertyComponent} from '../account-property/account-property.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';

describe('AccountEditComponent', () => {
    let component: AccountEditComponent;
    let fixture: ComponentFixture<AccountEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AccountEditComponent,
                AccountSharedEditorComponent,
                ContactEditorComponent,
                AccountPropertyComponent,
            ],
            imports: [
                NoopAnimationsModule,
                AdminMaterialModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                roles: {
                                    roles: [
                                        {roleId: 1, name: 'Admin', color: '#BD0000'},
                                        {roleId: 2, name: 'NOC', color: '#5B5BFF'},
                                        {roleId: 3, name: 'User', color: '#008000'}
                                    ],
                                    roleCount: 3
                                },
                                account: {
                                    account: {
                                        accountId: 1,
                                        accountName: 'account-name',
                                        accountType: 'AWS',
                                        contacts: [
                                            {type: 'email', value: 'name@domain.tld'}
                                        ],
                                        enabled: true,
                                        requiredRoles: [],
                                        properties: {accountNumber: '123456789012'}
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
