import {Component, ElementRef, ViewChild} from '@angular/core';
import {Location} from '@angular/common';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IAccountProperty, IAccountType} from '../../../../../interfaces';
import {Observable, throwError} from 'rxjs';
import {ENTER} from '@angular/cdk/keycodes';
import {MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {MatSelectChange} from '@angular/material/select';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AdminApiService} from '../../../services/admin-api.service';

@Component({
    selector: 'app-account-shared-editor',
    templateUrl: './account-shared-editor.component.html',
    styleUrls: ['./account-shared-editor.component.scss']
})
export class AccountSharedEditorComponent {
    accountForm: FormGroup;
    accountTypes: IAccountType[];
    allRoles: string[];
    roleCtrl = new FormControl();
    filteredRoles: Observable<string[]>;
    selectedAccountType: string;
    showProperties = false;
    buttonText: string;

    readonly separatorKeyCodes: number[] = [ENTER];

    @ViewChild('roleInput', { static: true }) roleInput: ElementRef<HTMLInputElement>;
    @ViewChild('roleAuto', { static: true }) roleAuto: MatAutocomplete;

    constructor(
        protected fb: FormBuilder,
        protected adminApi: AdminApiService,
        protected metadata: MetadataService,
        protected utils: UtilsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected location: Location,
    ) {}

    updateAccountType(event: MatSelectChange) {
        this.selectedAccountType = event.value;
        const properties = this.accountForm.get('properties') as FormGroup;
        this.showProperties = false;
        for (const ctrlName of Object.keys(properties.controls)) {
            properties.removeControl(ctrlName);
        }

        for (const property of this.getAccountProperties(event.value)) {
            const opts: any = {validators: []};

            if (property.required) {
                opts.validators.push(Validators.required);
            }

            if (property.pattern) {
                opts.validators.push(Validators.pattern(property.pattern));
            }

            const ctrl = new FormControl(property.default, opts);
            properties.addControl(property.key, ctrl);
        }
        this.showProperties = true;
    }

    removeRequiredRole(group) {
        const idx = this.accountForm.controls.requiredRoles.value.indexOf(group);

        if (idx >= 0) {
            this.accountForm.controls.requiredRoles.value.splice(idx, 1);
        }
    }

    addRequiredRole(event: MatChipInputEvent) {
        if (!this.roleAuto.isOpen) {
            const input = event.input;
            const value = event.value;

            if ((value || '').trim()) {
                if (this.allRoles.indexOf(value) === -1) {
                    this.utils.showError(`Invalid role ${value}`);
                    return;
                }

                const roles: string[] = this.accountForm.get('requiredRoles').value || [];
                roles.push(value);
                this.accountForm.get('requiredRoles').setValue(roles);
            }

            if (input) {
                input.value = '';
            }
        }
    }

    selectedRole($event) {
        const roles: string[] = this.accountForm.get('requiredRoles').value || [];
        roles.push($event.option.viewValue);
        this.accountForm.get('requiredRoles').setValue(roles);
        this.roleInput.nativeElement.value = '';
        this.roleCtrl.setValue(null);
    }

    protected _filter(roleFilter: string): string[] {
        const filterValue = roleFilter.toLowerCase();

        return this.allRoles.filter(role => role.toLowerCase().indexOf(filterValue) === 0);
    }

    getAccountProperties(accountType: string): IAccountProperty[] {
        for (const aType of this.metadata.accountTypes) {
            if (accountType === aType.name) {
                return aType.properties;
            }
        }

        return [];
    }

    save() {
        throwError('You must override the save method');
    }

    cancel() {
        this.location.back();
    }
}
