import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountSharedEditorComponent} from './account-shared-editor.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ContactEditorComponent} from '../contact-editor/contact-editor.component';
import {AccountPropertyComponent} from '../account-property/account-property.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('AccountSharedEditorComponent', () => {
    let component: AccountSharedEditorComponent;
    let fixture: ComponentFixture<AccountSharedEditorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AccountSharedEditorComponent,
                ContactEditorComponent,
                AccountPropertyComponent,
            ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountSharedEditorComponent);
        component = fixture.componentInstance;
        const fb = new FormBuilder();
        component.accountForm = fb.group({
            accountName: ['', Validators.required],
            accountType: ['', Validators.required],
            requiredRoles: [Array()],
            contacts: [Array(), Validators.required],
            enabled: [true],
            properties: fb.group([]),
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
