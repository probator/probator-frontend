import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IAccount} from '../../../../../interfaces';
import {UtilsService} from '../../../../utils/services/utils.service';
import {MatDialog} from '@angular/material/dialog';
import {AccountDeleteComponent} from '../account-delete/account-delete.component';
import {AdminApiService} from '../../../services/admin-api.service';
import {AccountImportExportComponent} from '../account-import-export/account-import-export.component';

@Component({
    selector: 'app-accounts-list',
    templateUrl: './account-list.component.html',
    styleUrls: ['./account-list.component.scss']
})
export class AccountListComponent implements OnInit {
    accounts: IAccount[] = [];
    columns = ['accountName', 'contacts', 'type', 'status', 'actions'];
    accountLabelClass = UtilsService.getAccountClass;
    contactLabelClass = UtilsService.getContactClass;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private utils: UtilsService,
        private adminApi: AdminApiService,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.accounts = this.route.snapshot.data.accounts.accounts;
    }

    delete(account: IAccount) {
        const confirm = this.dialog.open(AccountDeleteComponent, {
            data: account,
            width: '800px'
        });

        confirm.afterClosed().subscribe(doDelete => {
            if (doDelete) {
                this.adminApi.deleteAccount(account).subscribe(data => {
                    for (const acct of this.accounts) {
                        if (acct === account) {
                            this.accounts = this.accounts.filter(x => x !== account);
                        }
                    }
                    this.utils.showMessage(data['message']);
                });
            }
        });
    }

    importExport() {
        const dialogRef = this.dialog.open(AccountImportExportComponent, {
            width: '600px',
        });

        dialogRef.afterClosed().subscribe(updateAccountList => {
            if (updateAccountList === true) {
                this.adminApi.getAccounts().subscribe(data => {
                    this.accounts = data.accounts;
                });
            }
        });
    }
}
