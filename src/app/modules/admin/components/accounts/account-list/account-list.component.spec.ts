import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountListComponent} from './account-list.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {UtilsModule} from '../../../../utils/utils.module';
import {ActivatedRoute} from '@angular/router';

describe('AccountListComponent', () => {
    let component: AccountListComponent;
    let fixture: ComponentFixture<AccountListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AccountListComponent ],
            imports: [
                NoopAnimationsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
                UtilsModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                accounts: [
                                    {
                                        accountId: 1,
                                        accountName: 'account-name',
                                        accountType: 'AWS',
                                        contacts: [
                                            {type: 'email', value: 'name@domain.tld'}
                                        ],
                                        enabled: true,
                                        requiredRoles: [],
                                        properties: {accountNumber: '123456789012'}
                                    }
                                ]
                            }
                        }
                    }

                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
