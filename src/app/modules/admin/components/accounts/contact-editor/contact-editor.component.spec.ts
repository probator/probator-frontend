import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ContactEditorComponent} from './contact-editor.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {AdminMaterialModule} from '../../../admin.material.module';
import {HttpClientModule} from '@angular/common/http';

describe('ContactEditorComponent', () => {
    let component: ContactEditorComponent;
    let fixture: ComponentFixture<ContactEditorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ContactEditorComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ContactEditorComponent);
        component = fixture.componentInstance;
        component.contacts = new FormControl([{
            type: 'email',
            value: 'name@domain.tld'
        }]);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
