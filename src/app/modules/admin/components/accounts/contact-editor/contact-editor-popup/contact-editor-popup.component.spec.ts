import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ContactEditorPopupComponent} from './contact-editor-popup.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {AdminMaterialModule} from '../../../../admin.material.module';
import {HttpClientModule} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('ContactEditorPopupComponent', () => {
    let component: ContactEditorPopupComponent;
    let fixture: ComponentFixture<ContactEditorPopupComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ContactEditorPopupComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                AdminMaterialModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: []}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ContactEditorPopupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
