import {Component, OnInit} from '@angular/core';
import {MetadataService} from '../../../../../../services/metadata.service';
import {MatDialogRef} from '@angular/material/dialog';
import {MatSelectChange} from '@angular/material/select';
import {INotifier} from '../../../../../../interfaces';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ContactEditorComponent} from '../contact-editor.component';

@Component({
    selector: 'app-contact-editor-popup',
    templateUrl: './contact-editor-popup.component.html',
    styleUrls: ['./contact-editor-popup.component.scss']
})
export class ContactEditorPopupComponent implements OnInit {
    contactForm: FormGroup;
    type: string;
    notifiers: INotifier[];

    constructor(private metadata: MetadataService, private fb: FormBuilder, public dialogRef: MatDialogRef<ContactEditorComponent>) {
        this.notifiers = this.metadata.notifiers;
        this.contactForm = this.fb.group({
            type: ['', Validators.required],
            value: ['', Validators.required]
        });
    }

    ngOnInit() {}

    changeType(event: MatSelectChange) {
        // this.validator = event.value.validation;
        this.contactForm.removeControl('value');
        this.contactForm.addControl('value', new FormControl(
            '',
            {
                validators: [
                    Validators.required,
                    Validators.pattern(event.value.validation)
                ]
            }
        ));
    }

    add() {
        this.dialogRef.close({
            type: this.contactForm.get('type').value.type,
            value: this.contactForm.get('value').value
        });
    }

    cancel() {
        this.dialogRef.close();
    }
}
