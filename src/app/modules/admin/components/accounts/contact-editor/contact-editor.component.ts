import {Component, Input, OnInit} from '@angular/core';
import {IAccountContact} from '../../../../../interfaces';
import {UtilsService} from '../../../../utils/services/utils.service';
import {FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ContactEditorPopupComponent} from './contact-editor-popup/contact-editor-popup.component';

@Component({
    selector: 'app-contact-editor',
    templateUrl: './contact-editor.component.html',
    styleUrls: ['./contact-editor.component.scss']
})
export class ContactEditorComponent implements OnInit {
    @Input() contacts: FormControl;

    constructor(public dialog: MatDialog) {}

    ngOnInit() {}

    getClass(contact: IAccountContact): string {
        return UtilsService.getContactClass(contact.type);
    }

    openInput() {
        const form = this.dialog.open(ContactEditorPopupComponent, {
            width: '800px',
        });

        form.afterClosed()
            .subscribe(contact => {
                if (contact) {
                    const contacts = this.contacts.value;
                    contacts.push(contact);
                    this.contacts.setValue(contacts);
                }
            }
        );
    }

    remove(contact: IAccountContact) {
        const contacts = this.contacts.value;
        const idx = contacts.indexOf(contact);

        if (idx >= 0) {
            contacts.splice(idx, 1);
            this.contacts.setValue(contacts);
        }
    }
}
