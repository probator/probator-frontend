import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountDeleteComponent} from './account-delete.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('AccountDeleteComponent', () => {
    let component: AccountDeleteComponent;
    let fixture: ComponentFixture<AccountDeleteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AccountDeleteComponent ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: []}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountDeleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
