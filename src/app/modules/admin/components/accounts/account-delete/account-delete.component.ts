import {Component, Inject} from '@angular/core';
import {IAccount} from '../../../../../interfaces';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AccountListComponent} from '../account-list/account-list.component';

@Component({
    selector: 'app-account-delete',
    templateUrl: './account-delete.component.html',
    styleUrls: ['./account-delete.component.scss']
})
export class AccountDeleteComponent {
    account: IAccount;

    constructor(public dialogRef: MatDialogRef<AccountListComponent>, @Inject(MAT_DIALOG_DATA) account: IAccount) {
        this.account = account;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
