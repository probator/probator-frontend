import {Component, OnInit} from '@angular/core';
import {ImportExportComponent} from '../../shared/import-export/import-export.component';

@Component({
    selector: 'app-account-import-export',
    templateUrl: '../../shared/import-export/import-export.component.html',
    styleUrls: ['../../shared/import-export/import-export.component.scss']
})
export class AccountImportExportComponent extends ImportExportComponent implements OnInit {
    title = 'Import / Export Accounts';

    ngOnInit() {
        this.exportControl.setValue('json');
    }

    doExport() {
        this.adminApi.exportAccounts().subscribe(data => {
            if (data) {
                this.utils.downloadBlob('probator-accounts.json', data);
            }
        });
    }

    doImport(importData: string) {
        this.adminApi.importAccounts(importData).subscribe(data => {
            this.utils.showMessage(data['message']);
            this.dialogRef.close(true);
        });
    }
}

