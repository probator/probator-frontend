import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountImportExportComponent} from './account-import-export.component';
import {UtilsModule} from '../../../../utils/utils.module';
import {AdminMaterialModule} from '../../../admin.material.module';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogRef} from '@angular/material/dialog';
import {ImportExportComponent} from '../../shared/import-export/import-export.component';
import {ReactiveFormsModule} from '@angular/forms';

describe('AccountImportExportComponent', () => {
    let component: AccountImportExportComponent;
    let fixture: ComponentFixture<AccountImportExportComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AccountImportExportComponent,
                ImportExportComponent,
            ],
            imports: [
                NoopAnimationsModule,
                HttpClientModule,
                UtilsModule,
                AdminMaterialModule,
                ReactiveFormsModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountImportExportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
