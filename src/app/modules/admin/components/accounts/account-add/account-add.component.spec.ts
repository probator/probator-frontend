import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountAddComponent} from './account-add.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {AccountSharedEditorComponent} from '../account-shared-editor/account-shared-editor.component';
import {ContactEditorComponent} from '../contact-editor/contact-editor.component';
import {AccountPropertyComponent} from '../account-property/account-property.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('AccountAddComponent', () => {
    let component: AccountAddComponent;
    let fixture: ComponentFixture<AccountAddComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AccountAddComponent,
                AccountSharedEditorComponent,
                ContactEditorComponent,
                AccountPropertyComponent,
            ],
            imports: [
                NoopAnimationsModule,
                AdminMaterialModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                roles: {
                                    roles: [
                                        {roleId: 1, name: 'Admin', color: '#BD0000'},
                                        {roleId: 2, name: 'NOC', color: '#5B5BFF'},
                                        {roleId: 3, name: 'User', color: '#008000'}
                                    ],
                                    roleCount: 3
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountAddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
