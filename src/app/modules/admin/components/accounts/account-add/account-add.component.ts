import {Component, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {map} from 'rxjs/operators';
import {AccountSharedEditorComponent} from '../account-shared-editor/account-shared-editor.component';

@Component({
    selector: 'app-account-add',
    templateUrl: '../account-shared-editor/account-shared-editor.component.html',
    styleUrls: ['../account-shared-editor/account-shared-editor.component.scss']
})
export class AccountAddComponent extends AccountSharedEditorComponent implements OnInit {
    ngOnInit() {
        this.buttonText = 'Add Account';
        this.allRoles = this.route.snapshot.data.roles.roles.map(role => role.name);

        this.filteredRoles = this.roleCtrl.valueChanges.pipe(
            map((role: string | null) => role ? this._filter(role) : this.allRoles)
        );

        if (this.metadata.loaded) {
            this.accountTypes = this.metadata.accountTypes;
        } else {
            this.metadata.metadataLoaded.subscribe(() => {
                this.accountTypes = this.metadata.accountTypes;
            });
        }

        this.accountForm = this.fb.group(
            {
                accountName: ['', Validators.required],
                accountType: ['', Validators.required],
                requiredRoles: [Array()],
                contacts: [Array(), Validators.required],
                enabled: [true],
                properties: this.fb.group([]),
            }
        );
    }

    save() {
        this.adminApi.addAccount(this.accountForm.value).subscribe((data) => {
            this.utils.showMessage(data['message']);
            this.router.navigate(['../'], {relativeTo: this.route}).then();
        });
    }
}
