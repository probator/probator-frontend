import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AccountPropertyComponent} from './account-property.component';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {AdminMaterialModule} from '../../../admin.material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('AccountPropertyComponent', () => {
    let component: AccountPropertyComponent;
    let fixture: ComponentFixture<AccountPropertyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AccountPropertyComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountPropertyComponent);
        component = fixture.componentInstance;
        component.control = new FormControl('key');
        component.property = {
            key: 'key',
            type: 'string',
            name: 'My Property',
            default: '',
            required: true,
            pattern: '',
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
