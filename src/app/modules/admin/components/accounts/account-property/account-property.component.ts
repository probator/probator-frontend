import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import {IAccountProperty} from '../../../../../interfaces';
import {ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import {UtilsService} from '../../../../utils/services/utils.service';

@Component({
    selector: 'app-account-property',
    templateUrl: './account-property.component.html',
    styleUrls: ['./account-property.component.scss']
})
export class AccountPropertyComponent {
    @Input() control: FormControl;
    @Input() property: IAccountProperty;

    readonly separatorKeyCodes: number[] = [ENTER];
    public inputCtrl = new FormControl();

    constructor(private utils: UtilsService) {}

    removeListValue(group) {
        const idx = this.control.value.indexOf(group);

        if (idx >= 0) {
            this.control.value.splice(idx, 1);
        }
    }

    addListValue(event: MatChipInputEvent) {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            if (this.property.pattern) {
                const rgx = new RegExp(this.property.pattern);
                if (!rgx.test(value)) {
                    this.utils.showError(`Invalid formatted domain name: ${value}`);
                    return;
                }
            }
            const values: string[] = this.control.value || [];
            values.push(value);
            this.control.setValue(values);
        }

        if (input) {
            input.value = '';
        }
    }
}
