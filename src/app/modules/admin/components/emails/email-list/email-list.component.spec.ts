import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EmailListComponent} from './email-list.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {UtilsModule} from '../../../../utils/utils.module';

describe('EmailListComponent', () => {
    let component: EmailListComponent;
    let fixture: ComponentFixture<EmailListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EmailListComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule,
                UtilsModule,
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EmailListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
