import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FilterComponent} from '../../../../utils/components/filter/filter.component';
import {MatPaginator} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {IEmail, IEmailList} from '../../../interfaces';
import {Observable, of} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';
import {AdminApiService} from '../../../services/admin-api.service';
import {IFilter} from '../../../../utils/interfaces';

@Component({
    selector: 'app-email-list',
    templateUrl: './email-list.component.html',
    styleUrls: ['./email-list.component.scss']
})
export class EmailListComponent implements OnInit {
    emailCount = 0;
    emails: IEmail[];
    subsystems = [];
    displayedColumns = ['timestamp', 'subsystem', 'subject', 'recipients', 'actions'];
    loading = false;
    filters: IFilter[];

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(FilterComponent, { static: true }) filterComp: FilterComponent;
    @ViewChild('top', { read: ElementRef, static: true }) pageTop: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private adminApi: AdminApiService,
        private metadata: MetadataService,
        private router: Router,
        public utils: UtilsService
    ) { }

    ngOnInit() {
        const page = this.route.snapshot.queryParams.page;
        if (page) {
            this.paginator.pageIndex = page;
        }

        const limit = this.route.snapshot.queryParams.limit;
        if (limit) {
            this.paginator.pageSize = limit;
        }

        this.loadEmails(true).subscribe(data => {
            this.updateEmails(data);
            this.setupFilters();
        });
    }

    loadEmails(initial: boolean): Observable<IEmailList> {
        this.loading = true;
        let subsystems;

        // We need to have a separate use case for the initial page load, to properly handle people
        // bookmarking links with filters enabled.
        if (initial) {
            const params = this.route.snapshot.queryParams;
            subsystems = params.hasOwnProperty('subsystem') ? params.subsystem : [];
        } else {
            subsystems = this.filterComp.getValues('subsystem');
        }

        return this.adminApi.getEmails(
            {
                count: this.paginator.pageSize || 25,
                page: this.paginator.pageIndex,
                subsystems: subsystems
            }
        );
    }

    updateEmails(data: IEmailList) {
        this.loading = false;
        if (data) {
            this.emailCount = data.emailCount;
            this.emails = data.emails;
            this.subsystems = data.subsystems.map(x => ({value: x, name: x}));
        }

        this.pageTop.nativeElement.scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    }

    updateQueryParams(): void {
        const queryParams: any = {};

        if (this.paginator.pageSize !== 25) {
            queryParams.limit = this.paginator.pageSize;
        }

        if (this.paginator.pageIndex !== 0) {
            queryParams.page = this.paginator.pageIndex;
        }

        for (const filter of this.filters) {
            if (filter.values.length > 0) {
                queryParams[filter.key] = filter.values;
            }
        }

        this.router.navigate([], {
            relativeTo: this.route,
            queryParams: queryParams
        }).then();
    }

    setupFilters(): void {
        const listify = a => {if (!Array.isArray(a)) { return [a]; } else { return ''; }};
        const params = this.route.snapshot.queryParams;

        this.filters = [
            {
                name: 'Subsystem',
                key: 'subsystem',
                type: 'select',
                multiple: true,
                values: params.hasOwnProperty('subsystem') ? listify(params.subsystem) : [],
                options: this.subsystems
            },
        ];

        // region Event subscriptions
        this.paginator.page
            .pipe(
                switchMap(() => {
                    const resources = this.loadEmails(false);
                    this.updateQueryParams();
                    return resources;
                }),
                map(data => this.updateEmails(data)),
                catchError(this.handleError)
            )
            .subscribe(() => {})
        ;
        // endregion
    }

    filterChange() {
        if (this.route.snapshot.queryParams.hasOwnProperty('page')) {
            this.paginator.pageIndex = this.route.snapshot.queryParams.page;
        }

        const emails = this.loadEmails(false);
        this.updateQueryParams();
        emails.subscribe(data => this.updateEmails(data));
    }

    private handleError(err): Observable<any> {
        this.loading = false;
        this.utils.showError(err);
        return of([]);
    }
}
