import {Component, OnInit} from '@angular/core';
import {IEmail} from '../../../interfaces';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {AdminApiService} from '../../../services/admin-api.service';
import {UtilsService} from '../../../../utils/services/utils.service';

@Component({
    selector: 'app-email-details',
    templateUrl: './email-details.component.html',
    styleUrls: ['./email-details.component.scss']
})
export class EmailDetailsComponent implements OnInit {
    email: IEmail;
    sanitize = (data) => this.utils.sanitize(data);

    constructor(
        private adminApi: AdminApiService,
        private utils: UtilsService,
        private route: ActivatedRoute,
        private location: Location,
    ) {}

    ngOnInit() {
        this.email = this.route.snapshot.data.email.email;
    }

    resend() {
        this.adminApi.resendEmail(this.email.emailId).subscribe(
            data => {
                this.utils.showMessage(data['message']);
            }
        );
    }

    back() {
        this.location.back();
    }
}
