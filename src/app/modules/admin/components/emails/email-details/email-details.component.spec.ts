import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {EmailDetailsComponent} from './email-details.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';

describe('EmailDetailsComponent', () => {
    let component: EmailDetailsComponent;
    let fixture: ComponentFixture<EmailDetailsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EmailDetailsComponent ],
            imports: [
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                email: {
                                    email: {
                                        emailId: 1,
                                        timestamp: '2018-12-04T15:09:20',
                                        subsystem: 'auditor_rfc26',
                                        subject: 'Required tags audit notification',
                                        sender: 'probator@domain.tld',
                                        recipients: 'name@domain.tld',
                                        uuid: '0ebdf64c-7081-4748-86c3-6c545426a9a6',
                                        messageHtml: '<div>test</div>',
                                        messageText: 'test'
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EmailDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
