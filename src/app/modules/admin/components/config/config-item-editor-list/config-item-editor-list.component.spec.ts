import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigItemEditorListComponent} from './config-item-editor-list.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {AdminMaterialModule} from '../../../admin.material.module';

describe('ConfigItemEditorListComponent', () => {
    let component: ConfigItemEditorListComponent;
    let fixture: ComponentFixture<ConfigItemEditorListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConfigItemEditorListComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigItemEditorListComponent);
        component = fixture.componentInstance;
        component.list = new FormControl('');
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
