import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {UtilsService} from '../../../../utils/services/utils.service';

@Component({
    selector: 'app-config-item-editor-list',
    templateUrl: './config-item-editor-list.component.html',
    styleUrls: ['./config-item-editor-list.component.scss']
})
export class ConfigItemEditorListComponent implements OnInit {
    @Input() list: FormControl;
    listCtrl = new FormControl();
    readonly separatorKeyCodes: number[] = [ENTER, COMMA];

    constructor(private utils: UtilsService) { }

    ngOnInit() {}

    addListValue(event) {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            if (this.list.value.indexOf(value) !== -1) {
                this.utils.showWarning('Option already exists');
                return;
            }

            this.list.value.push(event.value);
            if (input) {
                input.value = '';
            }
        }
    }

    removeListValue(value) {
        const idx = this.list.value.indexOf(value);

        if (idx >= 0) {
            this.list.value.splice(idx, 1);
        }
    }
}
