import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IConfigNamespace} from '../../../interfaces';
import {ConfigListComponent} from '../config-list/config-list.component';

@Component({
    selector: 'app-config-namespace-delete',
    templateUrl: './config-namespace-delete.component.html',
    styleUrls: ['./config-namespace-delete.component.scss']
})
export class ConfigNamespaceDeleteComponent {
    namespace: IConfigNamespace;

    constructor(public dialogRef: MatDialogRef<ConfigListComponent>, @Inject(MAT_DIALOG_DATA) namespace: IConfigNamespace) {
        this.namespace = namespace;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }
}
