import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigNamespaceDeleteComponent} from './config-namespace-delete.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('ConfigNamespaceDeleteComponent', () => {
    let component: ConfigNamespaceDeleteComponent;
    let fixture: ComponentFixture<ConfigNamespaceDeleteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConfigNamespaceDeleteComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                AdminMaterialModule
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigNamespaceDeleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
