import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigImportExportComponent} from './config-import-export.component';
import {UtilsModule} from '../../../../utils/utils.module';
import {AdminMaterialModule} from '../../../admin.material.module';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ImportExportComponent} from '../../shared/import-export/import-export.component';
import {ReactiveFormsModule} from '@angular/forms';

describe('ConfigImportExportComponent', () => {
    let component: ConfigImportExportComponent;
    let fixture: ComponentFixture<ConfigImportExportComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConfigImportExportComponent,
                ImportExportComponent,
            ],
            imports: [
                NoopAnimationsModule,
                HttpClientModule,
                UtilsModule,
                AdminMaterialModule,
                ReactiveFormsModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigImportExportComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
