import {Component, OnInit} from '@angular/core';
import {ImportExportComponent} from '../../shared/import-export/import-export.component';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-config-import-export',
    templateUrl: '../../shared/import-export/import-export.component.html',
    styleUrls: ['../../shared/import-export/import-export.component.scss']
})
export class ConfigImportExportComponent extends ImportExportComponent implements OnInit {
    ngOnInit() {
        this.exportControl.setValue('json');
    }

    doExport() {
        this.adminApi.exportConfig().pipe(
            map(data => {
                return new Blob([JSON.stringify(JSON.parse(atob(data)))], { type: 'application/json' });
            })
        )
        .subscribe(
            blob => {
                const el = document.createElement('a');
                const url = URL.createObjectURL(blob);

                // Create a temporary link element, add it to the document and click it
                el.href = url;
                el.download = 'probator-configuration.json';
                document.body.appendChild(el);
                el.click();

                // Cleanup element and object url
                document.body.removeChild(el);
                URL.revokeObjectURL(url);

                this.dialogRef.close();
            }
        );
    }

    doImport(importData: string) {
        this.adminApi.importConfig(importData).subscribe(data => {
            this.utils.showMessage(data['message']);
            this.dialogRef.close(true);
        });
    }
}
