import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigItemAddComponent} from './config-item-add.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {ConfigItemEditorChoiceComponent} from '../config-item-editor-choice/config-item-editor-choice.component';
import {ConfigItemEditorListComponent} from '../config-item-editor-list/config-item-editor-list.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('ConfigItemAddComponent', () => {
    let component: ConfigItemAddComponent;
    let fixture: ComponentFixture<ConfigItemAddComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConfigItemAddComponent,
                ConfigItemEditorChoiceComponent,
                ConfigItemEditorListComponent
            ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                ReactiveFormsModule,
                AdminMaterialModule
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            params: {
                                namespacePrefix: 'default'
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigItemAddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
