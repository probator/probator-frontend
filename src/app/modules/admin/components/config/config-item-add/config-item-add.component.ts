import {Component, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {ConfigItemSharedEditorComponent} from '../config-item-shared-editor/config-item-shared-editor.component';

@Component({
    selector: 'app-config-add',
    templateUrl: '../config-item-shared-editor/config-item-shared-editor.component.html',
    styleUrls: ['../config-item-shared-editor/config-item-shared-editor.component.scss']
})
export class ConfigItemAddComponent extends ConfigItemSharedEditorComponent implements OnInit {
    ngOnInit() {
        const namespace = this.route.snapshot.params.namespacePrefix;
        this.buttonText = 'Add Config Item';
        this.configForm = this.fb.group(
            {
                namespacePrefix: [{value: namespace, disabled: true}, Validators.required],
                key: ['', Validators.required],
                type: ['', Validators.required],
                description: [''],
                value: ['', Validators.required],
            }
        );
    }

    save() {
        this.adminApi.addConfigItem(this.configForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../../'], {relativeTo: this.route}).then();
            }
        );
    }
}
