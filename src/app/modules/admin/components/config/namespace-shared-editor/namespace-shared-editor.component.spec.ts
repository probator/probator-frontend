import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NamespaceSharedEditorComponent} from './namespace-shared-editor.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {RouterTestingModule} from '@angular/router/testing';

describe('NamespaceSharedEditorComponent', () => {
    let component: NamespaceSharedEditorComponent;
    let fixture: ComponentFixture<NamespaceSharedEditorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ NamespaceSharedEditorComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(NamespaceSharedEditorComponent);
        component = fixture.componentInstance;
        component.namespaceForm = fb.group({
            namespacePrefix: ['', Validators.required],
            name: ['', Validators.required],
            sortOrder: [0, [Validators.required, Validators.pattern(/\d+/)]],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
