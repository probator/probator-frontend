import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AdminApiService} from '../../../services/admin-api.service';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
    selector: 'app-config-namespace-shared-editor',
    templateUrl: './namespace-shared-editor.component.html',
    styleUrls: ['./namespace-shared-editor.component.scss']
})
export class NamespaceSharedEditorComponent {
    namespaceForm: FormGroup;
    buttonText: string;

    constructor(
        protected fb: FormBuilder,
        protected adminApi: AdminApiService,
        protected metadata: MetadataService,
        protected utils: UtilsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected location: Location
    ) { }

    save() {}

    cancel() {
        this.location.back();
    }
}
