import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IConfigItem} from '../../../interfaces';
import {ConfigNamespaceListComponent} from '../config-namespace-list/config-namespace-list.component';

@Component({
    selector: 'app-config-item-delete',
    templateUrl: './config-item-delete.component.html',
    styleUrls: ['./config-item-delete.component.scss']
})
export class ConfigItemDeleteComponent {
    configItem: IConfigItem;

    constructor(public dialogRef: MatDialogRef<ConfigNamespaceListComponent>, @Inject(MAT_DIALOG_DATA) configItem: IConfigItem) {
        this.configItem = configItem;
    }

    confirm() {
        this.dialogRef.close(true);
    }

    cancel() {
        this.dialogRef.close(false);
    }

}
