import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigItemDeleteComponent} from './config-item-delete.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('ConfigItemDeleteComponent', () => {
    let component: ConfigItemDeleteComponent;
    let fixture: ComponentFixture<ConfigItemDeleteComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConfigItemDeleteComponent ],
            imports: [
                AdminMaterialModule
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: []}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigItemDeleteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
