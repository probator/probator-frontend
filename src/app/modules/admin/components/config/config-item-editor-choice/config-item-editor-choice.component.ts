import {Component, Input, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {UtilsService} from '../../../../utils/services/utils.service';
import {MatSelectChange} from '@angular/material/select';

@Component({
    selector: 'app-config-choice-editor',
    templateUrl: './config-item-editor-choice.component.html',
    styleUrls: ['./config-item-editor-choice.component.scss']
})
export class ConfigItemEditorChoiceComponent implements OnInit {
    @Input() choice: FormControl;
    choiceEnabledSelect = new FormControl();
    choiceMinInput = new FormControl([], Validators.required);
    choiceMaxInput = new FormControl([], Validators.required);

    constructor(private utils: UtilsService) {}

    ngOnInit() {
        this.choiceMinInput.setValue(this.choice.value.min_items);
        this.choiceMaxInput.setValue(this.choice.value.max_items);
        this.choiceEnabledSelect.setValue(this.choice.value.enabled);
        this.choiceMinInput.valueChanges.subscribe((value) => this.updateMin(value));
        this.choiceMaxInput.valueChanges.subscribe((value) => this.updateMax(value));
    }

    updateMin(value) {
        this.choice.value.min_items = value;
        if (value > this.choice.value.max_items) {
            this.choice.value.max_items = this.choice.value.min_items;
            this.choiceMaxInput.setValue(this.choice.value.max_items);
        }
    }

    updateMax(value) {
        if (value < this.choice.value.min_items) {
            this.utils.showWarning('Can not set a value less than min required items');
            this.choice.value.max_items = this.choice.value.min_items;
            this.choiceMaxInput.setValue(this.choice.value.max_items);
        } else {
            this.choice.value.max_items = value;
        }
    }

    updateEnabled(event: MatSelectChange) {
        this.choiceEnabledSelect.setValue(event.value);
        this.choice.value.enabled = event.value;

        if (event.value.length > this.choice.value.max_items) {
            this.utils.showWarning(`You have selected ${event.value.length - this.choice.value.max_items} too many values`);
        } else if (event.value.length < this.choice.value.min_items) {
            this.utils.showWarning(
                `You have selected ${event.value.length} options, but you must select at least ${this.choice.value.min_items}`
            );
        }
    }
}
