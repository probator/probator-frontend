import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ConfigItemEditorChoiceComponent} from './config-item-editor-choice.component';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {AdminMaterialModule} from '../../../admin.material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('ConfigItemEditorChoiceComponent', () => {
    let component: ConfigItemEditorChoiceComponent;
    let fixture: ComponentFixture<ConfigItemEditorChoiceComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConfigItemEditorChoiceComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigItemEditorChoiceComponent);
        component = fixture.componentInstance;
        component.choice = new FormControl('');
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
