import {Component, OnInit} from '@angular/core';
import {NamespaceSharedEditorComponent} from '../namespace-shared-editor/namespace-shared-editor.component';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-config-namespace-edit',
    templateUrl: '../namespace-shared-editor/namespace-shared-editor.component.html',
    styleUrls: ['../namespace-shared-editor/namespace-shared-editor.component.scss']
})
export class ConfigNamespaceEditComponent extends NamespaceSharedEditorComponent implements OnInit {
    ngOnInit() {
        const ns = this.route.snapshot.data.namespace.namespace;
        this.buttonText = 'Edit Namespace';
        this.namespaceForm = this.fb.group(
            {
                namespacePrefix: [{value: ns.namespacePrefix, disabled: true}, Validators.required],
                name: [ns.name, Validators.required],
                sortOrder: [ns.sortOrder, [Validators.required, Validators.pattern(/\d+/)]],
            }
        );
    }

    save() {
        this.adminApi.updateConfigNamespace(this.namespaceForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../../../'], {relativeTo: this.route}).then();
            }
        );
    }
}
