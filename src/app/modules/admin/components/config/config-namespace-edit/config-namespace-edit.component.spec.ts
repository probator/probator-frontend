import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigNamespaceEditComponent} from './config-namespace-edit.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRoute} from '@angular/router';

describe('ConfigNamespaceEditComponent', () => {
    let component: ConfigNamespaceEditComponent;
    let fixture: ComponentFixture<ConfigNamespaceEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConfigNamespaceEditComponent ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                namespace: {
                                    namespace: {
                                        namespacePrefix: 'default',
                                        name: 'Default',
                                        sortOrder: 0,
                                        configItems: []
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(ConfigNamespaceEditComponent);
        component = fixture.componentInstance;
        component.namespaceForm = fb.group({
            namespacePrefix: ['', Validators.required],
            name: ['', Validators.required],
            sortOrder: [0, [Validators.required, Validators.pattern(/\d+/)]],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
