import {Component, OnInit} from '@angular/core';
import {NamespaceSharedEditorComponent} from '../namespace-shared-editor/namespace-shared-editor.component';
import {Validators} from '@angular/forms';

@Component({
    selector: 'app-config-namespace-add',
    templateUrl: '../namespace-shared-editor/namespace-shared-editor.component.html',
    styleUrls: ['../namespace-shared-editor/namespace-shared-editor.component.scss']
})
export class ConfigNamespaceAddComponent extends NamespaceSharedEditorComponent implements OnInit {
    ngOnInit() {
        this.buttonText = 'Create Namespace';
        this.namespaceForm = this.fb.group(
            {
                namespacePrefix: ['', Validators.required],
                name: ['', Validators.required],
                sortOrder: [0, [Validators.required, Validators.pattern(/\d+/)]],
            }
        );
    }

    save() {
        this.adminApi.createConfigNamespace(this.namespaceForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../../'], {relativeTo: this.route}).then();
            }
        );
    }
}
