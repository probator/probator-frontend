import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigNamespaceAddComponent} from './config-namespace-add.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

describe('ConfigNamespaceAddComponent', () => {
    let component: ConfigNamespaceAddComponent;
    let fixture: ComponentFixture<ConfigNamespaceAddComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConfigNamespaceAddComponent ],
            imports: [
                NoopAnimationsModule,
                HttpClientModule,
                RouterTestingModule,
                ReactiveFormsModule,
                AdminMaterialModule,
            ],
            providers: [
                {provide: MatDialogRef, useValue: {}},
                {provide: MAT_DIALOG_DATA, useValue: {}},
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(ConfigNamespaceAddComponent);
        component = fixture.componentInstance;
        component.namespaceForm = fb.group({
            namespacePrefix: ['', Validators.required],
            name: ['', Validators.required],
            sortOrder: [0, [Validators.required, Validators.pattern(/\d+/)]],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
