import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {IConfigNamespace} from '../../../interfaces';
import {fabAnimations} from './fab-animations';
import {AdminApiService} from '../../../services/admin-api.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {MatDialog} from '@angular/material/dialog';
import {ConfigImportExportComponent} from '../config-import-export/config-import-export.component';

@Component({
    selector: 'app-config-list',
    templateUrl: './config-list.component.html',
    styleUrls: ['./config-list.component.scss'],
    animations: fabAnimations,
})
export class ConfigListComponent implements OnInit {
    namespaces: IConfigNamespace[];
    fabState = 'inactive';
    fabButtons = [
        {
            tooltip: 'Add a new namespace',
            icon: 'add',
            click: () => {
                this.router.navigate(
                    ['namespace/add'],
                    {relativeTo: this.route}
                ).then();
            },
        },
        {
            tooltip: 'Export configuration',
            icon: 'import_export',
            click: () => {
                this.importExport();
            }
        }
    ];
    buttons = [];

    constructor(
        private adminApi: AdminApiService,
        private utils: UtilsService,
        private route: ActivatedRoute,
        private router: Router,
        private dialog: MatDialog,
    ) {

    }

    ngOnInit() {
        this.namespaces = this.route.snapshot.data.config.namespaces;
    }

    namespaceDeleted(namespace: IConfigNamespace) {
        const idx = this.namespaces.indexOf(namespace);

        if (idx !== -1) {
            this.namespaces.splice(idx, 1);
        }
    }

    importExport() {
        const dialogRef = this.dialog.open(ConfigImportExportComponent, {
            width: '600px',
        });

        dialogRef.afterClosed().subscribe(updateConfigList => {
            if (updateConfigList === true) {
                this.adminApi.getConfigList().subscribe(data => {
                    this.namespaces = data.namespaces;
                });
            }
        });
    }

    toggleFab() {
        if (this.buttons.length) {
            this.fabState = 'inactive';
            this.buttons = [];
        } else {
            this.fabState = 'active';
            this.buttons = this.fabButtons;
        }
    }
}
