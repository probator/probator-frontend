import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigListComponent} from './config-list.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ConfigNamespaceListComponent} from '../config-namespace-list/config-namespace-list.component';
import {RouterTestingModule} from '@angular/router/testing';
import {UtilsModule} from '../../../../utils/utils.module';
import {ConfigItemTypePipe} from '../../../pipes/config-item-type.pipe';
import {HttpClientModule} from '@angular/common/http';

describe('ConfigListComponent', () => {
    let component: ConfigListComponent;
    let fixture: ComponentFixture<ConfigListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConfigListComponent,
                ConfigNamespaceListComponent,
                ConfigItemTypePipe,
            ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule,
                UtilsModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                config: {
                                    namespaces: [
                                        {
                                            namespacePrefix: 'default',
                                            name: 'Default',
                                            sortOrder: 0,
                                            configItems: [
                                                {
                                                    configItemId: 1,
                                                    key: 'debug',
                                                    value: false,
                                                    type: 'bool',
                                                    namespacePrefix: 'default',
                                                    description: 'Enable debug mode for flask'
                                                },
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
