import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IConfigItem, IConfigNamespace} from '../../../interfaces';
import {AdminApiService} from '../../../services/admin-api.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {MatDialog} from '@angular/material/dialog';
import {AccountDeleteComponent} from '../../accounts/account-delete/account-delete.component';
import {ConfigItemDeleteComponent} from '../config-item-delete/config-item-delete.component';

@Component({
    selector: 'app-config-namespace-list',
    templateUrl: './config-namespace-list.component.html',
    styleUrls: ['./config-namespace-list.component.scss'],
})
export class ConfigNamespaceListComponent {
    @Input() namespace: IConfigNamespace;
    @Output() readonly removed = new EventEmitter<IConfigNamespace>();
    @Output() readonly itemRemoved = new EventEmitter<IConfigItem>();

    constructor(
        private adminApi: AdminApiService,
        private utils: UtilsService,
        private dialogRef: MatDialog,
    ) { }

    display(item: IConfigItem): string {
        switch (item.type) {
            case 'bool':
                return item.value ? '<b>Enabled</b>' : '<b>Disabled</b>';

            case 'array':
                return item.value.join(', ');

            case 'json':
                return `<pre>${JSON.stringify(item.value, null, 4)}</pre>`;

            case 'choice':
                return item.value.enabled.join(', ');

            default:
                return item.value;
        }
    }

    deleteNamespace(ns: IConfigNamespace) {
        const confirm = this.dialogRef.open(AccountDeleteComponent, {
            data: ns,
            width: '600px'
        });

        confirm.afterClosed().subscribe(doDelete => {
            if (doDelete) {
                this.adminApi.deleteConfigNamespace(ns).subscribe(
                    data => {
                        this.removed.emit(ns);
                        this.utils.showMessage(data['message']);
                    }
                );
            }
        });
    }

    deleteItem(item: IConfigItem): void {
        const confirm = this.dialogRef.open(ConfigItemDeleteComponent, {
            data: item,
            width: '600px'
        });

        confirm.afterClosed().subscribe(doDelete => {
            if (doDelete) {
                this.adminApi.deleteConfigItem(item).subscribe(
                    data => {
                        const idx = this.namespace.configItems.indexOf(item);

                        if (idx !== -1) {
                            this.namespace.configItems.splice(idx, 1);
                            this.namespace.configItems = [...this.namespace.configItems];
                            this.utils.showMessage(data['message']);
                        }
                    }
                );
            }
        });
    }
}
