import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigNamespaceListComponent} from './config-namespace-list.component';
import {AdminMaterialModule} from '../../../admin.material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ConfigItemTypePipe} from '../../../pipes/config-item-type.pipe';
import {HttpClientModule} from '@angular/common/http';
import {SanitizePipe} from '../../../../utils/pipes/sanitize.pipe';

describe('ConfigNamespaceListComponent', () => {
    let component: ConfigNamespaceListComponent;
    let fixture: ComponentFixture<ConfigNamespaceListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConfigNamespaceListComponent,
                ConfigItemTypePipe,
                SanitizePipe,
            ],
            imports: [
                NoopAnimationsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigNamespaceListComponent);
        component = fixture.componentInstance;
        component.namespace = {
            namespacePrefix: 'default',
            name: 'Default',
            sortOrder: 0,
            configItems: []
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
