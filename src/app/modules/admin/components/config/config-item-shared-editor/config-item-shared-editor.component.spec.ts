import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigItemSharedEditorComponent} from './config-item-shared-editor.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ConfigItemEditorChoiceComponent} from '../config-item-editor-choice/config-item-editor-choice.component';
import {ConfigItemEditorListComponent} from '../config-item-editor-list/config-item-editor-list.component';
import {RouterTestingModule} from '@angular/router/testing';

describe('ConfigItemSharedEditorComponent', () => {
    let component: ConfigItemSharedEditorComponent;
    let fixture: ComponentFixture<ConfigItemSharedEditorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConfigItemSharedEditorComponent,
                ConfigItemEditorChoiceComponent,
                ConfigItemEditorListComponent
            ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                HttpClientModule,
                RouterTestingModule,
                AdminMaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        const fb = new FormBuilder();
        fixture = TestBed.createComponent(ConfigItemSharedEditorComponent);
        component = fixture.componentInstance;
        component.configForm = fb.group({
            namespacePrefix: [{value: 'default', disabled: true}, Validators.required],
            key: ['', Validators.required],
            type: ['', Validators.required],
            description: [''],
            value: ['', Validators.required],
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
