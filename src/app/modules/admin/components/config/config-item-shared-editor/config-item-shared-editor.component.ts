import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AdminApiService} from '../../../services/admin-api.service';
import {MetadataService} from '../../../../../services/metadata.service';
import {UtilsService} from '../../../../utils/services/utils.service';
import {Location} from '@angular/common';
import {MatSelectChange} from '@angular/material/select';
import {ValidateJSON} from '../../../validators/json-validator';

@Component({
    selector: 'app-config-shared-editor',
    templateUrl: './config-item-shared-editor.component.html',
    styleUrls: ['./config-item-shared-editor.component.scss']
})
export class ConfigItemSharedEditorComponent {
    configForm: FormGroup;
    buttonText: string;

    readonly configTypes = [
        'string',
        'int',
        'float',
        'array',
        'json',
        'bool',
        'choice',
        'text'
    ].sort();

    constructor(
        protected fb: FormBuilder,
        protected adminApi: AdminApiService,
        protected metadata: MetadataService,
        protected utils: UtilsService,
        protected route: ActivatedRoute,
        protected router: Router,
        protected location: Location
    ) { }

    save() {}

    cancel() {
        this.location.back();
    }

    changeType(event: MatSelectChange) {
        this.configForm.removeControl('value');

        let defaultValue: any;
        let validators = [];

        switch (event.value) {
            case 'string':
                defaultValue = '';
                break;

            case 'json':
                defaultValue = '';
                validators = [ValidateJSON];
                break;

            case 'number':
            case 'float':
                defaultValue = 0;
                break;

            case 'array':
                defaultValue = [];
                break;

            case 'choice':
                defaultValue = {
                    enabled: [],
                    available: [],
                    max_items: 1,
                    min_items: 0
                };
                break;

            case 'bool':
                defaultValue = true;
                break;
        }
        this.configForm.addControl('value', new FormControl(defaultValue, {validators: validators}));
    }
}
