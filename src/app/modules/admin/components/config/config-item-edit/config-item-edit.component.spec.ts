import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ConfigItemEditComponent} from './config-item-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {AdminMaterialModule} from '../../../admin.material.module';
import {ActivatedRoute} from '@angular/router';
import {ConfigItemEditorListComponent} from '../config-item-editor-list/config-item-editor-list.component';
import {ConfigItemEditorChoiceComponent} from '../config-item-editor-choice/config-item-editor-choice.component';
import {HttpClientModule} from '@angular/common/http';

describe('ConfigItemEditComponent', () => {
    let component: ConfigItemEditComponent;
    let fixture: ComponentFixture<ConfigItemEditComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConfigItemEditComponent,
                ConfigItemEditorListComponent,
                ConfigItemEditorChoiceComponent
            ],
            imports: [
                NoopAnimationsModule,
                ReactiveFormsModule,
                RouterTestingModule,
                HttpClientModule,
                AdminMaterialModule
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            data: {
                                configItem: {
                                    config: {
                                        configItemId: 1,
                                        key: 'debug',
                                        value: false,
                                        type: 'bool',
                                        namespacePrefix: 'default',
                                        description: 'Enable debug mode for flask'
                                    }
                                }
                            }
                        }
                    }
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigItemEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
