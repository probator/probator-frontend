import {Component, OnInit} from '@angular/core';
import {ConfigItemSharedEditorComponent} from '../config-item-shared-editor/config-item-shared-editor.component';
import {Validators} from '@angular/forms';
import {ValidateJSON} from '../../../validators/json-validator';

@Component({
    selector: 'app-config-edit',
    templateUrl: '../config-item-shared-editor/config-item-shared-editor.component.html',
    styleUrls: ['../config-item-shared-editor/config-item-shared-editor.component.scss']
})
export class ConfigItemEditComponent extends ConfigItemSharedEditorComponent implements OnInit {
    ngOnInit() {
        const configItem = this.route.snapshot.data.configItem.config;
        const validators = [Validators.required];
        let value = configItem.value;
        this.buttonText = 'Update Config Item';

        if (configItem.type === 'json') {
            value = JSON.stringify(value, null, 4);
            validators.push(ValidateJSON);
        }

        this.configForm = this.fb.group(
            {
                namespacePrefix: [{value: configItem.namespacePrefix, disabled: true}, validators],
                key: [{value: configItem.key, disabled: true}, validators],
                type: [{value: configItem.type, disabled: true}, validators],
                description: [{value: configItem.description, disabled: true}],
                value: [value],
            }
        );
    }

    save() {
        this.adminApi.updateConfigItem(this.configForm.getRawValue()).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.router.navigate(['../../../'], {relativeTo: this.route}).then();
            }
        );
    }
}
