import {FormControl} from '@angular/forms';

export function ValidateJSON(control: FormControl) {
    const value = control.value;

    try {
        JSON.parse(value);
        return null;
    } catch (e) {
        return {json: 'Invalid JSON'};
    }
}
