import {Component, Input} from '@angular/core';
import {v4 as uuid} from 'uuid';
import {ChartConfiguration, generate as generateGraph} from 'c3';
import {GaugeConfig} from '../../interfaces';

@Component({
    selector: 'app-gauge',
    templateUrl: './gauge.component.html',
    styleUrls: ['./gauge.component.scss']
})
export class GaugeComponent {
    @Input() config: GaugeConfig;
    gaugeId: string;

    constructor() {
        this.gaugeId = `gauge-${uuid()}`;
        this.showGraph();
    }

    showGraph() {
        // This may run before the DOM is fully updated, so check if the
        // element exists, and if it does not, wait a few ms and try again
        if (! document.getElementById(this.gaugeId)) {
            setTimeout(() => this.showGraph(), 5);
            return;
        }

        const gaugeConfig: ChartConfiguration = {
            bindto: `#${this.gaugeId}`,
            legend: {
                show: false,
            },
            gauge: {
                min: 0,
                max: 100,
                units: `${this.config.value} Issues`
            },
            tooltip: {
                show: false
            },
            interaction: {
                enabled: false,
            },
            data: {
                type: 'gauge',
                colors: {
                    '': '#009700',
                },
                columns: [['', this.config.percentage]],
            }
        };
        gaugeConfig.size = this.config.size ? this.config.size : {width: 250, height: 150};

        generateGraph(gaugeConfig);
    }
}
