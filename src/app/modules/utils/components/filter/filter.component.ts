import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IFilter, IFilterButton} from '../../interfaces';

@Component({
    selector: 'app-filters',
    templateUrl: './filter.component.html',
    styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
    @Input() filters: IFilter[];
    @Input() buttons: IFilterButton[];
    @Output() updated: EventEmitter<any> = new EventEmitter<any>();

    constructor() { }

    ngOnInit() { }

    selectionChange() {
        this.updated.emit();
    }

    getValues(field: string): string | string[] | number | boolean {
        if (!this.filters) {
            return;
        }

        for (const filter of this.filters) {
            if (filter.key === field) {
                return filter.values;
            }
        }

        return null;
    }
}
