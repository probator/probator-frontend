export interface IFilterValue {
    name: string;
    value: any;
}

export interface IFilter {
    name: string;
    key: string;
    type: string;
    multiple?: boolean;
    values: string | string[];
    grouped?: boolean;
    options?: IFilterValue[] | IFilterGroup[];
}

export interface IFilterButton {
    icon: string;
    color: string;
    tooltip?: string;
    action(event: MouseEvent): void;
}

export interface IFilterGroup {
    name: string;
    options: IFilterValue[];
}

export interface GaugeSize {
    width?: number;
    height?: number;
}

export interface GaugeConfig {
    title: string;
    value: number;
    percentage: number;
    size?: GaugeSize;
}
