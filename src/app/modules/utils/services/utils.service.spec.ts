import {TestBed} from '@angular/core/testing';

import {UtilsService} from './utils.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';

describe('UtilsService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            MatSnackBarModule,
        ]
    }));

    it('should be created', () => {
        const service: UtilsService = TestBed.inject(UtilsService);
        expect(service).toBeTruthy();
    });
});
