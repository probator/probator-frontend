import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';
import {IMessageOptions, IResource} from '../../../interfaces';
import {Observable, of} from 'rxjs';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';
import {sprintf} from 'sprintf-js';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {
    constructor(private snackbar: MatSnackBar, private sanitizer: DomSanitizer) { }

    /**
     * Return the class name for the provided contact type
     * @param contactType - Contact type name
     * @returns - Name of the CSS class
     */
    public static getContactClass(contactType: string): string {
        switch (contactType) {
            case 'email':
                return 'blue';

            case 'slack':
                return 'orange';

            default:
                return 'default';
        }
    }

    /**
     * Return the class name for the provided account type
     * @param accountType - Account type name
     * @returns - Name of the CSS class
     */
    public static getAccountClass(accountType: string): string {
        switch (accountType) {
            case 'AWS':
                return 'orange';
            case 'DNS: CloudFlare':
                return 'yellow';
            case 'DNS: AXFR':
                return 'blue';
            default:
                return 'default';
        }
    }

    /**
     * Return the class name for the provided item type
     * @param itemType - Item type name
     * @returns - Name of the CSS class
     */
    public static getConfigItemClass(itemType: string): string {
        switch (itemType) {
            case 'string':
                return 'green';
            case 'int':
                return 'orange';
            case 'float':
                return 'yellow';
            case 'json':
                return 'red';
            case 'bool':
                return 'blue';
            default:
                return '';
        }
    }

    /**
     * Show a message in a snackBar
     *
     * Defaults to showing a standard message for 2.5 seconds at the bottom of the screen
     * @param message - Message text to display
     * @param options - Configuration options for the snackbar
     */
    public showMessage(message: string, options?: IMessageOptions): void {
        const defaultOpts = {
            verticalPosition: 'bottom',
            duration: 2500,
            panelClass: null,
        };
        const opts = {...defaultOpts, ...options};

        this.snackbar.open(
            message,
            'OK',
            opts as MatSnackBarConfig
        );
    }

    /**
     * Helper function to show a warning snackBar message
     * @param message - Message text to display
     */
    public showWarning(message: string): void {
        this.showMessage(message, {
            panelClass: 'warning',
        });
    }

    /**
     * Helper function to show an error snackBar message
     * @param message - Message text to display
     */
    public showError(message: string): void {
        this.showMessage(message, {
            verticalPosition: 'top',
            panelClass: 'error',
            duration: 5000
        });
    }

    /**
     * Error handler for the HTTP service
     *
     * Any errors will be shown as a snackbar message, and an empty / null observable returned to halt any further processing of the error
     * @param err - Error raised by the HTTP client
     * @returns - Returns a observable on null
     */
    public handleError(err: any): Observable<any> {
        let errorMessage: string;
        if (err.error instanceof ErrorEvent) {
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            if (err.hasOwnProperty('body')) {
                if (err.body.hasOwnProperty('error')) {
                    errorMessage = `Backend return code ${err.status}: ${err.body.error}`;
                } else if (err.body.hasOwnProperty('message')) {
                    errorMessage = `Backend return code ${err.status}: ${err.body.message}`;
                }
            } else if (err.hasOwnProperty('error')) {
                if (err.error.hasOwnProperty('message')) {
                    errorMessage = err.error.message;
                }
            }
        }

        switch (err.status) {
            case 502:
                errorMessage = 'Backend server down';
                break;
            case 404:
                errorMessage = 'API Endpoint not found';
                break;
            case 401:
                // 401 errors are handled by the auth-interceptor, so we'll ignore this
                return;
        }

        if (!errorMessage) {
            errorMessage = `Backend return code ${err.status}`;
        }

        this.showError(errorMessage);
        return of(null);
    }

    /**
     * Clean the input object and remove any empty, undefined or null values
     *
     * *NOTE*: This is a mutating function, it does not return a new copy
     * @param params - Returns a cleaned copy of the object
     */
    public cleanParams(params: object): void {
        Object.keys(params).forEach((key) => {
            const value = params[key];
            if (value === null ||
                value === undefined ||
                (typeof(value) === 'string' && value === '') ||
                (Array.isArray(value) && value.length === 0)
            ) {
                delete params[key];
            }
        });
    }

    /**
     * Return a sanitized copy of the input HTML
     * @param data - String to sanitize
     * @returns - Returns sanitized SafeHTML
     */
    public sanitize(data: string): SafeHtml {
        return this.sanitizer.bypassSecurityTrustHtml(data);
    }

    /**
     * Initiate the download of a file on the client
     * @param filename - Suggested filename
     * @param data - File contents
     * @param contentType - File content mimetype
     */
    public downloadBlob(filename: string, data: string, contentType?: string): void {
        const blob = new Blob([data], { type: contentType ? contentType : 'application/json'});
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');

        link.download = filename;
        link.href = url;

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);

        window.URL.revokeObjectURL(url);
    }

    /**
     * Validate the input string as JSON data
     * @param data - Input data
     * @returns - True if valid JSON, else false
     */
    public validateJson(data: string): boolean {
        if (data.trim() === '') {
            return false;
        }

        try {
            JSON.parse(data);
            return true;
        } catch (e) {
            return false;
        }
    }

    /**
     * Return the name of the given resource, if available. If there is no Name tag just return the resource ID
     * @param resource - Resource to find name of
     * @param includeId - Include the resource ID in the output, even if name exists
     * @returns name_or_id - Returns the resource name and/or resource ID
     */
    public getResourceName(resource: IResource, includeId?: boolean): string {
        if (includeId === undefined) {
            includeId = true;
        }

        if (resource.hasOwnProperty('tags')) {
            for (const tag of resource.tags) {
                if (tag.key === 'Name') {
                    // Some resource types have the same ID and name (S3 Buckets for example)
                    if (tag.value === resource.resourceId) {
                        return tag.value;
                    }

                    return includeId ? `${tag.value} (ID: ${resource.resourceId})` : tag.value;
                }
            }
        }

        return resource.resourceId;
    }

    /**
     * Take a date or string value and format date
     * @param inputDate - Input date
     * @returns date - Human readable formatted version of the date
     */
    public formatDate(inputDate: string | Date): string {
        const d = new Date(inputDate);
        return sprintf(
            '%d-%02d-%02d %02d:%02d:%02d',
            d.getUTCFullYear(),
            d.getUTCMonth() + 1,
            d.getUTCDate(),
            d.getUTCHours(),
            d.getUTCMinutes(),
            d.getUTCSeconds()
        );
    }
}
