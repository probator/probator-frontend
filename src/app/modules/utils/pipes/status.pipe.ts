import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'status'
})
export class StatusPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        if (value === true) {
            return '<span class="label green">Active</span>';
        } else {
            return '<span class="label red">Inactive</span>';
        }
    }

}
