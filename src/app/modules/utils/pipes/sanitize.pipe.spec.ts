import {SanitizePipe} from './sanitize.pipe';
import {inject} from '@angular/core/testing';
import {DomSanitizer} from '@angular/platform-browser';

describe('SanitizePipe', () => {
    let sanitizer;

    beforeEach(inject([DomSanitizer], p => {
        sanitizer = p;
    }));

    it('create an instance', () => {
        const pipe = new SanitizePipe(sanitizer);
        expect(pipe).toBeTruthy();
    });
});
