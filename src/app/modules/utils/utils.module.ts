import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterComponent} from './components/filter/filter.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {FormsModule} from '@angular/forms';
import {StatusPipe} from './pipes/status.pipe';
import {UtilsService} from './services/utils.service';
import {SanitizePipe} from './pipes/sanitize.pipe';
import {FileDropDirective} from './directives/file-drop.directive';
import {GaugeComponent} from './components/gauge/gauge.component';

@NgModule({
    declarations: [
        FilterComponent,
        StatusPipe,
        SanitizePipe,
        FileDropDirective,
        GaugeComponent
    ],
    imports: [
        CommonModule,
        MatSelectModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        MatTooltipModule,
        FormsModule,
    ],
    exports: [
        FilterComponent,
        StatusPipe,
        SanitizePipe,
        FileDropDirective,
        GaugeComponent,
    ],
    providers: [
        UtilsService
    ]
})
export class UtilsModule { }
