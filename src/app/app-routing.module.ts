import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthCallbackComponent} from './components/auth-callback/auth-callback.component';
import {LoginComponent} from './components/login/login.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ShellComponent} from './components/shell/shell.component';
import {LoginResolver} from './components/login/login.resolver';
import {AuthGuard} from './guards/auth';
import {AdminGuard} from './guards/admin';

const routes: Routes = [
    {
        path: '', component: ShellComponent,
        children: [
            {
                path: 'browse',
                loadChildren: () => import('./modules/browse/browse.module').then(m => m.BrowseModule),
                canActivateChild: [AuthGuard]
            },
            {
                path: 'admin', loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule),
                canActivateChild: [AuthGuard, AdminGuard]
            },
            {path: '', redirectTo: 'browse/resources', pathMatch: 'full'}
        ]
    },
    {
        path: 'login',
        component: LoginComponent,
        pathMatch: 'full',
        resolve: { authInfo: LoginResolver }
    },
    {path: 'authenticate/:jwtToken/:csrfToken', component: AuthCallbackComponent},
    {path: '**', component: NotFoundComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            {
                anchorScrolling: 'enabled',
                scrollPositionRestoration: 'enabled',
            }
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
