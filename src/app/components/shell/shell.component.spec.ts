import {LayoutModule} from '@angular/cdk/layout';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ShellComponent} from './shell.component';
import {MaterialModule} from '../../material.module';
import {MenuComponent} from '../menu/menu.component';
import {RouterTestingModule} from '@angular/router/testing';
import {IUser} from '../../interfaces';
import {UserService} from '../../services/user.service';

class MockUserService {
    user: IUser = {
        authSystem: 'Local Authentication',
        username: 'admin',
        roles: [{name: 'Admin', color: '#123456'}, {name: 'User', color: '#654321'}],
        name: 'Administrator',
        email: 'admin@probator.io'
    };
    roles = ['Admin', 'User'];

    hasRole(roleName: string): boolean {
        return this.roles ? this.roles.indexOf(roleName) !== -1 : false;
    }
}

describe('ShellComponent', () => {
    let component: ShellComponent;
    let fixture: ComponentFixture<ShellComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ShellComponent, MenuComponent],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                LayoutModule,
                MaterialModule,
            ],
            providers: [
                {
                    provide: UserService,
                    useClass: MockUserService,
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ShellComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should compile', () => {
        expect(component).toBeTruthy();
    });
});
