import {Component} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
    selector: 'app-sidenav',
    templateUrl: './shell.component.html',
    styleUrls: ['./shell.component.scss'],
})
export class ShellComponent {
    doCollapse: Observable<boolean> = this.breakpointObserver.observe('(max-width: 1000px').pipe(
        map(result => result.matches)
    );

    constructor(private breakpointObserver: BreakpointObserver) {}
}
