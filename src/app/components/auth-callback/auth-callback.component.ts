import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, DefaultUrlSerializer, Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {AppSettings} from '../../app-settings';

@Component({
    selector: 'app-auth-callback',
    templateUrl: './auth-callback.component.html',
    styleUrls: ['./auth-callback.component.scss']
})
export class AuthCallbackComponent implements OnInit {
    jwtToken: string = null;
    csrfToken: string = null;

    constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {
        this.jwtToken = this.route.snapshot.params.jwtToken;
        this.csrfToken = this.route.snapshot.params.csrfToken;
    }

    ngOnInit() {
        if (this.userService.processLogin(this.jwtToken, this.csrfToken)) {
            const previousUrl = window.localStorage.getItem(AppSettings.StorageKeyRedir);

            if (previousUrl) {
                const redir = new DefaultUrlSerializer().parse(decodeURIComponent(previousUrl));
                let url = redir.toString();
                if (url.indexOf('?') !== -1) {
                    url = url.substr(0, url.indexOf('?'));
                }
                window.localStorage.removeItem(AppSettings.StorageKeyRedir);
                this.router.navigate([url], {queryParams: redir.queryParams}).then();
            } else {
                this.router.navigate(['/browse/dashboard']).then();
            }
        }
    }
}
