import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AuthCallbackComponent} from './auth-callback.component';
import {RouterTestingModule} from '@angular/router/testing';
import {MaterialModule} from '../../material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('AuthCallbackComponent', () => {
    let component: AuthCallbackComponent;
    let fixture: ComponentFixture<AuthCallbackComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AuthCallbackComponent ],
            imports: [
                NoopAnimationsModule,
                RouterTestingModule,
                MaterialModule,
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AuthCallbackComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
