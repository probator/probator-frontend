import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, DefaultUrlSerializer, Router} from '@angular/router';
import {IAuthInfo} from '../../interfaces';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {UserService} from '../../services/user.service';
import {Events} from '../../events';
import {AppSettings} from '../../app-settings';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    authInfo: IAuthInfo;
    return: string;
    loginType: string;
    loginForm: FormGroup;

    constructor(
        private api: ApiService,
        private userService: UserService,
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        this.authInfo = this.route.snapshot.data.authInfo;
        this.return = this.route.snapshot.queryParams.return;
        this.loginType = this.authInfo.hasOwnProperty('state') ? 'state' : 'url';

        if (this.loginType === 'state') {
            this.loginForm = this.fb.group({
                username: ['', Validators.required],
                password: ['', Validators.required],
            });
        }

        this.userService.events.subscribe(event => {
            if (event === Events.USER_LOGIN) {
                const previousUrl = window.localStorage.getItem(AppSettings.StorageKeyRedir);

                if (previousUrl) {
                    const redir = new DefaultUrlSerializer().parse(decodeURIComponent(previousUrl));
                    let url = redir.toString();
                    if (url.indexOf('?') !== -1) {
                        url = url.substr(0, url.indexOf('?'));
                    }
                    window.localStorage.removeItem(AppSettings.StorageKeyRedir);
                    this.router.navigate([url], {queryParams: redir.queryParams}).then();
                } else {
                    this.router.navigate(['/']).then();
                }
            }
        });
    }

    login() {
        this.api.loginLocalUser(this.loginForm.value).subscribe(data => {
            this.userService.processLogin(data['authToken'], data['csrfToken']);
        });
    }

    loginRedirect(url): void {
        if (this.return) {
            if (url.indexOf('?') === -1) {
                url += `?redir=${this.return}`;
            } else {
                url += `&redir=${this.return}`;
            }
        }

        window.location.href = url;
    }
}
