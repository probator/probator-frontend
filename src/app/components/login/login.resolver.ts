import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ApiService} from '../../services/api.service';
import {IAuthInfo} from '../../interfaces';

@Injectable({
    providedIn: 'root'
})
export class LoginResolver implements Resolve<any> {
    constructor(private apiService: ApiService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAuthInfo> {
        return this.apiService.getAuthInfo();
    }
}
