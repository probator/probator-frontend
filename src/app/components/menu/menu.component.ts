import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {MatDialog} from '@angular/material/dialog';
import {ChangePasswordComponent} from '../change-password/change-password.component';
import {AppSettings} from '../../app-settings';
import {IMenuGroup} from '../../interfaces';
import * as md5 from 'md5';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    animations: AppSettings.MenuAnimations,
})
export class MenuComponent implements OnInit {
    menu: IMenuGroup[] = [];
    isOpen = {};

    constructor(private dialogRef: MatDialog, public userService: UserService) {
        this.menu = AppSettings.MenuItems;
        for (const group of this.menu) {
            this.isOpen[group.key] = true;
        }
    }

    ngOnInit() { }

    changePassword() {
        this.dialogRef.open(ChangePasswordComponent, {
            data: this.userService.user.userId,
            width: '600px',
        });
    }

    logout() {
        this.userService.logout();
    }

    userHasAccess(group: IMenuGroup): boolean {
        if (group.hasOwnProperty('requiredRole')) {
            return this.userService.hasRole(group.requiredRole);
        }

        return true;
    }

    getGravatarHash(): string {
        if (this.userService.user.email) {
            return md5(this.userService.user.email.toLowerCase());
        } else {
            return '00000000000000000000000000000000';
        }
    }
}
