import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MenuComponent} from './menu.component';
import {MaterialModule} from '../../material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {UserService} from '../../services/user.service';
import {IUser} from '../../interfaces';

class MockUserService {
    user: IUser = {
        authSystem: 'Local Authentication',
        username: 'admin',
        roles: [{name: 'Admin', color: '#123456'}, {name: 'User', color: '#654321'}],
        name: 'Administrator',
        email: 'admin@probator.io'
    };
    roles = ['Admin', 'User'];

    hasRole(roleName: string): boolean {
        return this.roles ? this.roles.indexOf(roleName) !== -1 : false;
    }
}

describe('MenuComponent', () => {
    let component: MenuComponent;
    let fixture: ComponentFixture<MenuComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ MenuComponent ],
            imports: [
                RouterTestingModule,
                NoopAnimationsModule,
                MaterialModule,
            ],
            providers: [
                {
                    provide: UserService,
                    useClass: MockUserService,
                }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
