import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MenuComponent} from '../menu/menu.component';
import {ApiService} from '../../services/api.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilsService} from '../../modules/utils/services/utils.service';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
    passwordForm: FormGroup;
    userId: number;

    constructor(
        private api: ApiService,
        private fb: FormBuilder,
        private utils: UtilsService,
        public dialogRef: MatDialogRef<MenuComponent>,
        @Inject(MAT_DIALOG_DATA) userId: number,
    ) {
        this.userId = userId;
    }

    ngOnInit() {
        this.passwordForm = this.fb.group({
            newPassword: ['', Validators.required],
            repeatPassword: ['', Validators.required],
        }, {validators: validatePassword});
    }

    confirm() {
        this.api.changePassword(this.userId, this.passwordForm.get('newPassword').value).subscribe(
            data => {
                this.utils.showMessage(data['message']);
                this.dialogRef.close(true);
            },
            error => {
                this.utils.showError(error);
            }
        );
    }

    cancel() {
        this.dialogRef.close(false);
    }
}

export function validatePassword(form: FormGroup) {
    const p1 = form.controls.newPassword;
    const p2 = form.controls.repeatPassword;

    // Only warn after the user has modified the fields and they don't match
    if ((!p1.dirty || !p2.dirty) || (p1.value === p2.value)) {
        return null;
    }

    return {'passwordMismatch': true};
}
