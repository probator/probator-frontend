import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar';
import {LayoutModule} from '@angular/cdk/layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './components/login/login.component';
import {AuthCallbackComponent} from './components/auth-callback/auth-callback.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ShellComponent} from './components/shell/shell.component';
import {MenuComponent} from './components/menu/menu.component';
import {ChangePasswordComponent} from './components/change-password/change-password.component';
import {APP_BASE_HREF} from '@angular/common';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                AppRoutingModule,
                HttpClientModule,
                BrowserModule,
                BrowserAnimationsModule,
                MatProgressSpinnerModule,
                MatToolbarModule,
                MatButtonModule,
                LayoutModule,
                MatSidenavModule,
                MatIconModule,
                MatListModule,
                MatCardModule,
                MatMenuModule,
                MatDialogModule,
                ReactiveFormsModule,
                FormsModule,
                MatInputModule,
                MatSnackBarModule,
            ],
            declarations: [
                AppComponent,
                LoginComponent,
                AuthCallbackComponent,
                NotFoundComponent,
                ShellComponent,
                MenuComponent,
                ChangePasswordComponent,
            ],
            providers: [
                {provide: APP_BASE_HREF, useValue: '/'}
            ]
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
