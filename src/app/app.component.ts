import {Component} from '@angular/core';
import {Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {UserService} from './services/user.service';
import {MetadataService} from './services/metadata.service';
import {debounceTime, filter, map} from 'rxjs/operators';
import {Events} from './events';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    loading = false;

    constructor(private router: Router, private userService: UserService, private metadataService: MetadataService) {
        this.metadataService.init();

        router.events.pipe(
            filter(routerEvent => {
                return (routerEvent instanceof NavigationStart ||
                    routerEvent instanceof NavigationEnd ||
                    routerEvent instanceof NavigationCancel ||
                    routerEvent instanceof NavigationError
                );
            }),
            debounceTime(100),
            map(routerEvent => {
                this.checkRouterEvent(routerEvent);
            })
        ).subscribe();

        this.userService.events.subscribe(event => {
            if (event === Events.USER_LOGOUT) {
                window.location.href = '/login';
            }
        });
    }

    checkRouterEvent(routerEvent: Event) {
        if (routerEvent instanceof NavigationStart) {
            this.loading = true;
        }

        if (routerEvent instanceof NavigationEnd || routerEvent instanceof NavigationCancel || routerEvent instanceof NavigationError) {
            this.loading = false;
        }
    }
}
