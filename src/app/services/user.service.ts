import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {IUser} from '../interfaces';
import {Events} from '../events';
import {Subject} from 'rxjs';
import {UtilsService} from '../modules/utils/services/utils.service';
import {AppSettings} from '../app-settings';

const JWT = new JwtHelperService();

@Injectable({
    providedIn: 'root'
})
export class UserService {
    events = new Subject<Events>();

    jwtToken: string = null;
    csrfToken: string = null;
    isLoggedIn = false;

    user: IUser = null;
    authSystem: string = null;
    expiration: number = null;
    roles: string[] = null;

    constructor(private utils: UtilsService) {
        const dataString = window.localStorage.getItem(AppSettings.StorageKey);
        if (dataString) {
            const data = JSON.parse(dataString);
            if (this.processLogin(data.auth, data.csrf)) {
                return;
            }
        }

        this.logout();
    }

    processLogin(jwt: string, csrf: string): boolean {
        try {
            const data = JWT.decodeToken(jwt);

            if (!JWT.isTokenExpired(jwt)) {
                this.jwtToken = jwt;
                this.csrfToken = csrf;
                this.authSystem = data.authSystem;
                this.expiration = data.exp;
                this.roles = data.roles;
                this.user = data.user;

                this.saveTokens();
                this.isLoggedIn = true;
                this.events.next(Events.USER_LOGIN);

                return true;
            }
        } catch (e) {
            this.utils.showMessage('Failed authenticating user');
        }

        return false;
    }

    isAuthed(): boolean {
        return this.isLoggedIn && !JWT.isTokenExpired(this.jwtToken);
    }

    logout() {
        this.authSystem = null;
        this.user = null;
        this.expiration = null;
        this.roles = null;
        this.csrfToken = null;

        window.localStorage.removeItem(AppSettings.StorageKey);

        this.events.next(Events.USER_LOGOUT);
    }

    saveTokens() {
        window.localStorage.setItem(AppSettings.StorageKey, JSON.stringify({
            expiry: this.expiration * 1000,
            auth: this.jwtToken,
            csrf: this.csrfToken
        }));
    }

    isAdmin(): boolean {
        if (this.isLoggedIn && this.user) {
            for (const role of this.user.roles) {
                if (role.name === 'Admin') {
                    return true;
                }
            }
        }

        return false;
    }

    hasRole(roleName: string): boolean {
        return this.roles ? this.roles.indexOf(roleName) !== -1 : false;
    }
}
