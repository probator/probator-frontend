import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IAuthInfo, IVersionInfo} from '../interfaces';
import {catchError} from 'rxjs/operators';
import {UtilsService} from '../modules/utils/services/utils.service';
import {AppSettings} from '../app-settings';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(private http: HttpClient, private utils: UtilsService) {}

    getAuthInfo(): Observable<IAuthInfo> {
        return this.http.get<IAuthInfo>(`/auth/login`)
            .pipe(catchError((err) => this.utils.handleError(err)));
    }

    changePassword(userId, password) {
        return this.http.put(`${AppSettings.API_V1}/user/password/${userId}`, {password: password})
            .pipe(catchError((err) => this.utils.handleError(err)));
    }

    loginLocalUser(form) {
        return this.http.post(`/auth/local/login`, form)
            .pipe(catchError((err) => this.utils.handleError(err)));
    }

    getVersionInfo(): Observable<IVersionInfo> {
        return this.http.get('/version')
            .pipe(catchError((err) => this.utils.handleError(err)));
    }
}

