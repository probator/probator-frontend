import {TestBed} from '@angular/core/testing';
import {UserService} from './user.service';
import {UtilsModule} from '../modules/utils/utils.module';

describe('UserService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            UtilsModule,
        ]
    }));

    it('should be created', () => {
        const service: UserService = TestBed.inject(UserService);
        expect(service).toBeTruthy();
    });
});
