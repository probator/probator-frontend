import {EventEmitter, Injectable} from '@angular/core';
import {UserService} from './user.service';
import {Events} from '../events';
import {
    IAccount,
    IAccountProperty,
    IAccountType,
    IIssueType,
    IMetadata,
    INotifier,
    IResourceType,
    IResourceTypeProperty,
    IUser
} from '../interfaces';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../app-settings';
import {UtilsService} from '../modules/utils/services/utils.service';
import {IFilterGroup, IFilterValue} from '../modules/utils/interfaces';

@Injectable({
    providedIn: 'root'
})
export class MetadataService {
    constructor(private userService: UserService, private utils: UtilsService, private http: HttpClient) { }
    metadataLoaded = new EventEmitter<any>();
    loaded = false;

    accounts: IAccount[] = [];
    regions: string[] = [];
    accountTypes: IAccountType[] = [];
    resourceTypes: IResourceType[] = [];
    issueTypes: IIssueType[] = [];
    currentUser: IUser;
    notifiers: INotifier[] = [];

    static _name_compare(a, b): number {
        if (a.name < b.name) {
            return -1;
        }

        if (a.name > b.name) {
            return 1;
        }

        return 0;
    }

    init(): void {
        if (this.userService.isLoggedIn) {
            this.load();
        } else {
            this.userService.events.subscribe(event => {
                if (event === Events.USER_LOGIN) {
                    this.load();
                }
            });
        }
    }

    load() {
        this.http.get<IMetadata>(`${AppSettings.API_V1}/metadata`).pipe(
            catchError((err) => this.utils.handleError(err))
        ).subscribe(metadata => {
            this.accounts = metadata.accounts;
            this.regions = metadata.regions;
            this.accountTypes = metadata.accountTypes;
            this.resourceTypes = metadata.resourceTypes;
            this.issueTypes = metadata.issueTypes;
            this.currentUser = metadata.currentUser;
            this.notifiers = metadata.notifiers;

            this.loaded = true;
            this.metadataLoaded.emit();
        });
    }

    // region ResourcePropertyType helper functions
    public getResourceTypeName(resourceType): string {
        switch (typeof(resourceType)) {
            case 'number':
                for (const rtype of this.resourceTypes) {
                    if (rtype.resourceTypeId === resourceType) {
                        return rtype.name;
                    }
                }
                break;

            case 'string':
                for (const rtype of this.resourceTypes) {
                    if (rtype.resourceType === resourceType) {
                        return rtype.name;
                    }
                }
                break;
        }

        return resourceType;
    }

    public getResourceProperty(typeId, key): IResourceTypeProperty {
        for (const rtype of this.resourceTypes) {
            if (typeId === rtype.resourceTypeId) {
                for (const property of rtype.properties) {
                    if (property.key === key) {
                        return property;
                    }
                }
            }
        }

        return null;
    }

    public getResourcePrimaryProperties(typeId) {
        const props = [];

        for (const rtype of this.resourceTypes) {
            if (typeId === rtype.resourceTypeId) {
                for (const property of rtype.properties) {
                    if (property.primary) {
                        props.push(property);
                    }
                }
            }
        }

        return props;
    }

    public getResourcePropertyName(typeId, key): string {
        const property = this.getResourceProperty(typeId, key);
        return property ? property.name : key;
    }

    public getResourcePropertyType(typeId, key): string {
        const property = this.getResourceProperty(typeId, key);
        return property ? property.type : key;
    }
    // endregion

    // region IssuePropertyType helper functions
    public getIssueTypeName(issueType): string {
        switch (typeof(issueType)) {
            case 'number':
                for (const itype of this.issueTypes) {
                    if (itype.issueTypeId === issueType) {
                        return itype.name;
                    }
                }
                break;

            case 'string':
                for (const itype of this.issueTypes) {
                    if (itype.issueType === issueType) {
                        return itype.name;
                    }
                }
                break;
        }

        return issueType;
    }

    public getIssueProperty(typeId, key) {
        for (const itype of this.issueTypes) {
            if (typeId === itype.issueTypeId) {
                for (const property of itype.properties) {
                    if (property.key === key) {
                        return property;
                    }
                }
            }
        }
    }

    public getIssuePrimaryProperties(typeId) {
        const props = [];
        for (const itype of this.issueTypes) {
            if (typeId === itype.issueTypeId) {
                for (const property of itype.properties) {
                    if (property.primary) {
                        props.push(property);
                    }
                }
            }
        }

        return props;
    }

    public getIssuePropertyName(typeId, key): string {
        const property = this.getIssueProperty(typeId, key);
        return property ? property.name : key;
    }

    public getIssuePropertyType(typeId, key): string {
        const property = this.getIssueProperty(typeId, key);
        return property ? property.type : key;
    }
    // endregion

    // region Account propertyType helper functiosn
    public getAccountName(accountId: number): string {
        for (const account of this.accounts) {
            if (account.accountId === accountId) {
                return account.accountName;
            }
        }

        return null;
    }

    public getAccountFilters(): IFilterValue[] {
        const accounts: IFilterValue[] = [];

        for (const account of this.accounts.sort()) {
            accounts.push({
                name: account.accountName,
                value: account.accountName,
            });
        }

        return accounts;
    }

    public getAccountProperty(typeId, key): IAccountProperty {
        for (const atype of this.accountTypes) {
            if (typeId === atype.name) {
                for (const property of atype.properties) {
                    if (property.key === key) {
                        return property;
                    }
                }
            }
        }

        return null;
    }

    public getAccountPropertyName(typeId, key): string {
        const prop = this.getAccountProperty(typeId, key);
        if (prop) {
            return prop.name;
        }

        return key;
    }
    // endregion

    public getLocationFilters(): IFilterValue[] {
        const locations: IFilterValue[] = [];

        for (const location of this.regions.sort()) {
            locations.push({
                name: location,
                value: location
            });
        }

        return locations;
    }

    public getResourceTypeFilters(): IFilterGroup[] {
        const cache = {};

        for (const rType of this.resourceTypes.sort(MetadataService._name_compare)) {
            if (cache.hasOwnProperty(rType.resourceGroup)) {
                cache[rType.resourceGroup].push({
                    name: rType.name,
                    value: rType.resourceType
                });
            } else {
                cache[rType.resourceGroup] = [{
                    name: rType.name,
                    value: rType.resourceType
                }];
            }
        }

        const resourceGroups: IFilterGroup[] = [];
        for (const groupName of Object.keys(cache).sort(MetadataService._name_compare)) {
            resourceGroups.push({
                name: groupName,
                options: cache[groupName]
            });
        }
        return resourceGroups;
    }

    public getIssueTypeFilters(): IFilterValue[] {
        const issueTypes: IFilterValue[] = [];

        for (const rType of this.issueTypes.sort(MetadataService._name_compare)) {
            issueTypes.push({
                name: rType.name,
                value: rType.issueType
            });
        }

        return issueTypes;
    }
}
