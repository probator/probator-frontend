import {TestBed} from '@angular/core/testing';
import {MetadataService} from './metadata.service';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../material.module';

describe('MetadataService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            HttpClientModule,
            MaterialModule,
        ]
    }));

    it('should be created', () => {
        const service: MetadataService = TestBed.inject(MetadataService);
        expect(service).toBeTruthy();
    });
});
