import {TestBed} from '@angular/core/testing';

import {ApiService} from './api.service';
import {HttpClientModule} from '@angular/common/http';
import {UtilsModule} from '../modules/utils/utils.module';

describe('ApiService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [
            HttpClientModule,
            UtilsModule,
        ]
    }));

    it('should be created', () => {
        const service: ApiService = TestBed.inject(ApiService);
        expect(service).toBeTruthy();
    });
});
